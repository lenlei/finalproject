window.onload = function() {
        loadEvent();
        // 兼容document.getElementsByClassName 方法；
        if (!document.getElementsByClassName) {
            document.getElementsByClassName = function(cls) {
                var ret = [];
                var els = document.getElementsByTagName('*');
                for (var i = 0; i < els.length; i++) {
                    if (els[i].className === cls ||
                        els[i].className.indexOf(cls + ' ') >= 0 ||
                        els[i].className.indexOf(' ' + cls) >= 0 ||
                        els[i].className.indexOf(' ' + cls + ' ') >= 0) {
                        ret.push(els[i]);
                    }
                };
                return ret;
            }
        }
        var dbAAccount = sessionStorage.getItem("acc"); //取得使用者的登入email
        var userCart = "Cart_" + dbAAccount;
        var userBuy = "Buy_" + dbAAccount;
        var userBuyDetail = "BuyDetail_" + dbAAccount;
        console.log(JSON.parse(localStorage.getItem(userCart)));
        console.log(JSON.parse(localStorage.getItem(userBuy)));
        console.log(JSON.parse(localStorage.getItem(userBuyDetail)));
        if (dbAAccount == null) {
            var conf = confirm('請先登入');
            if (conf) {
                window.location.href = "login.html"; //跳轉到登入頁面
            }
        }
        ShowAllItems(userCart);

        function ShowAllItems(userCart) {
            var txt = "";
            console.log(JSON.parse(localStorage.getItem(userCart)));
            var jsonstr = JSON.parse(localStorage.getItem(userCart));
            if (jsonstr != null) {
                var productlist = jsonstr.productlist;
                var totalAmount = jsonstr.totalAmount;
                var Year = "";
                var Month = "";
                if (productlist.length > 0) {
                    var ImgUrlStr = "../Images/";
                    for (var i in productlist) { //jsonstr.productlist[i].TempImgs = window.previewImgsArray;
                        txt += "<tr><td class='checkbox'><input type='checkbox' class='check-one check'/></td><td class='goods'><span>";
                        txt += "<a href='second.html?dbPId=" + productlist[i].dbPId + "&dbTId=" + productlist[i].dbTId + "' class='goodsTitle'>" +
                            // productlist[i].dbPType.split("/")[0] +
                            productlist[i].dbTTheme +
                            // "<br/></a></span><br/><img src='p-picture/o-wedding.jpg' alt=''/></td>";
                            "<br/></a></span><br/><img src=" + ImgUrlStr + productlist[i].TempImgs[0] + " alt=''/></td>";
                        txt += "<td class='goods'><span><a href='second.html?dbPId=" + productlist[i].dbPId + "&dbTId=" + productlist[i].dbTId + "' class='goodsTitle'>" +
                            productlist[i].name +
                            "</a></span><br/></td>";
                        if (productlist[i].dbTStatus == 9) {
                            txt += "<td class='goods'><font color='#008800'>作品完成</font></td>";
                        } else {
                            txt += "<td class='goods'><font color='#FF0000'>編輯中</font></td>";
                        }
                        // if(productlist[i].name=="桌曆"){
                        //     Year = productlist[i].startMonth.slice(0, 4);
                        //     Month = productlist[i].startMonth.slice(4);
                        //     txt += "<td class='rule'>製作開始月份<br/>" + Year + "年" + Month + "月<br/><br/>" +
                        //         "尺寸：21 x 15.8 cm<br/>頁數：12 + 4 頁 ( 雙面數位印刷 )<br/>紙材：雪銅卡紙</td><td class='NTspan'><span>NT$</span></td>";     
                        // }else{
                        //     txt += "<td class='rule'>尺寸：客製長度x3.4(寬)cm<br/>頁數：單頁(單面數位印刷)<br/>紙材：薄紙" +
                        //         "</td><td class='NTspan'><span>NT$</span></td>";
                        // }
                        txt += "<td class='NTspan'><span>NT$</span></td><td class='price'>" +
                            productlist[i].price +
                            "</td><td class='count'><div><span class='reduce'>-</span><input type='text' class='count-input' value='" +
                            productlist[i].num +
                            "'/><span class='add'>+</span></div></td><td class='NTspan'><span>NT$</span></td>";
                        txt += "<td class='subtotal'>" +
                            parseInt(productlist[i].num) *
                            parseInt(productlist[i].price) + "</td>";
                        txt += "<td class='opration'><span class='deleteOne'>刪 除</span></td></tr>";
                    }
                }
            }
            $("#tbody").html(txt);
            console.log(JSON.parse(localStorage.getItem(userCart)));
            console.log(JSON.parse(localStorage.getItem(userBuy)));
        } // end of ShowAllItems
        var cartTable = document.getElementById('cartTable');
        var tr = cartTable.children[1].rows; // children子節點;
        var checkInputs = document.getElementsByClassName('check');
        var checkAllInput = document.getElementsByClassName('check-all');
        var selectedTotal = document.getElementById('selectedTotal');
        var priceTotal = document.getElementById('priceTotal');
        var selected = document.getElementById('selected');
        var cartFooter = document.getElementById('cartFooter');
        var selectedViewList = document.getElementById('selectedViewList');
        // var multiDelete=document.getElementById('multiDelete');
        var allDelete = document.getElementById('allDelete');
        var selallSPAN = document.getElementsByClassName('selallSPAN');
        // 選擇框事件；
        for (var i = 0; i < checkInputs.length; i++) {
            checkInputs[i].onclick = function() {
                if (this.className === 'check-all check') { // 全選；
                    for (var j = 0; j < checkInputs.length; j++) {
                        checkInputs[j].checked = this.checked;
                    }
                };
                if (this.checked == false) {
                    for (var k = 0; k < checkAllInput.length; k++) {
                        checkAllInput[k].checked = false;
                    }
                }
                getTotal();
            }
        };
        // 全選的checkbox
        selallSPAN[0].onclick = selallSPAN[1].onclick = function() {
                for (var k = 0; k < checkAllInput.length; k++) {
                    if (checkAllInput[k].checked) {
                        checkAllInput[k].checked = false;

                    } else {
                        checkAllInput[k].checked = true;
                    }
                }
                for (var j = 0; j < checkInputs.length; j++) {
                    checkInputs[j].checked = checkAllInput[0].checked;
                }
                getTotal();
            }
            // 計算；
        function getTotal() {
            var selected = 0;
            var price = 0;
            var HTMLstr = '';
            for (var i = 0; i < tr.length; i++) {
                var perCount = tr[i].getElementsByTagName('input')[1].value;
                if (tr[i].getElementsByTagName('input')[0].checked) {
                    tr[i].className = "on"; // checkbox勾選後 背景色變灰
                    selected += parseInt(tr[i].getElementsByTagName('input')[1].value);
                    price += parseInt(tr[i].cells[8].innerHTML); //將各行的小計加總 若有增加td 記得cells索引值也要跟著改 金額才可正確加總
                } else {
                    tr[i].className = " ";
                }
            };
            selectedTotal.innerHTML = selected;
            priceTotal.innerHTML = price;
            $("#totalAmount").html(price);
            // selectedViewList.innerHTML=HTMLstr;
            // 選中0時；
            if (selected == 0) {
                cartFooter.className = "cartFooter";
            }
        }
        // 加減事件
        for (var i = 0; i < tr.length; i++) {
            // 加減按鈕；
            tr[i].onclick = function(e) {
                    var tridx = $(this).index(); // 取得tr行數 (即i值) tridx =0
                    // →對應productlist[0]商品資訊
                    e = e || window.event;
                    document.onselectstart = new Function("event.returnValue=false;");
                    var el = e.target || e.srcElement;
                    var cls = el.className;
                    var input = this.getElementsByTagName('input')[1];
                    var val = parseInt(input.value);
                    var reduce = this.getElementsByTagName('span')[3];
                    switch (cls) {
                        case 'add':
                            input.value = val + 1;
                            reduce.innerHTML = '-';
                            getSubtotal(this);
                            var jsonstr = JSON.parse(localStorage.getItem(userCart));
                            var productlist = jsonstr.productlist;
                            productlist[tridx].num = input.value;
                            var newTotalAmount = 0;
                            var newTotalNum = 0;
                            for (var i in productlist) {
                                newTotalAmount += parseInt(productlist[i].num) *
                                    parseInt(productlist[i].price);
                                newTotalNum += parseInt(productlist[i].num);
                            }
                            jsonstr.totalNum = newTotalNum;
                            jsonstr.totalAmount = newTotalAmount;
                            localStorage.setItem(userCart, JSON.stringify(jsonstr));
                            console.log(JSON.parse(localStorage.getItem(userCart)));
                            break;
                        case 'reduce':
                            if (val > 1) {
                                input.value = val - 1;
                                getSubtotal(this);
                                var jsonstr = JSON.parse(localStorage.getItem(userCart));
                                var productlist = jsonstr.productlist;
                                productlist[tridx].num = input.value;
                                var newTotalAmount = 0;
                                var newTotalNum = 0;
                                for (var i in productlist) {
                                    newTotalAmount += parseInt(productlist[i].num) *
                                        parseInt(productlist[i].price);
                                    newTotalNum += parseInt(productlist[i].num);
                                }
                                jsonstr.totalNum = newTotalNum;
                                jsonstr.totalAmount = newTotalAmount;
                                localStorage.setItem(userCart, JSON.stringify(jsonstr));
                                console.log(JSON.parse(localStorage.getItem(userCart)));
                            }
                            if (input.value <= 1) {
                                reduce.innerHTML = '-';
                            }
                            break;
                        case 'deleteOne':
                            // 單行刪除；
                            var conf = confirm('確定要刪除嗎？');
                            if (conf) {
                                this.parentNode.removeChild(this);
                                // var i = event.target.id; 改用tridx作為點擊事件時的陣列索引值
                                var jsonstr = JSON.parse(localStorage.getItem(userCart)); // 先從localStorage取出並轉為JSON物件
                                var productlist = jsonstr.productlist;
                                jsonstr.totalNum = parseInt(jsonstr.totalNum) - parseInt(productlist[tridx].num);
                                jsonstr.totalAmount = parseInt(jsonstr.totalAmount) - (parseInt(productlist[tridx].num) * parseInt(productlist[tridx].price));
                                // 移除商品
                                productlist.splice(tridx, 1);
                                localStorage.setItem(userCart, JSON.stringify(jsonstr)); // 改完後
                                // 再轉成物件字串
                                // 存回localStorage
                                console.log(JSON.parse(localStorage.getItem(userCart)));
                            }
                            break;
                        default:
                            break;
                    }
                    getTotal();
                }
                // input輸入事件；
            tr[i].getElementsByTagName('input')[1].onkeyup = function() {
                var val = parseInt(this.value);
                var tr = this.parentNode.parentNode;
                var reduce = tr.getElementsByTagName('span')[3];
                if (isNaN(val) || val < 1) {
                    val = 1;
                }
                this.value = val; // 輸入控制法
                if (val <= 1) {
                    reduce.innerHTML = "";
                } else {
                    reduce.innerHTML = "-";
                }
                getSubtotal(tr);
                getTotal();
            }
        } // end of 加減事件
        // 小計
        function getSubtotal(tr) {
            var tds = tr.cells;
            var price = parseInt(tds[5].innerHTML); //若有增加td 記得cells索引值也要跟著改 金額才可正確加總
            var count = tr.getElementsByTagName('input')[1].value;
            var subTotal = parseFloat(price * count);
            tds[8].innerHTML = subTotal; //若有增加td 記得cells索引值也要跟著改 金額才可正確加總
        }
        allDelete.onclick = function() {
            var conf = confirm('確定清空購物車嗎？');
            if (conf) {
                checkAllInput[0].checked = true;
                checkAllInput[0].onclick();
                cartDel();
                getTotal();
                localStorage.removeItem(userCart); // 僅移除使用者的購物車localStorage
                console.log(JSON.parse(localStorage.getItem(userCart)));
            }
        }

        function cartDel() {
            for (var i = 0; i < tr.length; i++) {
                var input = tr[i].getElementsByTagName('input')[0];
                if (input.checked) {
                    tr[i].parentNode.removeChild(tr[i]);
                    i--;
                };
            };
        }
        $("#ReadyBuy").click(function() {
            var jsonstr = JSON.parse(localStorage.getItem(userCart)); // 先從localStorage取出並轉為JSON物件
            var productlist = jsonstr.productlist;
            var dbOTotalPrice = $("#priceTotal").html();
            var insertResult = 0;
            var oid;
            // 如果priceTotal不為0，表示有選擇想購買的商品
            var youCanBuy = false;
            for (var j = checkInputs.length - 2; j > 0; j--) {
                if (checkInputs[j].checked) { // 如果有勾選
                    var idx = parseInt(j) - parseInt(1);
                    if (productlist[idx].dbTStatus == 9) { //如果已經完成作品編輯 才可購買
                        youCanBuy = true;
                        console.log(youCanBuy);
                    } else { //作品未完成編輯 不可購買
                        // checkInputs[j].checked = false;
                        youCanBuy = false;
                        for (var k = 0; k < checkAllInput.length; k++) {
                            checkAllInput[k].checked = false;
                        }
                        console.log(youCanBuy);
                        break;
                    }
                }
            } //先判斷是否可購買
            // alert(youCanBuy);
            if (youCanBuy) { //////////////////////////////////////////////////////////////////////////
                if (dbOTotalPrice != null && dbOTotalPrice != 0) {
                    var orderInfo = JSON.parse(localStorage.getItem(userBuy));
                    if (orderInfo == null || orderInfo.length == 0) {
                        orderInfo = { //order的JSON字串
                            "dbOId": "", //訂單編號
                            "dbOOrderDate": "", //訂購日期
                            "dbOPickpCity": { // 縣市名稱
                                "dbCid": "",
                                "dbCName": "",
                                "dbRegionBean": []
                            },
                            "dbOPickupRegion": { // 區域名稱
                                "dbRid": "",
                                "dbRName": "",
                                "dbCid": ""
                            },
                            "dbOPickUpDetail": "", //詳細地址
                            "dbODeliverTime": "", //出貨日期
                            "dbOArriveDate": "", //到貨日期
                            "dbOTotalPrice": 0, //訂單總金額
                            "dbOMemberEmail": dbAAccount, //使用者Email
                            "buytotalNum": 0 //購買總數
                        };
                    }
                    var orderDetail = JSON.parse(localStorage.getItem(userBuyDetail));
                    if (orderDetail == null || orderDetail.length == 0) {
                        orderDetail = []
                    }
                    // 先確認購物車內每行商品前的checkbox，有無勾選，如果有勾選(true)
                    // 表示使用者要購買這項商品
                    for (var j = checkInputs.length - 2; j > 0; j--) {
                        if (checkInputs[j].checked) { // 如果有勾選(true)	
                            var idx = parseInt(j) - parseInt(1);
                            console.log(productlist);
                            orderDetail.push({ //宣告orderDetail的JSON字串
                                "dbODProductPicture": "", //作品圖片集
                                "dbODAmount": parseInt(productlist[idx].num) * parseInt(productlist[idx].price), //小計
                                "dbODQty": productlist[idx].num, //訂購數量
                                "dbODProductRequire": (productlist[idx].name == "桌曆" ? "製作起始月份" + productlist[idx].startMonth : ""), //商品需求  開始製作月份
                                "dbODpaymentMethod": "", //付款方式
                                "dbODtransferMethod": "", //配送方式
                                "dbTStatus": productlist[idx].dbTStatus,
                                "TempImgs": productlist[idx].TempImgs, //預覽圖檔名陣列
                                "dbTId": productlist[idx].dbTId, //暫存作品編號
                                "dbTTheme": productlist[idx].dbTTheme, //作品主題
                                "dbTIntroduction": productlist[idx].dbTIntroduction, //作品說明
                                "isFinished": productlist[idx].isFinished,
                                "dbODOrder": "", //訂單編號
                                "dbODProduct": {
                                    "dbPId": productlist[idx].dbPId, //產品編號
                                    "dbPType": productlist[idx].dbPType, //產品類型(風格)
                                    "dbPUnitPrice": productlist[idx].price, // 單價
                                    "dbPName": {
                                        "dbPNId": "", //產品名稱編號
                                        "dbPNName": productlist[idx].name //產品名稱
                                    }
                                }
                            }); //end of orderDetail.push
                            //計算總數量
                            orderInfo.buytotalNum = parseInt(orderInfo.buytotalNum) + parseInt(productlist[idx].num);
                            //計算訂單總金額
                            orderInfo.dbOTotalPrice = parseInt(orderInfo.dbOTotalPrice) + (parseInt(productlist[idx].num) * parseInt(productlist[idx].price));
                            jsonstr.totalNum = parseInt(jsonstr.totalNum) - parseInt(productlist[idx].num);
                            jsonstr.totalAmount = parseInt(jsonstr.totalAmount) - (parseInt(productlist[idx].num) * parseInt(productlist[idx].price));
                            productlist.splice(idx, 1);
                        } // end of 如果有勾選(true)	
                    } //
                    orderInfo.dbOTotalPrice += 80; //加運費80
                    $.ajax({
                            url: "insertOrder",
                            contentType: "application/json;charset=utf-8",
                            type: "POST",
                            dataType: "json",
                            data: JSON.stringify(orderInfo),
                            async: false,
                            success: function(data) {
                                orderInfo.dbOId = data;
                                oid = data;
                                // alert("orderInfo.dbOId==>" + orderInfo.dbOId);
                                for (var i in orderDetail) {
                                    orderDetail[i].dbODOrder = data;
                                }
                                localStorage.setItem(userBuy, JSON.stringify(orderInfo));
                                $.ajax({
                                    url: "insertOrderDetail",
                                    contentType: "application/json;charset=utf-8",
                                    type: "POST",
                                    dataType: "json",
                                    data: JSON.stringify(orderDetail),
                                    async: false,
                                    success: function(data) {
                                        insertResult = data;
                                    }
                                })
                                localStorage.setItem(userBuyDetail, JSON.stringify(orderDetail));
                            }
                        }) //end of ajax
                    localStorage.setItem(userCart, JSON.stringify(jsonstr));
                    localStorage.setItem(userBuy, JSON.stringify(orderInfo));
                    localStorage.setItem(userBuyDetail, JSON.stringify(orderDetail));
                    console.log("userCart--------");
                    console.log(JSON.parse(localStorage.getItem(userCart)));
                    console.log("userBuy--------");
                    console.log(JSON.parse(localStorage.getItem(userBuy)));
                    console.log("userBuyDetail--------");
                    console.log(JSON.parse(localStorage.getItem(userBuyDetail)));
                    if (insertResult > 0) {
                        window.location.href = "sop_payment.html?oid=" + oid;
                    } else {
                        // alert("Ooops!!!!!!");
                    }
                } else { // 如果priceTotal為0，提醒使用者須選擇欲購買商品
                    alert("請選擇欲購買的商品");
                }
            } else if (dbOTotalPrice == null || dbOTotalPrice == 0) {
                alert("請選擇欲購買的商品");
            } else {
                alert("完成作品編輯才可購買喔!");
            }
        }); //end of readybuy
    } //end of window.onload

function loadEvent() {
    if (!document.all) {
        window.constructor.prototype.__defineGetter__("event", function() {
            var func = arguments.callee.caller;
            while (func != null) {
                var arg0 = func.arguments[0];
                if (arg0 && (arg0.constructor == Event || arg0.constructor == MouseEvent)) {
                    return arg0;
                }
                func = func.caller;
            }
            return null;
        });
    }
}


$(document).ready(function hidepicture() {
    var dbAAccount = sessionStorage.getItem("acc");
    var txt = "";
    if (dbAAccount != null) {
        txt += "<li  id='logout'class=' position-right'><a href='index.html'><img src='p-picture/logout.svg'width='32' height='32' title='會員登出'/></li></a>"
        txt += "<li id='user' class='position-right'><a href='Membercenter.html'><img src='p-picture/user.svg'width='32' height='32' title='會員中心'/></li></a>"
        txt += "<li id='shoppingcar' class='position-right'><a href='sop_MyCart.html'><img src='p-picture/cart-of-ecommerce.svg'width='32' href=height='32' title='購物車'/></li></a>"
        $("#picturesShow").html(txt)
    } else {
        txt += "<li  id='login'class=' position-right'><a href='login.html' ><img src='p-picture/login .svg'width='32' height='32' title='會員登入'/></li> </a>"
        $("#picturesShow").html(txt)
    }
})

$("#picturesShow").on("click", "#logout", function() {
    sessionStorage.removeItem('acc');
})