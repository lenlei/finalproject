//貼圖
//原畫板

drag = 0;
			move = 0;
			var ie = document.all;
			var nn6 = document.getElementById && !document.all;
			var isdrag = false;
			var y,x;
			var oDragObj;
			var pic_width,pic_height,pic_zoom;
			var divleft,divtop;
			
			function moveMouse(e) {
				if (isdrag) {
					oDragObj.style.top = (nn6 ? nTY + e.clientY - y : nTY + event.clientY - y) + "px";
					oDragObj.style.left = (nn6 ? nTX + e.clientX - x : nTX + event.clientX - x) + "px";
					return false;
				}
			}

			function initDrag(e) {
				var oDragHandle = nn6 ? e.target : event.srcElement;
				var topElement = "HTML";
				while (oDragHandle.tagName != topElement && oDragHandle.className != "dragAble") {
					oDragHandle = nn6 ? oDragHandle.parentNode : oDragHandle.parentElement;
				}
				if (oDragHandle.className == "dragAble") {
					isdrag = true;
					oDragObj = oDragHandle;
					nTY = parseInt(oDragObj.style.top + 0);
					y = nn6 ? e.clientY : event.clientY;
					nTX = parseInt(oDragObj.style.left + 0);
					x = nn6 ? e.clientX : event.clientX;
					document.onmousemove = moveMouse;
					return false;
				}
			}
			document.onmousedown = initDrag;
			document.onmouseup = new Function("isdrag=false");
			
			var btn = document.getElementById('previewBtn');
			var toDownload = document.getElementById('toDownload');
			
			btn.onclick = function() {
			
				html2canvas(document.querySelector("#myView"), {
					useCORS: true,
					onrendered: function (canvas){
						$("#previewImage").append(canvas);
					}
				});
				
				html2canvas(document.querySelector("#myCalendar"), {
					useCORS: true,
					onrendered: function (canvas){
						$("#previewImage").append(canvas);
					}
				});
			}
			
			toDownload.onclick = function() {
				html2canvas(document.querySelector("#previewImage"), {
					useCORS: true,
					onrendered: function (canvas){
						var a = document.createElement('a');
						a.href = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");
						a.download = 'image.png';
						a.click();
					}
				});
			}
			