window.onload = function() {
        var dbAAccount = sessionStorage.getItem("acc"); //取得使用者的登入email
        var userCart = "Cart_" + dbAAccount;
        console.log(JSON.parse(localStorage.getItem(userCart)));
        var allProduct = []; //放產品資訊
        $.ajax({
            url: "GetAllProduct",
            type: "GET",
            success: function(data) {
                    var json = $.parseJSON(data);
                    console.log(json);
                    $.each(json, function(index, list) {
                        allProduct.push(list);
                        // alert(allProduct[index].dbPUnitPrice);//OK
                    })
                    showAllProduct(allProduct, dbAAccount);
                } //end of success
        })
        console.log(allProduct);
        ////////////////////////////////////////////////////////////////////////////////////////////
        var CartList = localStorage.getItem(userCart);
        cart = {
                addProduct: function(product) {
                    if (CartList == null || CartList == "") {
                        var jsonstr = {
                            "productlist": [{
                                "dbPId": product.dbPId,
                                "name": product.name,
                                "num": product.num,
                                "price": product.price,
                                "dbPType": product.dbPType, //產品類型(風格)
                                "startMonth": product.startMonth,
                                "dbTId": product.dbTId, //暫存作品編號
                                "dbTStatus": product.dbTStatus,
                                "dbTTheme": product.dbTTheme, //作品主題
                                "dbTIntroduction": product.dbTIntroduction, //作品說明
                                "isFinished": product.isFinished,
                                "TempImgs": product.TempImgs
                            }],
                            "totalNum": product.num,
                            "totalAmount": (product.price * product.num)
                        }
                        localStorage.setItem(userCart, JSON.stringify(jsonstr));
                        console.log(JSON.parse(localStorage.getItem(userCart)));
                    } else {
                        console.log(JSON.parse(localStorage.getItem(userCart)));
                        var jsonstr = JSON.parse(CartList);
                        var productlist = jsonstr.productlist;
                        var result = false; //判斷購物車中是否已經有該商品
                        for (var i in productlist) {
                            if (productlist[i].dbTId == product.dbTId) {
                                productlist[i].num = parseInt(productlist[i].num) + parseInt(product.num);
                                result = true;
                            }
                        } //end of for 
                        if (!result) {
                            productlist.push({
                                "dbPId": product.dbPId,
                                "name": product.name,
                                "num": product.num,
                                "price": product.price,
                                "dbPType": product.dbPType, //產品類型(風格)
                                "startMonth": product.startMonth,
                                "dbTId": product.dbTId, //暫存作品編號
                                "dbTStatus": product.dbTStatus,
                                "dbTTheme": product.dbTTheme, //作品主題
                                "dbTIntroduction": product.dbTIntroduction, //作品說明
                                "isFinished": product.isFinished,
                                "TempImgs": product.TempImgs
                            });
                        }
                        jsonstr.totalNum = parseInt(jsonstr.totalNum) + parseInt(product.num);
                        jsonstr.totalAmount = parseInt(jsonstr.totalAmount) + (parseInt(product.num) * parseInt(product.price));
                        localStorage.setItem(userCart, JSON.stringify(jsonstr));
                        console.log(JSON.parse(localStorage.getItem(userCart)));
                    }
                }, //end of addProduct
                getProductlist: function() {
                    var jsonstr = JSON.parse(CartList);
                    var productlist = jsonstr.productlist;
                    return productlist;
                }
            } //end of cart
    } //end of window.onload

function showAllProduct(allProduct, dbAAccount) {
    var price;
    var dbPId, dbPType; //使用者選到的產品編號  &  產品類型(風格)
    var ImgUrlStr = "../Images/"; //圖片路徑
    var txtImg = "<div class='container'><div class='products'><ul class='clearfix row'>";
    for (var i in allProduct) {
        if (allProduct[i].dbPName.dbPNName == "桌曆") {
            txtImg += "<li class='col-4 product'><div class='thumbnail' name='" + allProduct[i].dbPType + "' id='" + allProduct[i].dbPId + "'>";
            txtImg += "<img src='" + ImgUrlStr + allProduct[i].dbPTypeImage + "' width='320' height='320' id='" + allProduct[i].dbPUnitPrice + "'>";
            txtImg += "<div class='cover'><a href='#'><h2><b>" + allProduct[i].dbPType.split("/")[1] + "</b><small><br><strong>" + allProduct[i].dbPType.split("/")[0] + "</strong><br></small></h2><br></a></div></div></li>";
        }
    }
    txtImg += "</ul></div></div>";
    $("#recent-product").html(txtImg);

    //開始製作Button  
    var submitBtnDiv = "<ul class=''><li><button name='submitbtn' id='ProductBTN' class='btn btn-danger btn-block arrow-forward start' type='submit'><span>建立作品，開始製作</span></button></li></ul>";
    $("#submitBtnDiv").html(submitBtnDiv);
    $("div.thumbnail").click(function() {
            dbPType = $(this).attr("name");
            alert("您選擇的風格是：" + dbPType.split("/")[0]);
            dbPId = $(this).attr("id");
            // alert("dbPId==>" + dbPId); //OK
            price = $(this).find("img").attr("id");
            // alert("price==>" + price); //OK
        })
        //點擊圖片後 增加黑色外框
    $(".recent-product ul li").click(function(e) {
        e.preventDefault();
        $('li').removeClass('active');
        $(this).addClass('active');
    });
    $("#ProductBTN").click(function() {
            var dbTId; //暫存作品編號
            //開始月份
            var startMonth = $("#startMonth").val(); //201812
            // var startMonth =$("#startMonth").find("option:selected").text(); //2018年12月
            //抓到使用者點選的值之後  塞值到product物件
            if (dbPType != null && startMonth != null && dbAAccount != null) {
                var TempWork = new TemporaryWork(dbAAccount);
                //資料送到server端
                $.ajax({
                        url: "TemporaryController",
                        type: "POST",
                        data: {
                            TempWork: JSON.stringify(TempWork),
                            dbPId: dbPId
                        },
                        success: function(data) {
                            if (data) {
                                dbTId = data; //得到回傳的dbTId

                                var firstProduct = { ///第一筆訂單 假資料
                                    "dbPId": 3, //使用者選到的產品編號
                                    "name": "桌曆",
                                    "num": 1, //預設製作數量1個
                                    "price": 700, //依照product表格的dog風格單價=700
                                    "dbPType": "寵物狗/Pet",
                                    "startMonth": "201901",
                                    "dbTId": 1, //暫存作品編號
                                    "dbTStatus": 8, //新加入購物車的作品狀態一開始皆為8(編輯中)
                                    "dbTTheme": "我的柴犬成長日記", //作品主題
                                    "dbTIntroduction": "", //作品說明
                                    "isFinished": false, //第一筆假資料 已完成
                                    "TempImgs": ["abc1542174950724-pageView.png"] //預覽圖路徑
                                }
                                cart.addProduct(firstProduct); ///第一筆訂單 假資料 加到購物車
                                var product = {
                                    "dbPId": dbPId, //使用者選到的產品編號
                                    "name": "桌曆",
                                    "num": 1, //預設製作數量1個
                                    "price": price,
                                    "dbPType": dbPType,
                                    "startMonth": startMonth,
                                    "dbTId": data, //暫存作品編號
                                    "dbTStatus": 8, //新加入購物車的作品狀態一開始皆為8(編輯中)
                                    "dbTTheme": "", //作品主題
                                    "dbTIntroduction": "", //作品說明
                                    "isFinished": false, //預設尚未完成作品
                                    "TempImgs": [] //預覽圖檔名
                                }
                                cart.addProduct(product); //加到購物車
                                window.location.href = "second.html?dbPId=" + dbPId + "&dbTId=" + dbTId; //跳轉到second.html
                            } else if (data == "Fail") {
                                alert("資料傳輸時發生錯誤，請再點一次開始製作");
                            }
                        }
                    }) //end of Ajax               
            } else if (dbPType == null) {
                alert("請選擇作品風格");
            } else if (dbAAccount == null) {
                var conf = confirm('請先登入');
                if (conf) {
                    window.location.href = "login.html"; //跳轉到登入頁面
                }
            }
        }) //end of 開始製作
    function TemporaryWork(dbAAccount) {
        this.dbAAccount = dbAAccount;
    }
} //end of showAllProduct