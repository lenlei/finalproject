window.onload = function() {
        var url = window.location.search;
        if (url.indexOf("?") != -1) {
            var str = url.substr(1);
            strs = str.split("&");
            var key = new Array(strs.length);
            var value = new Array(strs.length);
            for (i = 0; i < strs.length; i++) {
                key[i] = strs[i].split("=")[0]
                value[i] = unescape(strs[i].split("=")[1]);
                // alert(key[i] + "=" + value[i]);
                if (key[i] == "oid") {
                    window.dbOId = value[i]; //取得訂單編號  
                }
            }
        }
        dbAAccount = sessionStorage.getItem("acc"); //取得使用者的登入email
        var userBuy = "Buy_" + dbAAccount;
        var userBuyDetail = "BuyDetail_" + dbAAccount;
        // var CustomerInfo = "CustomerInfo_" + dbAAccount;
        console.log("userBuy--------");
        console.log(JSON.parse(localStorage.getItem(userBuy)));
        console.log("userBuyDetail--------");
        console.log(JSON.parse(localStorage.getItem(userBuyDetail)));
        if (dbAAccount != null && dbAAccount.length != 0) {
            $.ajax({
                url: "showOrderInfo",
                type: "POST",
                // async: false,
                data: { oid: dbOId },
                success: function(data) {
                    console.log("success的data--------------");
                    console.log(data);
                    var CustomerInfo = "CustomerInfo_" + dbAAccount;
                    var CustomerInfoStr = JSON.parse(localStorage.getItem(CustomerInfo));
                    window.Order = $.parseJSON(data);
                    if (Order != null) {
                        ShowOrderDetail(Order, CustomerInfoStr, userBuyDetail);
                    } else {
                        alert("無訂單資訊可顯示");
                    }
                }
            })
        } else {
            var conf = confirm('請先登入');
            if (conf) {
                window.location.href = "login.html"; //跳轉到登入頁面
            }
        }
    } //end of window.onload

function ShowOrderDetail(Order, CustomerInfoStr, userBuyDetail) { //buyjsonst
    var Details = JSON.parse(localStorage.getItem(userBuyDetail));
    console.log("Details-----");
    console.log(Details);
    var orderDetail = Order.dbOOrderDetail;
    var orderInfo = "";
    orderInfo += "<thead><tr><th><div class='form-group'><label>訂購日期：</label><p>" + Order.dbOOrderDate.slice(0, 13) + "</p></div></th><th><div class='form-group'><label>訂單編號：</label><p>" + Order.dbOId + "</p></div></th>";
    orderInfo += "</tr><tr><th><div class='form-group'><label>訂單狀態：</label><p>" + Order.dbOStatus.dbSName + "</p></div></th></tr></thead>";
    $("#orderInfo").html(orderInfo);

    var buyerInfo = "";
    buyerInfo += "<thead><tr><th><div class='form-group'><label style='display:inline-block;width:80px;'>姓 名：</label><span>" + CustomerInfoStr.dbAName + "</span></div></th></tr>";
    buyerInfo += "<tr><th><div class='form-group'><label style='display:inline-block;width:80px;'>電 話：</label><span>" + CustomerInfoStr.dbAMobile + "</span></div></th></tr><tr><th><div class='form-group'><label style='display:inline-block;width:80px;'>地 址：</label>";
    buyerInfo += "<span>" + Order.dbOPickpCity.dbCName + Order.dbOPickupRegion.dbRName + Order.dbOPickUpDetail + "</span></div></th></tr></thead>";
    $("#buyerInfo").html(buyerInfo);

    var orderList = "";
    // var productTotalAmount = 0;
    orderList += "<thead><tr><th><div class='form-group'><label>商品</label></div></th><th><div class='form-group'><label>作品圖片集</label></div></th>";
    orderList += "<th><div class='form-group'><label>規 格</label></div></th><th><div class='form-group'><label>單 價</label></div></th><th><div class='form-group'><label> 數 量</label></div></th>";
    orderList += "<th><div class='form-group'><label>小 計</label></div></th></tr>";
    var ImgUrlStr = "../Images/"; //  //<img src='p-picture/product-wedding.jpg' width='150px'>
    for (var i = 0; i < orderDetail.length; i++) { //<span class='download'>下載作品pdf</span> //原本的商品編號=orderDetail[i].dbODProduct.dbPId 
        orderList += "<tr><td>" + (i + 1) + "</td><td><img src=" + ImgUrlStr + Details[0].TempImgs[0] + " width='150px' alt=''/><br/><span class='download' id=" + Details[i].dbTId + ">下載作品pdf</span></td><td><p>產品：" + orderDetail[i].dbODProduct.dbPName.dbPNName + "</p><p>主題：<br/>" + Details[i].dbTTheme + "</p></td><td><span class='subtotal'>NT$</span>";
        orderList += "<span class='subtotal'>" + orderDetail[i].dbODProduct.dbPUnitPrice + "</span></td><td><p>" + orderDetail[i].dbODQty + "</p></td><td><span class='subtotal'>NT$</span>";
        orderList += "<span class='subtotal'>" + parseInt(orderDetail[i].dbODProduct.dbPUnitPrice) * parseInt(orderDetail[i].dbODQty) + "</span></td></tr></thead>";
        // productTotalAmount += parseInt(buylist[i].num) * parseInt(buylist[i].price);
    }
    $("#orderList").html(orderList);

    var freight = "";
    var freightFee = 80; //運費
    freight += "<tr><td style='text-align:right;' colspan='3'><label><b>運 費:</b></label></td><td style='text-align:center;'><span style='width:100px'>NT$</span><span style='width:100px'>&nbsp&nbsp" + freightFee + "</span></td></tr>";
    $("#freight").html(freight);

    var totalAmount = "";
    // var orderAmount = freightFee + productTotalAmount; //訂單總金額 = 運費 + 總計
    totalAmount += "<tr><td style='text-align:right;' colspan='3'><label><b>總 計:</b></label><td style='text-align:center;'><span style='width:100px'>NT$</span><span class='subtotal'>" + Order.dbOTotalPrice + "</span></td></tr>";
    $("#totalAmount").html(totalAmount);

    var payAndTransferMethod = "";
    payAndTransferMethod += "<thead><tr><th><div class='form-group'><label><h4><font color='#28a745'>付款方式</font></h4></label><hr><br>";
    if (orderDetail[0].dbODpaymentMethod == "歐付寶") {
        payAndTransferMethod += "<p>歐付寶</p><p>親愛的" + CustomerInfoStr.dbAName + "，謝謝您的購買！</p><p>客服專線:0800-000-000</p>";
    } else {
        payAndTransferMethod += "<p>歐付寶</p><p>親愛的" + CustomerInfoStr.dbAName + "，謝謝您的購買！</p><p>客服專線:0800-000-000</p>";
    }
    // else if(orderDetail[0].dbODpaymentMethod == "ATM轉帳") {
    //     payAndTransferMethod += "<p>ATM 代號:008</p><p>ATM 銀行:玉山銀行</p><p>ATM 帳號:156789087234</p>";
    // } else if (orderDetail[0].dbODpaymentMethod == "貨到付款") {
    //     payAndTransferMethod += "<p>貨到付款</p><p> </p><p> </p>";
    //     // payAndTransferMethod += "<p>ATM 代號:008</p><p>ATM 銀行:玉山銀行</p><p>ATM 帳號:156789087234</p>";
    // } else if (orderDetail[0].dbODpaymentMethod == "線上刷卡") {
    //     payAndTransferMethod += "<p>線上刷卡</p><p> </p><p> </p>";
    //     // payAndTransferMethod += "<p>ATM 代號:008</p><p>ATM 銀行:玉山銀行</p><p>ATM 帳號:156789087234</p>";
    // }
    payAndTransferMethod += "</div></th><th><div class='form-group'><label><h4><font color='#28a745'>配送資訊</font></h4></label><hr><label style='display:inline-block;width:100px;'>配送方式：</label><span>" + orderDetail[0].dbODtransferMethod + "</span>";
    payAndTransferMethod += "<br><label style='display:inline-block;width:100px;'>收貨時間：</label><span>" + CustomerInfoStr.dbAPickupDetail.split("/")[1] + "</span><br><label style='display:inline-block;width:100px;'>出貨狀況：</label><span>待出貨</span></thead>";
    $("#payAndTransferMethod").html(payAndTransferMethod);

    $(".download").click(function() {
        var dbTId = $(this).attr("id");
        //        alert(dbTId);
        //        alert(sessionStorage.getItem("acc"));
        $.ajax({
            url: "printAllPDF.do",
            type: "POST",
            data: { Account: sessionStorage.getItem("acc"), dbTId: dbTId },
            success: function(data) {
                var a = document.createElement('a');
                a.href = '/FinalProject/PDF/' + data;
                a.download = data;
                setTimeout(function() {
                    a.click();
                }, 5000)
            }
        })
    })

} //end of ShowOrderDetail(buyjsonstr)