
//TODO: use Flickr API to get pictures by tags.
//      until then, use this mockup object.
var data = {
	"collection" : [
			{
				"name" : "Toad 1",
				"url" : "dog/puppy_01.png",
			},
			{
				"name" : "Toad 2",
				"url" : "dog/puppy_02.png",
			},
			{
				"name" : "Toad 3",
				"url" : "dog/puppy_03.png",
			},
			{
				"name" : "Toad 4",
				"url" : "dog/puppy_04.png",
			},
			{
				"name" : "Toad 5",
				"url" : "dog/puppy_05.png",
			},
			{
				"name" : "Toad 6",
				"url" : "dog/puppy_06.png",
			},
			{
				"name" : "Toad 7",
				"url" : "dog/puppy_07.png",
			},

			{
				"name" : "Toad 8",
				"url" : "dog/puppy_08.png",
			},]}

// Data Bind
for (var i = 0; i < data.collection.length; i += 1) {
	$("<li><image src='{0}' alt='{1}' width='130px' height='130px'/></li>".replace(/\{0}/,
					data.collection[i].url).replace(/\{1}/,
					data.collection[i].name)).appendTo($("#negative-film"));
}
