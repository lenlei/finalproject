var dbPId, dbTId, YearMonth; //產品編號,暫存作品編號
window.onload = function() {
        var dbAAccount = sessionStorage.getItem("acc"); //取得使用者的登入email
        window.userCart = "Cart_" + dbAAccount;
        if (dbAAccount == null) {
            var conf = confirm('請先登入');
            if (conf) {
                window.location.href = "login.html"; //跳轉到登入頁面
            }
        }
        var url = window.location.search;
        if (url.indexOf("?") != -1) {
            var str = url.substr(1);
            strs = str.split("&");
            var key = new Array(strs.length);
            var value = new Array(strs.length);
            for (i = 0; i < strs.length; i++) {
                key[i] = strs[i].split("=")[0]
                value[i] = unescape(strs[i].split("=")[1]);
                // alert(key[i] + "=" + value[i]);
                if (key[i] == "dbPId") {
                    dbPId = value[i]; //取得產品編號
                } else if (key[i] == "dbTId") {
                    dbTId = value[i]; //取得暫存作品編號
                }
            }
        }
        var jsonstrCart = JSON.parse(localStorage.getItem(userCart))
        console.log(jsonstrCart); //放在localStorage的購物車內容
        var productlist = jsonstrCart.productlist;
        for (var i in productlist) {
            if (productlist[i].dbTId == dbTId) {
                YearMonth = productlist[i].startMonth;
            }
        }
        // alert("YearMonth====" + YearMonth);//ok
        YearMonth = (YearMonth == null) ? "201901" : YearMonth;
        productsPreview(dbPId, dbTId, YearMonth); //1~12月的預覽圖框
        $("#finishProduct").click(function() {
            if (window.isFinished == false) {
                alert(UnFinishedMonth + "\n尚未完成編輯喔!");
            } else {
                $.ajax({
                        url: "updateTempWorkStatus",
                        type: "POST",
                        data: {
                            dbTPId: dbTId
                        }, //暫存作品編號
                        async: false,
                        success: function(data) {}
                    }) //end of ajax
                var jsonstr = JSON.parse(localStorage.getItem(userCart));
                for (var i in jsonstr.productlist) {
                    if (jsonstr.productlist[i].dbTId == dbTId) {
                        jsonstr.productlist[i].dbTStatus = 9; //將狀態改為9 作品完成
                        jsonstr.productlist[i].isFinished = true;
                    }
                }
                localStorage.setItem(userCart, JSON.stringify(jsonstr));
                console.log(JSON.parse(localStorage.getItem(userCart)));
                window.location.href = "sop_MyCart.html";
            }
        })
        $(window).bind('beforeunload', function() {
            var jsonstr = JSON.parse(localStorage.getItem(userCart));
            for (var i in jsonstr.productlist) {
                if (jsonstr.productlist[i].dbTId == dbTId) {
                    jsonstr.productlist[i].dbTTheme = $("#dbTTheme").val().trim();
                    jsonstr.productlist[i].dbTIntroduction = $("#dbTIntroduction").val().trim();
                    jsonstr.productlist[i].TempImgs = window.previewImgsArray;
                }
            }
            localStorage.setItem(userCart, JSON.stringify(jsonstr));
            $.ajax({
                url: "saveTempIntroduction",
                type: "POST",
                data: {
                    dbTId: dbTId, //暫存作品編號
                    dbTTheme: $("#dbTTheme").val().trim(), //主題
                    dbTIntroduction: $("#dbTIntroduction").val().trim() //作品說明
                },
                success: function(data) {}
            })
        })
    } //end of window.onload

function productsPreview(dbPId, dbTId, YearMonth) {
    window.isFinished = false; //產品是否已經完成編輯? 先立旗標false
    window.FinishedMonth = [];
    window.UnFinishedMonth = [];
    window.previewImgsArray = [];
    $.ajax({
            url: "testAllPreviewImgs",
            type: "POST",
            data: {
                dbPId: dbPId,
                dbTPId: dbTId //暫存作品編號
            },
            async: false,
            success: function(data) {
                var allTempWorkInfos = $.parseJSON(data);
                console.log("得到的回傳值如下");
                console.log(allTempWorkInfos);
                window.dbTIntroduction = allTempWorkInfos.dbTIntroduction; //作品說明
                $("#dbTIntroduction").html(window.dbTIntroduction);
                window.dbTTheme = allTempWorkInfos.dbTTheme; //作品主題dbTTheme
                $("#dbTTheme").val(window.dbTTheme);
                window.dbPImage = allTempWorkInfos.product.dbPImage.split(","); //1~12月預設圖框底圖的檔名陣列
                window.dbPTypeImage = allTempWorkInfos.product.dbPTypeImage; //風格圖片
                window.dbPType = allTempWorkInfos.product.dbPType.split("/")[0]; //作品風格
                $("#dbPType").html(window.dbPType);
                console.log("使用者編輯中的預覽圖如下");
                console.log(allTempWorkInfos.previewImgs);
                var previewImgs = allTempWorkInfos.previewImgs;
                previewImgsArray = previewImgsArray.concat(dbPImage); //先將所有1~12月份預設圖框的檔名放到 陣列previewImgsArray
                if (previewImgs != null && previewImgs.length != 0) { //如果不為空，代表有編輯中的預覽圖
                    for (var i in previewImgs) {
                        var idxReplaced = parseInt(previewImgs[i].dbTPage) - 1; //被替換掉的索引值
                        previewImgsArray.splice(idxReplaced, 1, previewImgs[i].dbTPageImage.split(",")[0]);
                        FinishedMonth.push(idxReplaced);
                    }
                }
                if (previewImgs.length >= 12) { //代表使用者已經完成12個月份的編輯
                    window.isFinished = true;
                    var jsonstrCart = JSON.parse(localStorage.getItem(userCart));
                    var productlist = jsonstrCart.productlist;
                    for (var i in productlist) {
                        if (productlist[i].dbTId == dbTId) {
                            productlist[i].dbTStatus = 9;
                            localStorage.setItem(userCart, JSON.stringify(jsonstrCart));
                            console.log("如果previewImgs 大於12則設定status=9");
                            console.log(JSON.parse(localStorage.getItem(userCart)));
                        }
                    }
                }
                showImg(previewImgsArray, YearMonth);
            }
        }) //end of Ajax
} //end of productsPreview
function showImg(previewImgsArray, YearMonth) {
    console.log("previewImgsArray如下");
    console.log(previewImgsArray);
    var Year = YearMonth.slice(0, 4);
    var Month = YearMonth.slice(4);
    var ImgUrlStr = "../Images/";
    var month = ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"];
    UnFinishedMonth = UnFinishedMonth.concat(month);
    for (var idx = FinishedMonth.length - 1; idx >= 0; idx--) {
        UnFinishedMonth.splice(FinishedMonth[idx], 1);
    }
    var txt = "<ul class='clearfix row' style='padding:15px;'>";
    for (var i in month) {
        txt += "<li class='col-3 product'><div class='info'>";
        //原本的寫法↓↓↓↓↓
        // txt += "<b>" + month[i] + "</b></div><div class='thumbnail' style='border:1px #cccccc solid;' id='" + (parseInt(i) + 1) + "'>";
        // txt += "<img src='" + ImgUrlStr + previewImgsArray[i] + "' width='320' height='320' border='0'></div></li>";
        //新的寫法↓↓↓↓↓
        // txt += "<b>" + month[i] + "</b></div><div class='thumbnail' style='background-image: url(" + ImgUrlStr + previewImgsArray[i] + "); height:150px;background-size:contain;background-position:center;background-image:burlywood; border:1px #cccccc solid;' id='" + (parseInt(i) + 1) + "'>";
        txt += "<b>" + month[i] + "</b></div><div class='thumbnail' style='background-image: url(" + ImgUrlStr + previewImgsArray[i] + "); height:120px;background-size:contain;background-position:center;background-repeat: no-repeat; border:1px #cccccc solid;' id='" + (parseInt(i) + 1) + "'>";
    }
    // <div style="background-image: url(" "); height:150px;background-size:cover;background-position:center;background-image:burlywood;"></div>
    txt += "</ul>";
    $("#productsPreview").html(txt);
    $("div.thumbnail").click(function() {
        var MonthClicked = $(this).attr("id");
        var tempInfo = dbPId + "/" + dbTId + "/" + Year + "/" + MonthClicked;
        sessionStorage.setItem("tempInfo", tempInfo);
        console.log(sessionStorage.getItem("tempInfo"));
        window.location.href = "artboard.html";
    })
} //end of showImg