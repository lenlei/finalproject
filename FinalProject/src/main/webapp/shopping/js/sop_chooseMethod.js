// window.onload = function() {
$(document).ready(function() {
        dbAAccount = sessionStorage.getItem("acc"); //取得使用者的登入email
        var cityNames = [];
        for (var propName in TaiwanZipData) { //取得"縣市"名稱，push到cityNames陣列
            cityNames.push(propName);
        }

        if (dbAAccount != null && dbAAccount.length != 0) {
            $.ajax({
                    url: "showUserInfo",
                    type: "POST",
                    async: false,
                    data: { dbAAccount: dbAAccount },
                    success: function(data) {
                        console.log("success的data--------------");
                        console.log(data);
                        window.userInfo = $.parseJSON(data);
                        if (userInfo.dbAName != null && userInfo.dbAMobile != null && userInfo.dbAPickpCity != null && userInfo.dbAPickupRegion != null && userInfo.dbAPickupDetail != null) {
                            $("#account").val(userInfo.dbAName);
                            $("#tel").val(userInfo.dbAMobile);
                            var initCity = userInfo.dbAPickpCity.dbCName;
                            var initRegion = userInfo.dbAPickupRegion.dbRName;
                            var initAddr = userInfo.dbAPickupDetail;
                            ko.applyBindings(new ShowCityAndRegion(cityNames, initCity, initRegion, initAddr)); // knockout綁定
                        } else {
                            window.userInfo = {
                                "dbAName": "",
                                "dbAMobile": "",
                                "dbAPickpCity": {
                                    "dbCid": "",
                                    "dbCName": ""
                                },
                                "dbAPickupRegion": {
                                    "dbRid": "",
                                    "dbRName": ""
                                },
                                "dbAPickupDetail": ""
                            }
                            ko.applyBindings(new ShowCityAndRegion(cityNames, "", "", ""));
                        }
                        console.log(userInfo);
                        // return userInfo;
                    }
                }) //end of Ajax  
        } else {
            var conf = confirm('請先登入');
            if (conf) {
                window.location.href = "login.html"; //跳轉到登入頁面
            }
        }
        var userCart = "Cart_" + dbAAccount;
        var userBuy = "Buy_" + dbAAccount;
        var userBuyDetail = "BuyDetail_" + dbAAccount;
        console.log("userCart--------");
        console.log(JSON.parse(localStorage.getItem(userCart)));
        console.log("userBuy--------");
        console.log(JSON.parse(localStorage.getItem(userBuy)));
        console.log("userBuyDetail--------");
        console.log(JSON.parse(localStorage.getItem(userBuyDetail)));
        var url = window.location.search;
        if (url.indexOf("?") != -1) {
            var str = url.substr(1);
            strs = str.split("&");
            var key = new Array(strs.length);
            var value = new Array(strs.length);
            for (i = 0; i < strs.length; i++) {
                key[i] = strs[i].split("=")[0]
                value[i] = unescape(strs[i].split("=")[1]);
                // alert(key[i] + "=" + value[i]);
                if (key[i] == "oid") {
                    window.dbOId = value[i]; //取得訂單編號  
                }
            }
        }

        $("#GiveMeMoney").click(function() {
            var dbAName = $("#account").val();
            var dbAMobile = $("#tel").val();
            var dbAPickupDetail = $("#address").val();
            var pickUpTime = $("[name='pickUpTime']:checked").val();
            var dbODpaymentMethod = $("[name='pay']:checked").val();
            var dbODtransferMethod = $("[name='transferMethod']:checked").val();
            var dbOTotalPrice;
            // var allAmountPrice = 0;
            // $.each(JSON.parse(localStorage.getItem(userBuyDetail)), function(index, orderdetail) {
            //         allAmountPrice += parseInt(orderdetail.dbODAmount)
            //     })
            console.log("訂單編號" + dbOId);
            if (dbAName != null && dbAName.length != 0 &&
                dbAMobile != null && dbAMobile.length != 0 &&
                dbAPickupDetail != null && dbAPickupDetail.length != 0 &&
                pickUpTime != null && pickUpTime.length != 0 &&
                dbODpaymentMethod != null && dbODpaymentMethod.length != 0 &&
                dbODtransferMethod != null && dbODtransferMethod.length != 0
            ) { //確定所有欄位的資料都有輸入
                var orderInfo = JSON.parse(localStorage.getItem(userBuy));
                console.log("orderInfo如下");
                console.log(orderInfo);
                if (orderInfo.dbOId == dbOId) {
                    dbOTotalPrice = orderInfo.dbOTotalPrice;
                }
                window.orderDetail = JSON.parse(localStorage.getItem(userBuyDetail));
                if (orderDetail == null || orderDetail.length == 0) {
                    alert("無訂單資訊可顯示");
                } else {
                    var y = new Date();
                    userInfo.dbAAccount = dbAAccount;
                    userInfo.dbAName = dbAName;
                    userInfo.dbAMobile = dbAMobile;
                    // orderInfo.dbAPickupDetail = dbAPickupDetail;
                    var CityChoosed = $("#CityChoosed").find("option:selected").text();
                    var RegionChoosed = $("#RegionChoosed").find("option:selected").text();
                    var dbCid = cityNames.indexOf(CityChoosed);
                    console.log(dbCid);
                    var dbRid = TaiwanZipData[CityChoosed][RegionChoosed];
                    console.log(dbRid);
                    userInfo.dbAPickpCity.dbCid = dbCid;
                    userInfo.dbAPickpCity.dbCName = CityChoosed;
                    userInfo.dbAPickupRegion.dbRid = dbRid;
                    userInfo.dbAPickupRegion.dbRName = RegionChoosed;
                    // userInfo.dbAPickupDetail = ("" + dbAPickupDetail).slice(6) + "/" + pickUpTime;
                    userInfo.dbAPickupDetail = dbAPickupDetail + "/" + pickUpTime;
                    for (var i = 0; i < orderDetail.length; i++) {
                        orderDetail[i].dbODpaymentMethod = dbODpaymentMethod;
                        orderDetail[i].dbODtransferMethod = dbODtransferMethod;
                    }
                    // orderInfo.dbOorderDate = y.getFullYear() + "年" + (y.getMonth() + 1) + "月" + y.getDate() + "日 " + y.getHours() + "點" + y.getMinutes() + "分"; //購買日期
                    var CustomerInfo = "CustomerInfo_" + dbAAccount;
                    localStorage.setItem(CustomerInfo, JSON.stringify(userInfo));
                    localStorage.setItem(userBuyDetail, JSON.stringify(orderDetail));
                    console.log(JSON.parse(localStorage.getItem(userBuyDetail)));
                    $.ajax({
                        url: "updateOrderDetail",
                        // contentType: "application/json;charset=utf-8",
                        type: "POST",
                        // dataType: "json",
                        data: {
                            dbAAccount: dbAAccount,
                            dbAName: dbAName,
                            dbAMobile: dbAMobile,
                            dbCid: dbCid,
                            dbRid: dbRid,
                            dbAPickupDetail: String(dbAPickupDetail).slice(6),
                            dbOId: dbOId,
                            paymentMethod: $("[name='pay']:checked").val(),
                            transferMethod: $("[name='transferMethod']:checked").val()
                        },
                        success: function(data) {
                            if (data > 0) {
                                var oid = data;
                                $.ajax({
                                    url: "opayPayment.do",
                                    type: "POST",
                                    // data: { oid: oid, allPrice: allAmountPrice },
                                    data: { oid: oid, allPrice: dbOTotalPrice },
                                    success: function(data) {
                                        $("#oPay").html(data);
                                    }
                                })
                            }
                        }
                    })
                }
            } else if (dbAName == null || dbAName.length == 0) {
                alert("請輸入姓名");
            } else if (dbAMobile == null || dbAMobile.length == 0) {
                alert("請輸入手機號碼");
            } else if (dbAPickupDetail == null || dbAPickupDetail.length == 0) {
                alert("請輸入取貨地址");
            } else if (pickUpTime == null || pickUpTime.length == 0) {
                alert("請選擇收貨時間");
            } else if (dbODpaymentMethod == null || dbODpaymentMethod.length == 0) {
                alert("請選擇付款方式");
            } else if (dbODtransferMethod == null || dbODtransferMethod.length == 0) {
                alert("請選擇配送方式");
            }
        })
    }) // end of ready

function ShowCityAndRegion(cityNames, initCity, initRegion, initAddr) {
    var self = this;
    self.cities = ko.observableArray(cityNames);
    // self.city = ko.observable("" + initCity);
    if (initCity != null || initCity.length != 0) {
        self.city = ko.observable(initCity);
    } else {
        self.city = ko.observable();
    }
    self.areas = ko.computed(function() {
        var areaData = TaiwanZipData[self.city()];
        var options = [];
        if (areaData) {
            for (var propName in areaData) {
                options.push({
                    // value: propName + areaData[propName],//原本寫法(含郵遞區號)
                    value: propName,
                    text: propName
                });
            }
        }
        return options;
    }); // end of self.areas
    if (initRegion != null || initRegion.length != 0) {
        self.areaZip = ko.observable(initRegion);
    } else {
        self.areaZip = ko.observable();
    }
    // window.dbRid = TaiwanZipData[self.city()][self.areaZip];
    if (initAddr != null || initAddr.length != 0) {
        self.detail = ko.observable(initAddr);
    } else {
        self.detail = ko.observable();
    }
    self.addrPrefix = ko.computed({
        read: function() {
            return ((self.city() || "") + (self.areaZip() || "") + (self
                .detail() || "")).trim();
        },
        write: function(value) {
            if (value.length > 3) {
                self.city(value.trim().substr(0, 3));
                self.areaZip(value.trim().substr(3, 3));
                self.detail(value.trim().substr(6));
            }
        }
    }); // end of addrPrefix
} // end of ShowCityAndRegion

document.addEventListener("DOMContentLoaded", function() {
    document.getElementById("account").addEventListener("blur", chkName); // 事件繫結，焦點離開
    document.getElementById("tel").addEventListener("blur", checkTel);
    document.getElementById("address").addEventListener("blur", chkAddr);
});

function chkName() {
    var theName = document.getElementById("account").value.trim();
    if (theName == null || theName.length == "") {
        document.getElementById("chkName").innerHTML = "<img src='images/error.png'><span class='red'>姓名不可空白</span>";
    } else {
        document.getElementById("chkName").innerHTML = "<span id='chkName'></span>";
    }
} // end of chkName

function checkTel() {
    var tel = document.getElementById("tel").value;
    // var re = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[A-Za-z]).{6,}$/;
    // var re = /^/d{8}$/;
    if (tel == null || tel.length == "") {
        document.getElementById("chkTel").innerHTML = "<img src='images/error.png'><span class='red'>電話欄位不可空白</span>";
    } else if (tel.length < 10) {
        document.getElementById("chkTel").innerHTML = "<img src='images/error.png'><span class='red'>請輸入10碼手機號碼</span>";
    } else {
        document.getElementById("chkTel").innerHTML = "<span id='chkTel'></span>";
    }
} // end of function checkTel

function chkAddr() {
    var address = document.getElementById("address").value;
    if (address == null || address.length == "") {
        document.getElementById("chkAddr").innerHTML = "<img src='images/error.png'><span class='red'>地址欄為不可空白</span>";
    } else if (address.length < 10) {
        document.getElementById("chkAddr").innerHTML = "<img src='images/error.png'><span class='red'>請輸入詳細地址</span>";
    } else {
        document.getElementById("chkAddr").innerHTML = "<span id='chkAddr'></span>";
    }
} // end of function chkAdd;