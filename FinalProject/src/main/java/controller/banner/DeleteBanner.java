package controller.banner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import model.BannerBean;
import model.banner.BannerService;

@Controller
public class DeleteBanner {
	@Autowired
	private BannerService bs;
	@RequestMapping(path="/backstage/DeleteBanner.do")
	public void deleteBanner(BannerBean bb) {
			bs.delete(bb);
	}
	
	
}
