package controller.banner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import model.BannerBean;
import model.banner.BannerService;

@Controller
public class ListAllBanner {
	@Autowired
	private BannerService bs;
	
	@RequestMapping(path="/backstage/ListAllBanner.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String listAllBanner() {
		return new Gson().toJson(bs.selectAll());
	}

	@RequestMapping(path="/backstage/selectBannerType.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String selectBannerType(BannerBean bb) {
		return new Gson().toJson(bs.selectType(bb));
//		return new Gson().toJson(bs.selectAll());
	}
	
}
