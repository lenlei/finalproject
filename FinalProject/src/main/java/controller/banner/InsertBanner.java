package controller.banner;

import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import model.BannerBean;
import model.StatusBean;
import model.banner.BannerService;

@Controller
public class InsertBanner {
	//Save the uploaded file to this folder
//	private static String UPLOADED_FOLDER = "C:\\FinalProject\\Repository\\FinalProject\\src\\main\\webapp\\Images\\";
	private static String UPLOADED_FOLDER = "C:\\Users\\user\\git\\repository\\FinalProject\\src\\main\\webapp\\Images\\";
	@Autowired
	private BannerService bs;
	@RequestMapping(path="/backstage/insertBanner.do",method=RequestMethod.POST)
	public String insertBanner(BannerBean bb, MultipartFile file, String dbSId) {
		
		bb.setDbBPicture(file.getOriginalFilename());
		try {
			bb.setDbBSort((bs.selectSort(bb)+1));
		}catch(Exception e) {
			e.printStackTrace();
			bb.setDbBSort(1);
		}
		
		try {
			file.transferTo(new File(UPLOADED_FOLDER+file.getOriginalFilename()));
//			file.transferTo(new File(UPLOADED_FOLDER+new Date().getTime()+file.getOriginalFilename()));
		} catch (IllegalStateException | IOException e) {
			e.printStackTrace();
		}
		
		if(dbSId==null) {
			bb.setDbBStatus(new StatusBean((byte)1, null));
		}else {			
			bb.setDbBStatus(new StatusBean((byte)2, null));
		}
		
		bs.insert(bb);
		return "bannerIndex";
	}
}
