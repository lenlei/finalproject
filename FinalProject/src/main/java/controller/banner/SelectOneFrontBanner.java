package controller.banner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import model.BannerBean;
import model.banner.BannerService;

@Controller
public class SelectOneFrontBanner {
	@Autowired
	private BannerService bs;

	@RequestMapping(path = "/decor/SelectOneBanner.do", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String selectPictureAndSort(BannerBean bb) {
		System.out.println("33333333333");
		return new Gson().toJson(bs.selectPicture(bb));
	}
	@RequestMapping(path = "/shopping/SelectOneBanner.run", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String selectPictureAndSort2(BannerBean bb) {
		System.out.println("33333333333");
		return new Gson().toJson(bs.selectPicture(bb));
	}
//	@RequestMapping(path = "/shopping/SelectOneBanner.run", produces = "text/html;charset=UTF-8")
//	@ResponseBody
//	public String selectPictureAndSort2() {
//		System.out.println("33333333333");		return new Gson().toJson(bs.selectPicture2());
//	}
}
