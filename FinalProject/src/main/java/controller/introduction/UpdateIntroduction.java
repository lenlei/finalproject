package controller.introduction;

import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import model.CompanyAddressBean;
import model.CompanyEmailBean;
import model.CompanyIntroductionBean;
import model.CompanyProcessPicBean;
import model.CompanyProductProcessBean;
import model.CompanySiteMapBean;
import model.CompanyTelBean;
import model.IntroductionSetBean;
import model.StatusBean;
import model.introduction.IntroductionService;

@Controller
public class UpdateIntroduction {
	//Save the uploaded file to this folder
//	private static String UPLOADED_FOLDER = "C:\\Users\\user\\git\\repository\\FinalProject\\src\\main\\webapp\\shopping\\p-picture";
	private static String UPLOADED_FOLDER = "C:\\Users\\user\\git\\repository\\FinalProject\\src\\main\\webapp\\Images\\";	
	
		@Autowired
		private IntroductionService is;
		
		@RequestMapping(value="/updateintro.do",method=RequestMethod.POST)
		//public String update( String dbISIntroduction,String dbSId,String dbISTel,String dbISEmail,String dbISAddress,String dbISProductprocess,String dbISProcesspics,String dbISitemap) {
		public String update( String dbISIntroduction,String dbSIda,String dbISTel,String dbSIdb,String dbISEmail,String dbSIdc,String dbISAddress,String dbSIdd,String dbISProductprocess,String dbSIde,String dbISProcesspics,MultipartFile file,String dbSIdf,String dbISitemap,String dbSIdg) {
		//public String update( String dbISIntroduction,String dbSIda,String dbISTel,String dbSIdb,String dbISEmail,String dbSIdc,String dbISAddress,String dbSIdd,String dbISProductprocess,String dbSIde,String dbISProcesspics,MultipartFile file,String dbSIdf,String dbISitemap,String dbSIdg) {
			System.out.println("============================hahhahaha");
			//關於我們
			System.out.println("dbISIntroduction"+dbISIntroduction);
			System.out.println("dbSIda"+dbSIda);
			
			//電話
			System.out.println("dbISTel"+dbISTel);
			System.out.println("dbSIdb"+dbSIdb);
			
			//信箱
			System.out.println("dbISEmail"+dbISEmail);
			System.out.println("dbSIdc"+dbSIdc);
	
			//地址
			System.out.println("dbISEmail"+dbISEmail);
			System.out.println("dbSIdd"+dbSIdd);
			//產品製作流程
			System.out.println("dbISProductprocess"+dbISProductprocess);
			System.out.println("dbSIde"+dbSIde);
		
			//sop圖
			System.out.println("dbISProcesspics"+dbISProcesspics);
			System.out.println("dbSIdf"+dbSIdf);
		
			//網站地圖
			System.out.println("dbISitemap"+dbISitemap);
			System.out.println("dbSIdg"+dbSIdg);

			CompanyIntroductionBean a = null;
			CompanyTelBean b = null;
			CompanyEmailBean c=null;
			CompanyAddressBean d =null;
			CompanyProductProcessBean e =null;
			CompanyProcessPicBean f =new CompanyProcessPicBean();
			CompanySiteMapBean g =null;
			//關於我們
			if(dbSIda!=null) {
				a = new CompanyIntroductionBean(null,dbISIntroduction,new StatusBean((byte)2,null));
			}else {
				a = new CompanyIntroductionBean(null,dbISIntroduction,new StatusBean((byte)1,null));
			}
			//電話
			if(dbSIdb!=null) {
				b = new CompanyTelBean(null,dbISTel,new StatusBean((byte)2,null));
			}else {
				b = new CompanyTelBean(null,dbISTel,new StatusBean((byte)1,null));				
			}
			//信箱
			if(dbSIdc!=null) {
				c= new CompanyEmailBean(null,dbISEmail,new StatusBean((byte)2,null));
			}else {
				c= new CompanyEmailBean(null,dbISEmail,new StatusBean((byte)1,null));
			}
			//地址
			if(dbSIdd!=null) {
				d=new CompanyAddressBean(null,dbISAddress,new StatusBean((byte)2,null));
			}else {
				d=new CompanyAddressBean(null,dbISAddress,new StatusBean((byte)1,null));
			}
			//產品製作流程
			if(dbSIde!=null) {
				e=new CompanyProductProcessBean(null,dbISProductprocess,new StatusBean((byte)2,null));
			}else {
				e=new CompanyProductProcessBean(null,dbISProductprocess,new StatusBean((byte)1,null));
			}
			//sop圖
			System.out.println(file.getOriginalFilename());
			f.setDbIProcesspics(file.getOriginalFilename());
			
			try {
				file.transferTo(new File(UPLOADED_FOLDER+file.getOriginalFilename()));
			}catch(IllegalStateException | IOException e1) {
				e1.printStackTrace();
			}
			//sop圖按鈕
			if(dbSIdf!=null) {
				f.setDbIStatus(new StatusBean((byte)2,null));
			}else {
				f.setDbIStatus(new StatusBean((byte)1,null));
		
			}
			//網站地圖
			if(dbSIdg!=null) {
				g= new CompanySiteMapBean(null,dbISitemap,new StatusBean((byte)2,null));
			}else {
				g= new CompanySiteMapBean(null,dbISitemap,new StatusBean((byte)1,null));
			}
			
			IntroductionSetBean ib =new IntroductionSetBean();
			ib.setDbISIntroduction(a);
			ib.setDbISTel(b);
			ib.setDbISEmail(c);
			ib.setDbISAddress(d);
			ib.setDbISProductprocess(e);
			ib.setDbISProcesspics(f);
			ib.setDbISitemap(g);
			is.updatealldetail(ib);
			
//
//			if(dbSIda!=null && dbSIdb!=null && dbSIdc!=null && dbSIdd!=null && dbSIde!=null && dbSIdf!=null && dbSIdg!=null) {
//			is.updatealldetail(new IntroductionSetBean(null, new CompanyIntroductionBean(null,dbISIntroduction,new StatusBean((byte)2,null)),
//					          						   new CompanyTelBean(null,dbISTel,new StatusBean((byte)2,null)),
//					          						   new CompanyEmailBean(null,dbISEmail,new StatusBean((byte)2,null)),
//					          						   new CompanyAddressBean(null,dbISAddress,new StatusBean((byte)2,null)),
//					          						   new CompanyProductProcessBean(null,dbISProductprocess,new StatusBean((byte)2,null)),
//					          						   new CompanyProcessPicBean(null,dbISProcesspics,new StatusBean((byte)2,null)),
//					          						   new CompanySiteMapBean(null,dbISitemap,new StatusBean((byte)2,null))));
//			}
//			
			
			return "introduction";
	

		}
}
