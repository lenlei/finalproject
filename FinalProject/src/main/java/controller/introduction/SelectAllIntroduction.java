package controller.introduction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import model.introduction.IntroductionService;

@Controller
public class SelectAllIntroduction {
	@Autowired
	private IntroductionService Is;
	//後台抓值
	
	@RequestMapping(path="/backstage/ListAll.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String selectAllIntroduction() {
//		return new Gson().toJson(Is.selectAll());
//		System.out.println(new JSONObject(Is.selectAll()));
//		System.out.println(new JSONObject(Is.selectAll());
		return new Gson().toJson(Is.selectAllback());
	
	}

}

