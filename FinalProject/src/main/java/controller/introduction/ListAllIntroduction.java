package controller.introduction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import model.introduction.IntroductionService;

@Controller
public class ListAllIntroduction {
	@Autowired
	private IntroductionService Is;
	//前台抓值
	
	@RequestMapping(path="/shopping/ListAllIntroduction.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String listAlldata() {
		System.out.println(new Gson().toJson(Is.selectAll()));
		System.out.println("1111");
		return new Gson().toJson(Is.selectAll());
	}

}
