package controller.licensegroup;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import model.LicenseGroupBean;
import model.StatusBean;
import model.licensegroup.LicenseGroupService;

@Controller
public class InsertLicenseGroup {

	@Autowired
	private LicenseGroupService lgs;
	@RequestMapping(path="/backstage/insertLG.do",method=RequestMethod.POST)
	public String insertLicenseGroup(String dbLGName ,String dbSId
			,String dbLGSetBanner,String dbLGSetProd,String dbLGSetShare
			,String dbLGSetComProfile,String dbLGSetPopProd
			,String dbLGSetCust,String dbLGSetOrder,String dbLGSetSEO
			,String dbLGSetLic) {
		System.out.println("-------------");
		System.out.println("@@@@@@@@"+dbLGName);
		System.out.println("@@@@@@@@"+dbSId);
		System.out.println("@@@@@@@@"+dbLGSetBanner);
		System.out.println("@@@@@@@@"+dbLGSetProd);
		System.out.println("@@@@@@@@"+dbLGSetShare);
		System.out.println("@@@@@@@@"+dbLGSetComProfile);
		System.out.println("@@@@@@@@"+dbLGSetPopProd);
		System.out.println("@@@@@@@@"+dbLGSetCust);
		System.out.println("@@@@@@@@"+dbLGSetOrder);
		System.out.println("@@@@@@@@"+dbLGSetSEO);
		System.out.println("@@@@@@@@"+dbLGSetLic);
		
		LicenseGroupBean lgb = new LicenseGroupBean();
		lgb.setDbLGName(dbLGName);
		if(dbSId==null) {
			lgb.setDbLGStatus(new StatusBean((byte)1, null));
		}else {			
			lgb.setDbLGStatus(new StatusBean((byte)2, null));
		}
		
		if(dbLGSetBanner==null) {
			lgb.setDbLGSetBanner(new StatusBean((byte)1, null));
		}else {			
			lgb.setDbLGSetBanner(new StatusBean((byte)2, null));
		}
		
		if(dbLGSetProd==null) {
			lgb.setDbLGSetProd(new StatusBean((byte)1, null));
		}else {			
			lgb.setDbLGSetProd(new StatusBean((byte)2, null));
		}
		
		if(dbLGSetShare==null) {
			lgb.setDbLGSetShare(new StatusBean((byte)1, null));
		}else {			
			lgb.setDbLGSetShare(new StatusBean((byte)2, null));
		}
		
		if(dbLGSetComProfile==null) {
			lgb.setDbLGSetComProfile(new StatusBean((byte)1, null));
		}else {			
			lgb.setDbLGSetComProfile(new StatusBean((byte)2, null));
		}
		
		if(dbLGSetPopProd==null) {
			lgb.setDbLGSetPopProd(new StatusBean((byte)1, null));
		}else {			
			lgb.setDbLGSetPopProd(new StatusBean((byte)2, null));
		}
		
		if(dbLGSetCust==null) {
			lgb.setDbLGSetCust(new StatusBean((byte)1, null));
		}else {			
			lgb.setDbLGSetCust(new StatusBean((byte)2, null));
		}
		
		if(dbLGSetOrder==null) {
			lgb.setDbLGSetOrder(new StatusBean((byte)1, null));
		}else {			
			lgb.setDbLGSetOrder(new StatusBean((byte)2, null));
		}
		
		if(dbLGSetSEO==null) {
			lgb.setDbLGSetSEO(new StatusBean((byte)1, null));
		}else {			
			lgb.setDbLGSetSEO(new StatusBean((byte)2, null));
		}
		
		if(dbLGSetLic==null) {
			lgb.setDbLGSetLic(new StatusBean((byte)1, null));
		}else {			
			lgb.setDbLGSetLic(new StatusBean((byte)2, null));
		}
		
		lgs.insertLicenseGroup(lgb);
		
		return "licensegroupIndex";
	}
	
}
