package controller.licensegroup;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import model.LGmodel;
import model.LicenseGroupBean;
import model.licensegroup.LicenseGroupService;

@Controller
public class SelectLALG {

	@Autowired
	private LicenseGroupService lgs;

	@RequestMapping(value = "/backstage/ListAllLALG.do", produces = "text/html;charset=utf-8", method = {RequestMethod.POST })
	
	
	
//	@RequestMapping(path = "/backstage/ListAllLALG.do", method = RequestMethod.POST, produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String listAllLALG(Integer dbLGId, String dbLAAccount) {
		if (dbLAAccount == null) {
			//判斷後台帳號非法登入則連結到登入頁
			return "loginfail";
		} else {
			//執行後台帳號的功能權限
			LicenseGroupBean lgb = new LicenseGroupBean();
			lgb.setDbLGId(dbLGId);
			LicenseGroupBean data = lgs.selectLGUpdate(lgb);
			String txt = "";
			String txt1 = "";
			if (data.getDbLGStatus().getDbSId() == 2) {
				if (data.getDbLGSetBanner().getDbSId() == 2) {
					txt += "<a class=\"nav-link nav-link\" data-toggle=\"dropdown\"";
					txt += "href=\"BackStage_Banner.html\" role=\"button\" aria-haspopup=\"true\"";
					txt += "aria-expanded=\"false\" style=\"font-family: 微軟正黑體 ;color:#00AA00\">Banner管理</a>&nbsp;&nbsp;&nbsp;";
				} else {
					txt += "<div style=\"display:none\"> <a class=\"nav-link nav-link\" data-toggle=\"dropdown\"";
					txt += "href=\"BackStage_Banner.html\" role=\"button\" aria-haspopup=\"true\"";
					txt += "aria-expanded=\"false\">Banner管理</a></div>";
				}

				if (data.getDbLGSetProd().getDbSId() == 2) {
					txt += "<a class=\"nav-link nav-link\"";
					txt += "data-toggle=\"dropdown\" href=\"BackStage_Product.html\" role=\"button\"";
					txt += "aria-haspopup=\"true\" aria-expanded=\"false\" style=\"font-family: 微軟正黑體 ;color:#00AA00 \">產品介紹管理</a>&nbsp;&nbsp;&nbsp;";
				} else {
					txt += "<div style=\"display:none\"> <a class=\"nav-link nav-link\"";
					txt += "data-toggle=\"dropdown\" href=\"BackStage_Product.html\" role=\"button\"";
					txt += "aria-haspopup=\"true\" aria-expanded=\"false\">產品介紹管理</a></div>";
				}

//				if (data.getDbLGSetShare().getDbSId() == 2) {
//					txt += "<a class=\"nav-link nav-link\" data-toggle=\"dropdown\"";
//					txt += "href=\"BackStage_Work.html\" role=\"button\" aria-haspopup=\"true\"";
//					txt += "aria-expanded=\"false\" style=\"font-family: 微軟正黑體 ;color:#00AA00 \">作品分享管理</a>&nbsp;&nbsp;&nbsp;";
//				} else {
//					txt += "<div style=\"display:none\"><a class=\"nav-link nav-link\" data-toggle=\"dropdown\"";
//					txt += "href=\"BackStage_Work.html\" role=\"button\" aria-haspopup=\"true\"";
//					txt += "aria-expanded=\"false\">作品分享管理</a></div>&nbsp;&nbsp;&nbsp;";
//				}

				if (data.getDbLGSetComProfile().getDbSId() == 2) {
					txt += "<a class=\"nav-link nav-link\"";
					txt += "data-toggle=\"dropdown\" href=\"BackStage_Introduction.html\"";
					txt += "role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\" style=\"font-family: 微軟正黑體 ;color:#00AA00 \">公司介紹管理</a>&nbsp;&nbsp;&nbsp;";
				} else {
					txt += "<div style=\"display:none\"><a class=\"nav-link nav-link\"";
					txt += "data-toggle=\"dropdown\" href=\"BackStage_Introduction.html\"";
					txt += "role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">公司介紹管理</a></div>";
				}

				if (data.getDbLGSetPopProd().getDbSId() == 2) {
					txt += "<a class=\"nav-link nav-link\"";
					txt += "data-toggle=\"dropdown\" href=\"BackStage_Plugin.html\"";
					txt += "role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\" style=\"font-family: 微軟正黑體 ;color:#00AA00 \">插件圖庫管理</a>&nbsp;&nbsp;&nbsp;";
				} else {
					txt += "<div style=\"display:none\"><a class=\"nav-link nav-link\"";
					txt += "data-toggle=\"dropdown\" href=\"BackStage_Plugin.html\"";
					txt += "role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">插件圖庫管理</a></div>";
				}

				if (data.getDbLGSetCust().getDbSId() == 2) {
					txt += "<a class=\"nav-link nav-link\"";
					txt += "data-toggle=\"dropdown\" href=\"BackStage_AccountUser.html\"";
					txt += "role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\" style=\"font-family: 微軟正黑體 \">客戶帳號管理</a>&nbsp;&nbsp;&nbsp;";
				} else {
					txt += "<div style=\"display:none\"><a class=\"nav-link nav-link\"";
					txt += "data-toggle=\"dropdown\" href=\"BackStage_AccountUser.html\"";
					txt += "role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">客戶帳號管理</a></div>";
				}

				if (data.getDbLGSetOrder().getDbSId() == 2) {
					txt += "<a class=\"nav-link nav-link\" data-toggle=\"dropdown\"";
					txt += "href=\"BackStage_Order.html\" role=\"button\" aria-haspopup=\"true\"";
					txt += "aria-expanded=\"false\" style=\"font-family: 微軟正黑體 \">訂單管理</a>&nbsp;&nbsp;&nbsp;";
				} else {
					txt += "<div style=\"display:none\"><a class=\"nav-link nav-link\" data-toggle=\"dropdown\"";
					txt += "href=\"BackStage_Order.html\" role=\"button\" aria-haspopup=\"true\"";
					txt += "aria-expanded=\"false\">訂單管理</a></div>";
				}

				if (data.getDbLGSetLic().getDbSId() == 2) {
					txt += "<a class=\"nav-link nav-link\"";
					txt += "data-toggle=\"dropdown\" href=\"BackStage_LicenseAccount.html\"";
					txt += "role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\" style=\"font-family: 微軟正黑體 \">後台帳號管理</a>&nbsp;&nbsp;&nbsp;";
				} else {
					txt += "<div style=\"display:none\"><a class=\"nav-link nav-link\"";
					txt += "data-toggle=\"dropdown\" href=\"BackStage_LicenseAccount.html\"";
					txt += "role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">後台帳號管理</a></div>";
				}

				if (data.getDbLGSetLic().getDbSId() == 2) {
					txt += "<a class=\"nav-link nav-link\" data-toggle=\"dropdown\"";
					txt += "href=\"BackStage_LicenseGroup.html\" role=\"button\" aria-haspopup=\"true\"";
					txt += "aria-expanded=\"false\" style=\"font-family: 微軟正黑體 \">部門權限管理</a>&nbsp;&nbsp;&nbsp;";
				} else {
					txt += "<div style=\"display:none\"><a class=\"nav-link nav-link\" data-toggle=\"dropdown\"";
					txt += "href=\"BackStage_LicenseGroup.html\" role=\"button\" aria-haspopup=\"true\"";
					txt += "aria-expanded=\"false\">部門權限管理</a> </div>";
				}

				if (data.getDbLGSetSEO().getDbSId() == 2) {
					txt += " <a class=\"nav-link nav-link\"";
					txt += "data-toggle=\"dropdown\" href=\"BackStage_Seo.html\" role=\"button\"";
					txt += "aria-haspopup=\"true\" aria-expanded=\"false\" style=\"font-family: 微軟正黑體 \">SEO管理</a>&nbsp;&nbsp;&nbsp;";
				} else {
					txt += "<div style=\"display:none\"><a class=\"nav-link nav-link\"";
					txt += "data-toggle=\"dropdown\" href=\"BackStage_Seo.html\" role=\"button\"";
					txt += "aria-haspopup=\"true\" aria-expanded=\"false\">SEO管理</a></div>";
				}
			}
			
			txt1+="<li id=\"dbLAUserNamedb\" class=\"nav-item dropdown\"></li>";
			txt1+="<li class=\"nav-item dropdown\"><a id=\"logout\" class=\"nav-link nav-link\"";
			txt1+="data-toggle=\"dropdown\" href=\"http://localhost:8080/FinalProject/backstage/BackStage_Login.html\" role=\"button\" aria-haspopup=\"true\"";
			txt1+="aria-expanded=\"false\" style=\"font-family: 微軟正黑體 \">登出</a></li></ul>";
		
			LGmodel lgmodel = new LGmodel();
			lgmodel.setBackstageMenu(txt);
			lgmodel.setBackstageMenu_last(txt1);
			return new Gson().toJson(lgmodel);

		}

	}
}
