package controller.licensegroup;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import model.licensegroup.LicenseGroupService;

@Controller
public class ListAllLicenseGroup {
	@Autowired
	private LicenseGroupService lgs;
	
	@RequestMapping(path="/backstage/ListAllLicenseGroup.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String listAllLicenseGroup() {
		return new Gson().toJson(lgs.selectAll());
	}
}
