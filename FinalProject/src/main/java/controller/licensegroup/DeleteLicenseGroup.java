package controller.licensegroup;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import model.LicenseGroupBean;
import model.licensegroup.LicenseGroupService;

@Controller
public class DeleteLicenseGroup {
	@Autowired
	private LicenseGroupService lgs;
	@RequestMapping(path="/backstage/DeleteLicenseGroup.do")
	@ResponseBody
	public String deleteLicenseGroup(LicenseGroupBean lgb) {
		lgs.delete(lgb);
		return "licensegroupIndex";
	}
}
