package controller.licensegroup;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import model.LicenseGroupBean;
import model.StatusBean;
import model.licensegroup.LicenseGroupService;

@Controller
public class UpdateLicenseGroup {
	
		@Autowired
		private LicenseGroupService lgs;
		@RequestMapping(path="/backstage/updateLicenseGroup.do",method=RequestMethod.POST)
		public String updateLicenseAccount(Integer dbLGId, String dbLGName ,String dbSId
				,String dbLGSetBanner,String dbLGSetProd,String dbLGSetShare
				,String dbLGSetComProfile,String dbLGSetPopProd
				,String dbLGSetCust,String dbLGSetOrder,String dbLGSetSEO
				,String dbLGSetLic) {
			LicenseGroupBean lgb = new LicenseGroupBean();
			lgb.setDbLGId(dbLGId);
			lgb.setDbLGName(dbLGName);
			if(dbSId==null) {
				lgb.setDbLGStatus(new StatusBean((byte)1, null));
			}else {			
				lgb.setDbLGStatus(new StatusBean((byte)2, null));
			}
			
			if(dbLGSetBanner==null) {
				lgb.setDbLGSetBanner(new StatusBean((byte)1, null));
			}else {			
				lgb.setDbLGSetBanner(new StatusBean((byte)2, null));
			}
			
			if(dbLGSetProd==null) {
				lgb.setDbLGSetProd(new StatusBean((byte)1, null));
			}else {			
				lgb.setDbLGSetProd(new StatusBean((byte)2, null));
			}
			
			if(dbLGSetShare==null) {
				lgb.setDbLGSetShare(new StatusBean((byte)1, null));
			}else {			
				lgb.setDbLGSetShare(new StatusBean((byte)2, null));
			}
			
			if(dbLGSetComProfile==null) {
				lgb.setDbLGSetComProfile(new StatusBean((byte)1, null));
			}else {			
				lgb.setDbLGSetComProfile(new StatusBean((byte)2, null));
			}
			
			if(dbLGSetPopProd==null) {
				lgb.setDbLGSetPopProd(new StatusBean((byte)1, null));
			}else {			
				lgb.setDbLGSetPopProd(new StatusBean((byte)2, null));
			}
			
			if(dbLGSetCust==null) {
				lgb.setDbLGSetCust(new StatusBean((byte)1, null));
			}else {			
				lgb.setDbLGSetCust(new StatusBean((byte)2, null));
			}
			
			if(dbLGSetOrder==null) {
				lgb.setDbLGSetOrder(new StatusBean((byte)1, null));
			}else {			
				lgb.setDbLGSetOrder(new StatusBean((byte)2, null));
			}
			
			if(dbLGSetSEO==null) {
				lgb.setDbLGSetSEO(new StatusBean((byte)1, null));
			}else {			
				lgb.setDbLGSetSEO(new StatusBean((byte)2, null));
			}
			
			if(dbLGSetLic==null) {
				lgb.setDbLGSetLic(new StatusBean((byte)1, null));
			}else {			
				lgb.setDbLGSetLic(new StatusBean((byte)2, null));
			}
			lgs.update(lgb);
			return "licensegroupIndex";	
		}
		
		@RequestMapping(path="/backstage/selectLGUpdate.do",method=RequestMethod.POST,produces="text/html;charset=UTF-8")
		@ResponseBody
		public String selectUpdate(LicenseGroupBean lab) {
			return new Gson().toJson(lgs.selectLGUpdate(lab));
		}
}
