package controller.product;

import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;

import model.ProductBean;
import model.ProductNameBean;
import model.StatusBean;
import model.product.ProductService;

@Controller
public class UpdateProduct {
	//Save the uploaded file to this folder
//		private static String UPLOADED_FOLDER = "C:\\FinalProject\\Repository\\FinalProject\\src\\main\\webapp\\Images\\";
		private static String UPLOADED_FOLDER = "C:\\Users\\user\\git\\repository\\FinalProject\\src\\main\\webapp\\Images\\";
		@Autowired
		private ProductService ps;
		@RequestMapping(path="/backstage/updateProduct.do",method=RequestMethod.POST)
		public String insertProduct(ProductBean pb,Integer dbPNId, MultipartFile[] typePics , MultipartFile[] btmPics , String dbSId) {
			
			pb.setDbProductName(new ProductNameBean(dbPNId, null));
			
			for(int i =0 ; i<typePics.length ; i++){
				pb.setDbPTypeImage(pb.getDbPTypeImage()+(i<(typePics.length-1) ? typePics[i].getOriginalFilename()+"," : typePics[i].getOriginalFilename()));
				try {
					typePics[i].transferTo(new File(UPLOADED_FOLDER+typePics[i].getOriginalFilename()));
				} catch (IllegalStateException | IOException e) {
					e.printStackTrace();
				}
			}
			
			for(int i = 0 ; i<btmPics.length ; i++){
				pb.setDbPImage(i<(btmPics.length-1) ? btmPics[i].getOriginalFilename()+"," : btmPics[i].getOriginalFilename());
					try {
						btmPics[i].transferTo(new File(UPLOADED_FOLDER+btmPics[i].getOriginalFilename()));
					} catch (IllegalStateException | IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
			
			if(dbSId==null) {
				pb.setDbPStatus(new StatusBean((byte)1, null));
			}else {			
				pb.setDbPStatus(new StatusBean((byte)2, null));
			}
			ps.update(pb);
			return "productIndex";	
		}
		
		@RequestMapping(path="/backstage/selectUpdate.do",method=RequestMethod.POST,produces="text/html;charset=UTF-8")
		@ResponseBody
		public String selectUpdate(ProductBean pb) {
			return new Gson().toJson(ps.selectUpdate(pb));
		}
}
