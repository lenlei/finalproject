package controller.product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import model.ProductBean;
import model.product.ProductService;

@Controller
public class DeleteProduct {
	@Autowired
	private ProductService ps;
	@RequestMapping(path="/backstage/DeleteProduct.do")
	public void deleteBanner(ProductBean pb) {
		ps.delete(pb);			
	}
}
