package controller.product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import model.ProductBean;
import model.product.ProductService;

@Controller
public class GetOneProduct {

	@Autowired
	private ProductService ps;
	@RequestMapping(path="/shopping/GetOneProduct", method = RequestMethod.POST,produces="text/html;charset=UTF-8")
	@ResponseBody
	public String listAllProductToSecondHTML(Integer dbPId) {
		return new Gson().toJson(ps.selectByPId(dbPId));//將回傳的ProductBean轉為Json字串
	}
}
