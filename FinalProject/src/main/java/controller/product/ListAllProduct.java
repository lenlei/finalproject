package controller.product;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import model.ProductBean;
import model.product.ProductService;

@Controller
public class ListAllProduct {
	@Autowired
	private ProductService ps;
	
	@RequestMapping(path="/backstage/ListAllProduct.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String listAllProduct() {
		return new Gson().toJson(ps.selectAll());
	}
	
	@RequestMapping(path="/shopping/GetAllProduct",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String listAllProductToFirstHTML() {
		List<ProductBean> d = ps.selectAll_status2();
		for(ProductBean i : d) {
			System.out.println("-----------------------------------");
			System.out.println(i.getDbPId());
			System.out.println(i.getDbPImage());
			System.out.println(i.getDbPIntroduction());
			System.out.println(i.getDbPType());
			System.out.println(i.getDbPTypeImage());
			System.out.println(i.getDbPUnitPrice());
			System.out.println(i.getDbPId());
			System.out.println(i.getDbPStatus().getDbSName());
			System.out.println("-----------------------------------");
		}
		return new Gson().toJson(d);
	}//for first.html圖片使用，只選(已啟用)status是2的ProductBean


	
	@RequestMapping(path="/shopping/GetAllProductFilm",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String listAllProductToFirstHTMLFilm() {
		List<ProductBean> d = ps.selectAll_status2Film();
		for(ProductBean i : d) {
			System.out.println("-----------------------------------");
			System.out.println(i.getDbPId());
			System.out.println(i.getDbPImage());
			System.out.println(i.getDbPIntroduction());
			System.out.println(i.getDbPType());
			System.out.println(i.getDbPTypeImage());
			System.out.println(i.getDbPUnitPrice());
			System.out.println(i.getDbPId());
			System.out.println(i.getDbPStatus().getDbSName());
			System.out.println("-----------------------------------");
		}
		return new Gson().toJson(d);
	}//for first.html圖片使用，只選(已啟用)status是2的ProductBean
	
}