package controller.seo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import model.SEODescriptionBean;
import model.SEOKeyWordBean;
import model.SEOSetBean;
import model.StatusBean;
import model.seo.SeoService;

@Controller
public class UpdateSEO {

	@Autowired
	private SeoService Ss;
	@RequestMapping(path="/backstage/selectAllSEOKeyword.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String selectKeyword() {
    	//System.out.println("33333");
		return new Gson().toJson(Ss.selectKeyword());
	}
	@RequestMapping(path="/backstage/selectAllSEODescription.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String selectDspt() {
		//System.out.println("33333");
		return new Gson().toJson(Ss.selectDspt());
	}
	@RequestMapping(path="/shopping/selectAllSEOKeyword.run",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String selectKeyword1() {
		//System.out.println("33333");
		return new Gson().toJson(Ss.selectKeyword());
	}
	@RequestMapping(path="/shopping/selectAllSEODescription.run",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String selectDspt1() {
    	//System.out.println("33333");
		return new Gson().toJson(Ss.selectDspt());
	}
	@RequestMapping(path="/backstage/update.go",method=RequestMethod.POST)
	@ResponseBody
	public String update(String dspt, String keyword,String dbSIdk ,String dbSIdd) {
		

		
		if("false".equals(dbSIdk) &&"false".equals(dbSIdd)) {

			Ss.update(new SEOSetBean(new SEODescriptionBean(null,dspt,new StatusBean((byte) 1, null)),
					                 new SEOKeyWordBean(null,keyword,new StatusBean((byte) 1, null))));
						
		}else if ("false".equals(dbSIdk) &&"true".equals(dbSIdd)){			 

			Ss.update(new SEOSetBean(new SEODescriptionBean(null,dspt,new StatusBean((byte) 2, null)), new SEOKeyWordBean(null,keyword,new StatusBean((byte) 1, null))));
		}else if ("true".equals(dbSIdk) &&"false".equals(dbSIdd)){			 

			Ss.update(new SEOSetBean(new SEODescriptionBean(null,dspt,new StatusBean((byte) 1, null)), new SEOKeyWordBean(null,keyword,new StatusBean((byte) 2, null))));
		}else {

			Ss.update(new SEOSetBean(new SEODescriptionBean(null,dspt,new StatusBean((byte) 2, null)), new SEOKeyWordBean(null,keyword,new StatusBean((byte) 2, null))));
		}		

		return "yes";		
	}
}

