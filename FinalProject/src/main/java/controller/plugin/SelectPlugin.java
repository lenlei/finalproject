package controller.plugin;

import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;

import model.PluginBean;
import model.ProductBean;
import model.StatusBean;
import model.plugin.PluginService;
import model.product.ProductService;

@Controller
public class SelectPlugin {
	private static String UPLOADED_FOLDER = "C:\\FinalProject\\Repository\\FinalProject\\src\\main\\webapp\\Images\\";
	@Autowired
	private PluginService ps;
	@Autowired
	private ProductService products;
	

	@RequestMapping(path = "/shopping/selectPlugin.do", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String selectPlugin(PluginBean pb) {
		return new Gson().toJson(ps.selectPlugin(pb));
	}

	@RequestMapping(path="/shopping/selectTheme.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String selectTheme(ProductBean pb) {
		return new Gson().toJson(products.selectAll());
	}

	// 後臺畫面撈資料使用
	@RequestMapping(path = "/backstage/selectPlugin.run", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String selectPlugin2() {
		return new Gson().toJson(ps.selectPlugin2());
	}

	// 後台畫面刪除
	@RequestMapping(path = "/backstage/DeletePlugin.do")
	public String deletePlugin(PluginBean pb) {
		ps.delete(pb);
		return "pluginIndex";
	}

	// 後台畫面新增
	@RequestMapping(path = "/backstage/insertPlugin.do", method = RequestMethod.POST)
	public String insertPlugin(PluginBean pb, MultipartFile file, String dbSId) {
		
		pb.setDbPIImage(file.getOriginalFilename());
		//pb.setDbPIName(pb.getDbPIName());
//		pb.setDbPIStatus(pb.getDbPIStatus());

		try {
			file.transferTo(new File(UPLOADED_FOLDER + file.getOriginalFilename()));
//			file.transferTo(new File(UPLOADED_FOLDER+new Date().getTime()+file.getOriginalFilename()));
		} catch (IllegalStateException | IOException e) {
			e.printStackTrace();
		}

		if (dbSId == null) {
			pb.setDbPIStatus(new StatusBean((byte) 1, null));
		} else {
			pb.setDbPIStatus(new StatusBean((byte) 2, null));
		}

		ps.insert(pb);
		return "pluginIndex";
	}

	// 更新後台
	@RequestMapping(path = "/backstage/updateplugin.do", method = RequestMethod.POST)
	public String updateplugin(PluginBean pb, MultipartFile file, String dbSId) {
//				public String updateBanner(BannerBean bb,Integer dbBId,String dbBHyperLink, MultipartFile file, String dbSId) {
		
		pb.setDbPIImage(file.getOriginalFilename());
		
		try {
			file.transferTo(new File(UPLOADED_FOLDER + file.getOriginalFilename()));
//					file.transferTo(new File(UPLOADED_FOLDER+new Date().getTime()+file.getOriginalFilename()));
		} catch (IllegalStateException | IOException e) {
			e.printStackTrace();
		}
		if (dbSId == null) {
			pb.setDbPIStatus(new StatusBean((byte) 1, null));
		} else {
			pb.setDbPIStatus(new StatusBean((byte) 2, null));
		}
		ps.update(pb);
		return "pluginIndex";

	}
}
