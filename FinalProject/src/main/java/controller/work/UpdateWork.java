package controller.work;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import model.StatusBean;
import model.WorkBean;
import model.work.WorkService;

@Controller
public class UpdateWork {
		@Autowired
		private WorkService ws;
		@RequestMapping(path="/backstage/UpdateWork.do",method=RequestMethod.POST)
		public String insertProduct(WorkBean wb, String dbSId) {

			if(dbSId==null) {
				wb.setDbWStatus(new StatusBean((byte)1, null));
			}else {			
				wb.setDbWStatus(new StatusBean((byte)2, null));
			}
			ws.update(wb);
			return "workIndex";	
		}
		
//		@RequestMapping(path="/backstage/selectUpdate.do",method=RequestMethod.POST,produces="text/html;charset=UTF-8")
//		@ResponseBody
//		public String selectUpdate(ProductBean pb) {
//			return new Gson().toJson(ps.selectUpdate(pb));
//		}
}
