package controller.work;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.GsonBuilder;

import model.work.WorkService;

@Controller
public class ListAllWork {
	@Autowired
	private WorkService ws;
	
	@RequestMapping(path="/backstage/ListAllWork.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String listAllWork() {
		return new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(ws.selectAll());
	}
}
