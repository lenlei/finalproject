package controller.work;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import model.WorkBean;
import model.work.WorkService;

@Controller
public class DeleteWork {
	@Autowired
	private WorkService ws;
	@RequestMapping(path="/backstage/DeleteWork.do")
	public void deleteBanner(WorkBean wb) {
		ws.delete(wb);
	}
}
