package controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import model.AccountUserBean;
import model.AccountUser.AccountUser_Service;

@Controller
public class AccountUser_Controller {

	@Autowired
	private AccountUser_Service accountService;

	@RequestMapping(path = "/accountController", method = RequestMethod.POST)
	@ResponseBody
	public String method(String Button, AccountUserBean ac) {

		if ("Register".equals(Button)) {
			if (!"".equals(ac.getDbAAccount()) && !"".equals(ac.getDbAPassword())) {
				System.out.println(ac.getDbAAccount());
				System.out.println(ac.getDbAPassword());

				accountService.Register(ac); //測試OK
//				boolean s = accountService.userExists(ac.getDbAAccount()); //測試OK
//				System.out.println(s);

				return "Success";
			}
		}
		return "";
	}

}

