package controller.tempPreview;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import model.TemporaryPreviewBean;
import model.temporaryPreview.TempPreview_Service;

@Controller
public class PreviewImgs_Controller {
	@Autowired
	private TempPreview_Service tempPreview_Service;
	
	@RequestMapping(path="/shopping/GetAllPreviewImgs",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String listAllPreviewImgsToSecondHTML(Integer dbTPId) {
		System.out.println("dbTPId==>>"+dbTPId);
		List<TemporaryPreviewBean> previewImgs = tempPreview_Service.PreviewImgsBydbTPId(dbTPId);
		if(previewImgs!=null) {	
			for(TemporaryPreviewBean t:previewImgs) {
				System.out.println("-----------------------------------");
				System.out.println(t.getDbTPage());
				System.out.println(t.getDbTPageImage());
				System.out.println("-----------------------------------");
			}
			return new Gson().toJson(previewImgs);
		}
		return "empty";
	}//for second.html顯示預覽圖使用，透過暫存作品編號dbTPId去找
}
