package controller.tempPreview;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import model.ProductBean;
import model.TemporaryBean;
import model.TemporaryPreviewBean;
import model.product.ProductService;
import model.temp.TempService;
import model.temporaryPreview.TempPreview_Service;

@Controller
public class TestAllPreviewImgs {
	@Autowired
	private ProductService ps;
	@Autowired
	private TempPreview_Service tempPreview_Service;
	@Autowired
	private TempService ts;
	@RequestMapping(path="/shopping/testAllPreviewImgs",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String listAllPreviewImgsToSecondHTML(Integer dbPId,Integer dbTPId) {
		ProductBean product = ps.selectByPId(dbPId);
		TemporaryBean tempWork = ts.selectOneTemp(dbTPId);
		TempInfos ti = new TempInfos();
		ti.setProduct(product);
		ti.setPreviewImgs(tempPreview_Service.PreviewImgsBydbTPId(dbTPId));
		ti.setDbTTheme(tempWork.getDbTTheme());
		ti.setDbTIntroduction(tempWork.getDbTIntroduction());
		ti.setTempWork(tempWork);
		return new Gson().toJson(ti);
	}//for second.html圖片使用
	class TempInfos{
		ProductBean product;
		List<TemporaryPreviewBean> previewImgs;
		String dbTTheme;//作品主題
		String dbTIntroduction;//作品說明	
		TemporaryBean tempWork;
		
		public TemporaryBean getTempWork() {
			return tempWork;
		}
		public void setTempWork(TemporaryBean tempWork) {
			this.tempWork = tempWork;
		}
		public ProductBean getProduct() {
			return product;
		}
		public void setProduct(ProductBean product) {
			this.product = product;
		}
		public List<TemporaryPreviewBean> getPreviewImgs() {
			return previewImgs;
		}
		public void setPreviewImgs(List<TemporaryPreviewBean> previewImgs) {
			this.previewImgs = previewImgs;
		}
		public String getDbTTheme() {
			return dbTTheme;
		}
		public void setDbTTheme(String dbTTheme) {
			this.dbTTheme = dbTTheme;
		}
		public String getDbTIntroduction() {
			return dbTIntroduction;
		}
		public void setDbTIntroduction(String dbTIntroduction) {
			this.dbTIntroduction = dbTIntroduction;
		}
	}
}
