package controller.artboard;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import model.StatusBean;
import model.TemporaryBean;
import model.TemporaryPreviewBean;
import model.product.ProductService;
import model.temp.TempService;
import model.temporaryPreview.TempPreview_Service;

@Controller
public class SaveArtBoard {
	@Autowired
	private TempService ts;
	
	@Autowired
	private TempPreview_Service tps;
	
	@Autowired
	private ProductService ps;
	@RequestMapping(path="/shopping/saveTempImage.do")
	@ResponseBody
	public String saveTempImage(String Account,String Images,Integer dbTDWId,Integer page,Integer dbPId) {
		return ts.insertTempImage(Account,Images,dbTDWId,page,dbPId);
	}
	
	@RequestMapping(path="/shopping/saveTempWord.do")
	@ResponseBody
	public String saveTempWord(String Account,String Words,Integer dbTDWId,Integer page,Integer dbPId) {
		if(Words!=null) {
			ts.insertTempWord(Account, Words, dbTDWId,page,dbPId);
		}
		return "OK!!!";
	}
	
	@RequestMapping(path="/shopping/selectProductImages.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String selectProductImages(Integer dbPId) {
		return new Gson().toJson(ps.selectOneProduct(dbPId));
	}
	
	@RequestMapping(path="/shopping/selectAllImages.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String selectAllImages(Integer dbTId,Integer page) {
		return new Gson().toJson(ts.selectAllImages(dbTId,page));
	}
	
	@RequestMapping(path="/shopping/selectAllWords.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String selectAllWords(Integer dbTId,Integer page) {
		return new Gson().toJson(ts.selectAllWords(dbTId,page));
	}
	@RequestMapping(path="/shopping/savePreview.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String savePreview(String Account,TemporaryPreviewBean tpb,String PageView,String CalendarView) {
		if(Account!=null) {
			ts.insertTempPreview(Account,tpb,PageView,CalendarView);
			return "預覽圖存檔成功";
		}
		return "您未登入";
	}
	
	@RequestMapping(path="/shopping/saveTempImageFilm.do")
	@ResponseBody
	public String saveTempImageFilm(String Account,String Images,Integer dbTDWId,Integer page,Integer dbPId) {
		System.out.println("======hao===="+ts.insertTempImageFilm(Account,Images,dbTDWId,page,dbPId));
		return ts.insertTempImageFilm(Account,Images,dbTDWId,page,dbPId);
	}
	
	
	@RequestMapping(path="/shopping/savePreviewFilm.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String savePreviewFilm(String Account,TemporaryPreviewBean tpb,String CalendarView) {
		if(Account!=null) {
			
			return ts.insertTempPreviewFilm(Account,tpb,CalendarView);
		}
		return "您未登入";
	}
	
//	@RequestMapping(path="/shopping/changeFilmStatus.do",produces="text/html;charset=UTF-8")
//	@ResponseBody
//	public String changeFilmStatus(TemporaryBean bean,String status) {
//		Integer num=Integer.parseInt(status);
//		Byte m = 0;
//		if(num.equals(3)) {
//			m=3;
//		}
//		if(num.equals(8)) {
//			m=8;
//		}
//		bean.setDbTStatus(new StatusBean(m,null));
//		System.out.println(m);
//		ts.insertFilm(bean);
//		return "成功";
//	}
	
	@RequestMapping(path="/shopping/changeFilmStatus.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String savePreviewFilmandchange(TemporaryBean bean,String status,String Account,TemporaryPreviewBean tpb,String CalendarView) {
		if(Account!=null) {
			ts.insertTempPreviewFilm(Account,tpb,CalendarView);

			Integer num=Integer.parseInt(status);
			Byte m = 0;
			if(num.equals(9)) {
				m=9;
			}
			if(num.equals(8)) {
				m=8;
			}
			bean.setDbTStatus(new StatusBean(m,null));
			System.out.println(CalendarView);
			System.out.println("!!!!!!!!!!!!!!");
			ts.insertFilm(bean);
			
			return "預覽圖存檔成功";
		}
		return "您未登入";
	}
	
	
	
	
	@RequestMapping(path="/shopping/saveImageToArray.do")
	@ResponseBody
	public String saveImageToArray(Integer dbTPId) {
		System.out.println("second send=======" + dbTPId);
		TemporaryPreviewBean bean = tps.PreviewImgsFilmBydbTPId(dbTPId);	
		System.out.println(bean.getDbTPageImage());
		return bean.getDbTPageImage();
	}
}
