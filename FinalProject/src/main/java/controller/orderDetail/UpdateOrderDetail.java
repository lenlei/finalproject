package controller.orderDetail;

//import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import model.AccountUserBean;
import model.CityBean;
import model.OrderBean;
import model.RegionBean;
import model.AccountUser.AccountUser_Service;
import model.Order.Order_Service;
import model.OrderDetail.OrderDetail_Service;

@Controller
public class UpdateOrderDetail {

	@Autowired
	private AccountUser_Service accountUser_Service;
	@Autowired
	private Order_Service order_Service;
	@Autowired
	private OrderDetail_Service orderDetail_Service;
	
	@RequestMapping(path = "/shopping/updateOrderDetail", method = RequestMethod.POST)
	@ResponseBody
	public Integer updateOrderDetail(AccountUserBean ac,String dbCid,String dbRid,String dbOId,String paymentMethod,String transferMethod) {
		Integer dbODOrder =Integer.parseInt(dbOId);
		Integer dbCidInt;
		Integer dbRidInt;
		if(dbCid!=null) {
			dbCidInt =Integer.parseInt(dbCid);
		}else {
			dbCidInt =0;
		}
		if(dbRid!=null) {			
			dbRidInt =Integer.parseInt(dbRid);
		}else {
			dbRidInt =106;
		}
		
		System.out.println(dbODOrder+"..."+dbCidInt+"..."+dbRidInt);
		orderDetail_Service.updateDetail(dbODOrder, paymentMethod, transferMethod);

		ac.setDbAPickpCity(new CityBean(dbCidInt+1));
		ac.setDbAPickupRegion(new RegionBean(dbRidInt));

		accountUser_Service.updatePickUpDetail(ac);	
		System.out.println("===========================");
		OrderBean ob = order_Service.getOrder(dbODOrder);
		ob.setDbOId(dbODOrder);
		ob.setDbOPickpCity(new CityBean(dbCidInt+1));
		ob.setDbOPickupRegion(new RegionBean(dbRidInt));
		ob.setDbOPickUpDetail(ac.getDbAPickupDetail());
		order_Service.updateAddress(ob,dbODOrder);
		return dbODOrder;
	}
}
