package controller.orderDetail;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import model.OrderDetailBean;
import model.OrderDetail.OrderDetail_Service;

@Controller
public class insertOrderDetail {
	
	@Autowired
	private OrderDetail_Service orderDetail_Service;
	
	@RequestMapping(path = "/shopping/insertOrderDetail", method = RequestMethod.POST)
	@ResponseBody
	public Integer insertOrder(@RequestBody List<OrderDetailBean> oderDetail) {
		Integer num=0;
		for (OrderDetailBean od : oderDetail) {
			System.out.println("==================OrderDetailBean========================");
			System.out.println("訂單編號====>"+od.getDbODOrder());// 訂單編號
			System.out.println(od.getDbODAmount());// 小計
			System.out.println(od.getDbODQty());// 數量
			System.out.println(od.getDbODProductRequire());// 需求
			System.out.println(od.getDbODProduct().getDbPId());// 產品編號
			System.out.println(od.getDbODProduct().getDbPType());// 類型
			System.out.println(od.getDbODProduct().getDbPUnitPrice());// 單價
			System.out.println("==================OrderDetailBean========================");
			orderDetail_Service.insertOrderDetail(od);
			num++;
		}
		System.out.println("insert訂單明細筆數==>"+num);

		return num;
	}

}
