package controller.orderDetail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import model.OrderDetailBean;
import model.ProductBean;
import model.OrderDetail.OrderDetail_Service;
import model.product.ProductService;

@Controller
public class DeleteOrderDetail {
	@Autowired
	private OrderDetail_Service ods;
	@RequestMapping(path="/backstage/DeleteOrderDetail.do")
	public void deleteOrderDetail(OrderDetailBean odb) {
		ods.deleteOD(odb);
	}
}
