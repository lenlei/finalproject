package controller.orderDetail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import model.OrderDetail.OrderDetail_Service;

@Controller
public class selectOrderDetail {
    @Autowired
	private OrderDetail_Service ods;
    
    @RequestMapping(path="/backstage/ListAllOrderDetail.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String listAllOrderDetail() {
		return new Gson().toJson(ods.selectAll());
	}
}
