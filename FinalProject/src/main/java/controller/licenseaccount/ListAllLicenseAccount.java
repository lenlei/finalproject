package controller.licenseaccount;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import model.licenseaccount.LicenseAccountService;

@Controller
public class ListAllLicenseAccount {
	@Autowired
	private LicenseAccountService las;
	
	@RequestMapping(path="/backstage/ListAllLicenseAccount.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String listAllLicenseAccount() {
		return new Gson().toJson(las.selectAll());
	}
	
}

