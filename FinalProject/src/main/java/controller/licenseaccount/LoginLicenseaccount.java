package controller.licenseaccount;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import model.LicenseAccountBean;
import model.licenseaccount.LicenseAccountService;


@Controller
public class LoginLicenseaccount {
	@Autowired
	private LicenseAccountService las;

	@RequestMapping(value = "/backstage/AccountLogin.do", produces = "text/html;charset=utf-8", method = {
			RequestMethod.POST })
	@ResponseBody
	public String method(LicenseAccountBean lab) {
		LicenseAccountBean selectlab = las.checkPassword(lab);
		if(	las.checkPassword(lab) != null ) {
			if(selectlab.getDbLAStatus().getDbSId()==7) {
				return "locked";
				}else {
					return "success";
					}
		}
			else {

				return "error";
			}

	}
	

	@RequestMapping(value = "/backstage/ReloadLoginData.do", produces = "text/html;charset=utf-8", method = {
			RequestMethod.POST })
	@ResponseBody
	public String reloadLoginData(LicenseAccountBean lab) {
		return new Gson().toJson(las.selectLAUpdate(lab));
	}
}