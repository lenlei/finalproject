package controller.licenseaccount;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import model.LicenseAccountBean;
import model.LicenseGroupBean;
import model.StatusBean;
import model.licenseaccount.LicenseAccountService;

@Controller
public class InsertLicenseAccount {

	@Autowired
	private LicenseAccountService las;
	@RequestMapping(path="/backstage/insertLicenseAccount.do",method=RequestMethod.POST)
	public String insertLicenseAccount(String dbLAAccount,Integer dbLGId,String dbLAPassword,String dbLAUsername, String dbSId) {
		LicenseAccountBean bean = new LicenseAccountBean();
		bean.setDbLAAccount(dbLAAccount);
		bean.setDbLAPassword(dbLAPassword);
		bean.setDbLAUserName(dbLAUsername);
//		StatusBean dbLAStatus =new StatusBean();
//		bean.setDbLAStatus(dbLAStatus);
		LicenseGroupBean dbLAGroup = new LicenseGroupBean();
		dbLAGroup.setDbLGId(dbLGId);
		bean.setDbLAGroupDefine(dbLAGroup);
		System.out.println(dbSId);
		if(dbSId==null) {
			bean.setDbLAStatus(new StatusBean((byte)7, null));
		}else {			
			bean.setDbLAStatus(new StatusBean((byte)2, null));
		}
		las.insert(bean);
		return "licenseaccountIndex";
	}
	
	@RequestMapping(path="/backstage/ListAllLGName.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String listAllLGName() {
		return new Gson().toJson(las.selectAllLGName());
	}
	
}
