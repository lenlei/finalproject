package controller.licenseaccount;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import model.LicenseAccountBean;
import model.LicenseGroupBean;
import model.StatusBean;
import model.licenseaccount.LicenseAccountService;

@Controller
public class UpdateLicenseAccount {
	
		@Autowired
		private LicenseAccountService las;
		@RequestMapping(path="/backstage/updateLicenseAccount.do",method=RequestMethod.POST)
		@ResponseBody
		public String updateLicenseAccount(LicenseAccountBean lab,Integer dbLGId ,boolean dbSId) {
			lab.setDbLAGroupDefine(new LicenseGroupBean(dbLGId));
			
			if(dbSId==false) {
				lab.setDbLAStatus(new StatusBean((byte)7, null));
			}else {			
				lab.setDbLAStatus(new StatusBean((byte)2, null));
			}
			las.update(lab);
			return "licenseaccountHome";	
		}
		
		@RequestMapping(path="/backstage/selectLAUpdate.do",method=RequestMethod.POST,produces="text/html;charset=UTF-8")
		@ResponseBody
		public String selectUpdate(LicenseAccountBean lab) {
			return new Gson().toJson(las.selectLAUpdate(lab));
		}
}
