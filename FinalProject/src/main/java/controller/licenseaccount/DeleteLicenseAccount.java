package controller.licenseaccount;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import model.LicenseAccountBean;
import model.licenseaccount.LicenseAccountService;

@Controller
public class DeleteLicenseAccount {
	@Autowired
	private LicenseAccountService las;
	@RequestMapping(path="/backstage/DeleteLicenseAccount.do")
	@ResponseBody
	public String deleteLicenseAccount(LicenseAccountBean lab) {
		las.delete(lab);
		return "licenseaccountIndex";
	}
}
