package controller.temporaryWork;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import model.temp.TempService;

@Controller
public class ListTemporary {
	@Autowired
	private TempService ts;
	@RequestMapping(name="/shopping/ListTemporary.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String selectTemporary(String account) {
		return new Gson().toJson(ts.selectTemporary(account));
	}
}
