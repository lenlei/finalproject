package controller.temporaryWork;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import model.StatusBean;
import model.TemporaryBean;
import model.temp.TempService;


@Controller
public class SaveTempIntroduction {
	@Autowired
	private TempService ts;
	
	@RequestMapping(path = "/shopping/saveTempIntroduction", method = RequestMethod.POST)
	@ResponseBody
	public String saveTempIntroduction(TemporaryBean temp) {
		ts.updateTempInfo(temp);
		return "OK";
	}
}
