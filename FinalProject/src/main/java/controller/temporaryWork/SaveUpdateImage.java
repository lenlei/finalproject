package controller.temporaryWork;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

/**
 * Servlet implementation class ChangeServlet
 */
@Controller
public class SaveUpdateImage  {	

//	private static String UPLOADED_FOLDER = "C:\\FinalProject\\Repository\\FinalProject\\src\\main\\webapp\\Images\\";
//	private static String UPLOADED_FOLDER = "C:\\Users\\TUNG\\git\\repository\\FinalProject\\src\\main\\webapp\\Images\\";
	private static String UPLOADED_FOLDER = "C:\\Users\\user\\git\\repository\\FinalProject\\src\\main\\webapp\\Images\\";


	
	@RequestMapping(value = "/shopping/upload",method = RequestMethod.POST , produces = MediaType.TEXT_PLAIN_VALUE)
	@ResponseBody
	  public String upload(MultipartFile finput) throws IOException {
		String newFileName = "";
	    if (!finput.getOriginalFilename().isEmpty()) {
	    	newFileName =  new Date().getTime()+finput.getOriginalFilename();
	    	finput.transferTo(new File(UPLOADED_FOLDER+newFileName));
	    }else{
	      return "fail";
	    }
	    System.out.println(newFileName+"~~~~~~~~~~~~");
	    return newFileName;
	}	
	
}
