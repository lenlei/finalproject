package controller.temporaryWork;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import model.temp.TempService;

@Controller
public class PrintAllPDF {
	@Autowired
	private TempService ts;
	@RequestMapping(path="/shopping/printAllPDF.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String printAllPDF(String Account,Integer dbTId) {
		return ts.selectTemp(Account, dbTId);
	}
	
	
	@RequestMapping(path="/shopping/printAllPDFFilm.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String printAllPDFFilm(String Account,Integer dbTId) {
		return ts.selectTempFilm(Account, dbTId);
	}
}
