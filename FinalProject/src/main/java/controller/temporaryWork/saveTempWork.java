package controller.temporaryWork;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import model.StatusBean;
import model.TemporaryBean;
import model.temp.TempService;

@Controller
public class saveTempWork {
	@Autowired
	private TempService ts;

	@RequestMapping(path = "/shopping/TemporaryController", method = RequestMethod.POST)
	@ResponseBody
	public Integer saveTemp(String TempWork,Integer dbPId) {
		Gson gson = new Gson();
		TemporaryBean tempBean = gson.fromJson(TempWork, TemporaryBean.class);
		tempBean.setDbPId(dbPId);//存入暫存作品編號
		tempBean.setDbTStatus(new StatusBean((byte)8,null));
		Integer k = (Integer)ts.insertNewTemp(tempBean);
		return k;//回傳暫存作品編號到 first.html
	}
}

