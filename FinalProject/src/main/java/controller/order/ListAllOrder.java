package controller.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import model.OrderBean;
import model.Order.OrderService;

@Controller
public class ListAllOrder {
	@Autowired
	private OrderService os;
	
	@RequestMapping(path="/backstage/ListAllOrder.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String listAllOrder() {
		return new Gson().toJson(os.selectAll());
	}
	@RequestMapping(path="/backstage/ListAllOrderDesc.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String listAllOrderDesc() {
		return new Gson().toJson(os.selectAllByOrderIdDesc());
	}
	@RequestMapping(path="/backstage/ListAllOrderAsc.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String listAllOrderAsc() {
		return new Gson().toJson(os.selectAllByOrderIdAsc());
	}
	@RequestMapping(path="/backstage/KeyupOrderDesc.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String keyUpOrderDesc(Integer searchKeyOrder) {
		return new Gson().toJson(os.selectKeyupOrderDesc2(searchKeyOrder));
	}
	@RequestMapping(path="/backstage/KeyupOrderAsc.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String keyUpOrderAsc(Integer searchKeyOrder) {
		
		return new Gson().toJson(os.selectKeyupOrderAsc2(searchKeyOrder));
	}
	@RequestMapping(path="/backstage/KeyupOrderByNameDesc.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String selectKeyupOrderByNameDesc2(String searchKeyName) {
		
		return new Gson().toJson(os.selectKeyupOrderByNameDesc2(searchKeyName));
	}
	@RequestMapping(path="/backstage/KeyupOrderByNameAsc.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String selectKeyupOrderByNameAsc2(String searchKeyName) {
		
		return new Gson().toJson(os.selectKeyupOrderByNameAsc2(searchKeyName));
	}
	@RequestMapping(path="/backstage/selectOrderByType.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String selectOrderByType(OrderBean ob) {	
		return new Gson().toJson(os.selectOrderByType(ob));
	}
	@RequestMapping(path="/backstage/selectOrderByType.run",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String selectOrderByType2(Byte OrderKey) {	
		return new Gson().toJson(os.selectOrderByType2(OrderKey));
	}
	
}
