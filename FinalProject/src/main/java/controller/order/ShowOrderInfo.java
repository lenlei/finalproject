package controller.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import model.OrderBean;
import model.Order.Order_Service;

@Controller
public class ShowOrderInfo {
	
	@Autowired
	private Order_Service order_Service;
	
	@RequestMapping(path = "/shopping/showOrderInfo", method = RequestMethod.POST,produces="text/html;charset=UTF-8")
	@ResponseBody
	public String findUserInfo(Integer oid) {
		System.out.println("oid=="+oid);
		order_Service.changeStatusTo_6(oid);
		OrderBean Order = order_Service.getOrder(oid);
		return new Gson().toJson(Order);
	}

}
