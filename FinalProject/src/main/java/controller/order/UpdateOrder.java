package controller.order;

import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;

import model.CityBean;
import model.OrderBean;
import model.OrderDetailBean;
import model.ProductBean;
import model.ProductNameBean;
import model.RegionBean;
import model.StatusBean;
import model.Order.OrderService;
import model.OrderDetail.OrderDetail_Service;
import model.product.ProductService;

@Controller
public class UpdateOrder {
		@Autowired
		private OrderService os;
		@Autowired
		private OrderDetail_Service ods;
		
		@RequestMapping(path="/backstage/updatedbOTotalPrice.do",method=RequestMethod.POST)
		public void updatedbOTotalPrice(OrderBean ob) {
			os.updatetotalprice(ob);	
		}
		
		@RequestMapping(path="/backstage/selectUpdateOrder.do",method=RequestMethod.POST,produces="text/html;charset=UTF-8")
		@ResponseBody
		public String selectUpdate(OrderBean ob) {
			return new Gson().toJson(os.selectUpdate(ob));
		}
		
		@RequestMapping(path="/backstage/SelectOrderDetail.run",method=RequestMethod.POST)
		@ResponseBody
		public String selectUpdateOrderDetail(OrderDetailBean odb) {
			return new Gson().toJson(os.selectUpdateOrderDetail(odb));
		}
		
		
		@RequestMapping(path="/backstage/updateOrderSetPage.do",method=RequestMethod.POST)
		public String updateOrderSetPage(OrderBean ob,Integer dbCid,Integer dbRid,Integer dbSId) {
			System.out.println("ob.getDbOTotalPrice()="+ob.getDbOTotalPrice());
			System.out.println("ob.getDbOPickUpDetail()="+ob.getDbOPickUpDetail());
			System.out.println("ob.getDbOPickupRegion()="+dbRid);
			System.out.println("ob.getDbOPickpCity()="+dbCid);
			System.out.println("ob.getDbOStatus()="+dbSId);
			
			ob.setDbOPickpCity(new CityBean(dbCid));
			ob.setDbOPickupRegion(new RegionBean(dbRid));
			ob.setDbOStatus(new StatusBean(dbSId.byteValue(), null));
			os.updateOrderSetPage(ob);
			
			return "orderIndex";	
		}
		
		@RequestMapping(path="/backstage/updateODdbODAmount.do",method=RequestMethod.POST)
		public void updateODdbODAmount(OrderDetailBean odb) {
			ods.updateODdbODAmount(odb);
		}
}
