package controller.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import model.OrderBean;
import model.StatusBean;
import model.Order.OrderService;

@Controller
public class InsertOrder {
	@Autowired
	private OrderService orderService;
	
	@RequestMapping(path = "/shopping/insertOrder", method = RequestMethod.POST)
	@ResponseBody
	public Integer insertOrder(@RequestBody  OrderBean order) {
		OrderBean ob = new OrderBean();
		ob.setDbOMemberEmail(order.getDbOMemberEmail());
		ob.setDbOTotalPrice(order.getDbOTotalPrice());
		ob.setDbOStatus(new StatusBean((byte)3,null));
		Integer orderNumber = (Integer) orderService.insert(ob);//新增到Order表格		
		return orderNumber;//回傳訂單編號到 sop_MyCart.html
	}

}
