package controller.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import model.Order.OrderService;

@Controller
public class OPayOrder {
	@Autowired
	private OrderService os;
	@RequestMapping(path="/shopping/opayPayment.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String opayPayment(Integer oid,Integer allPrice) {
		System.out.println("oid==>"+oid);
		return os.opayPayment(oid,allPrice);//寫到<div></div>裡丟到某個地方就會自己跑
	}
}
