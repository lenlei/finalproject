package controller.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import model.OrderBean;
import model.Order.OrderService;

@Controller
public class DeleteOrder {
	@Autowired
	private OrderService os;
	@RequestMapping(path="/backstage/DeleteOrder.do")
	@ResponseBody
	public String deleteOrder(OrderBean ob) {
		try {
			System.out.println("1111111111111");
			System.out.println(ob.getDbOId());
			os.delete(ob);		
		}catch(Exception e){
			e.printStackTrace();
			return "刪除失敗";
		}
		return "刪除成功";
	}
}
