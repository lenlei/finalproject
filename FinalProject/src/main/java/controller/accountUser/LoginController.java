package controller.accountUser;


import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import model.AccountUser.AccountUser_Service;
@Controller
public class LoginController {

	@Autowired
	private AccountUser_Service accountService;
	
	
	@RequestMapping(value = "/shopping/LoginServlet.go", produces = "text/html;charset=utf-8", method = {
			RequestMethod.POST })
	@ResponseBody
	public String method(String account, String pswd) {		
		if(	accountService.checkPassword(account,pswd) != null ) {			
			if(accountService.selectStatus(account).equals("已凍結")) {
				return "您的帳號已被凍結";
//				return "登入成功!";	
			}
			else {
				return "登入成功!";			
//				return "您的帳號已被凍結";
			}
		}
			else { 
				return "登入失敗!";
			
	}
	}
	
	@RequestMapping(value = "/shopping/ForgetPsw.go", produces = "text/html;charset=utf-8", method = {
			RequestMethod.POST })
	@ResponseBody
	public String method1(String mail) {		
		
		int z;
	    StringBuilder sb = new StringBuilder();
	    int i;
	    for (i = 0; i < 8; i++) {
	      z = (int) ((Math.random() * 7) % 3);
	 
	      if (z == 1) { // 放數字
	        sb.append((int) ((Math.random() * 10) + 48));
	      } else if (z == 2) { // 放大寫英文
	        sb.append((char) (((Math.random() * 26) + 65)));
	      } else {// 放小寫英文
	        sb.append(((char) ((Math.random() * 26) + 97)));
	      }
	    }
	    System.out.println(sb.toString());
		
		final String url = "http://script.google.com/macros/s/AKfycbzzPGuPr-qjB8DudchtvvW9HZ9mj-5s5GZZIFYHlW5kxCmOJ6wM/exec";
		final String urls = "https://script.google.com/macros/s/AKfycbzzPGuPr-qjB8DudchtvvW9HZ9mj-5s5GZZIFYHlW5kxCmOJ6wM/exec";
		 //存進資料庫
		try {		
			String responseString = new HttpClientTest().httpPostFormPswSend(mail,sb.toString());//email to user
			accountService.changePsw(mail, sb.toString());
			System.out.println(mail+sb.toString());
			System.out.println("OOOOOOOOOOOOOOOOOOOOOOOO");
			return "success";
		} catch (KeyManagementException e) {
			System.out.println("error");
		} catch (NoSuchAlgorithmException e) {
			System.out.println("error");
		}
		System.out.println("fail");
		return "fail";
		
	}
	}