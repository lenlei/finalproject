package controller.accountUser;


import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import model.AccountUserBean;
import model.CityBean;
import model.OrderBean;
import model.RegionBean;
import model.StatusBean;
import model.AccountUser.AccountUser_Service;
import model.Order.OrderService;
import model.cityAndRegion.CityService;

@Controller
public class ModifyMemInfo {

	@Autowired
	private AccountUser_Service service;
	
	@Autowired
	private CityService cityService;

	@Autowired
	private OrderService orderService;
	
	@RequestMapping(value = "/shopping/Modify.go", produces="application/json; charset=UTF-8",method = {
			RequestMethod.POST })
	@ResponseBody
	public String method(String account) {
		System.out.println("-----------------載入一");
		System.out.println(new JSONObject(service.selectAcc(account)));
//		  JSONObject jsonobj = new JSONObject(service.selectAcc(account));
//		     System.out.println("JSONSTR="+jsonobj.toString()); 


		System.out.println((service.selectAcc(account)).toString());
//		  JSONObject jsonobj = new JSONObject(service.selectAcc(account));
//		     System.out.println("new JSONObject(service.selectAcc(account))).toString());
		return (new JSONObject(service.selectAcc(account))).toString();		
	}
	
	@RequestMapping(value = "/shopping/Send.go", produces="application/json; charset=UTF-8", method = {
			RequestMethod.POST })
	@ResponseBody
	public String method2(AccountUserBean bean,String status,String cid,String rid) {
		Byte k;
		if(status.equals("未啟用")) {
			k=1;
		}
		else {
		    k=2;	
		}
		System.out.println(bean.toString());
		Integer ridd=Integer.parseInt(rid);
		Integer cidd=Integer.parseInt(cid);
		bean.setDbAPickpCity(new CityBean(cidd));
		bean.setDbAPickupRegion(new RegionBean(ridd));
		bean.setDbAStatus(new StatusBean(k,null));
			//JSONObject jsonobj = new JSONObject(service.updateMem(account, password, name, tel));	
			System.out.println(service.updateUserDetails(bean).toString());
			return (new JSONObject(service.updateUserDetails(bean))).toString();	
			//return (new JSONObject(service.updateUserDetails(new AccountUserBean(account,password,name,detail, new CityBean(cidd,null),new RegionBean(ridd,null),tel,new StatusBean(k,null))))).toString();

	}
	
	@RequestMapping(value="/shopping/ChangeStatus.go",method= {RequestMethod.POST})
	@ResponseBody
	public  String method3(String account) {
		//發送驗證碼
		//random 6碼存進資料庫  & email
		//email number & link to member center status
		final String url = "http://script.google.com/macros/s/AKfycbzzPGuPr-qjB8DudchtvvW9HZ9mj-5s5GZZIFYHlW5kxCmOJ6wM/exec";
		final String urls = "https://script.google.com/macros/s/AKfycbzzPGuPr-qjB8DudchtvvW9HZ9mj-5s5GZZIFYHlW5kxCmOJ6wM/exec";
		Integer num=(int) (Math.random()*1000000+1); //存進資料庫
		Long nowtime =System.currentTimeMillis();
		try {		
			String responseString = new HttpClientTest().httpPostForm(account,num);//email to user
			service.memVerifiy(account, num, nowtime);
			System.out.println(account+num);
			System.out.println("OOOOOOOOOOOOOOOOOOOOOOOO");
			return "success";
		} catch (KeyManagementException e) {
			System.out.println("error");
		} catch (NoSuchAlgorithmException e) {
			System.out.println("error");
		}
		System.out.println("fail");
		return "fail";
	}
	
	@RequestMapping(value="/shopping/SendStatus.go", produces = "text/html;charset=utf-8",method= {RequestMethod.POST})
	@ResponseBody
	public  String method4(String account,String num) {
		System.out.println(account);
		System.out.println(num);
		Long nowtime =System.currentTimeMillis();
		try{
			Integer vernum=Integer.parseInt(num);
			System.out.println(service.selectAcc(account).getVerification());
			
			if(vernum.equals(service.selectAcc(account).getVerification())) {
				if((nowtime-(service.selectAcc(account).getVerificationTime()))>7200000){
					System.out.println("認證逾時，請重新認證");
					return "認證逾時，請重新認證";
				}
				else {
					service.memVerifiySuccess(account,new StatusBean((byte) 2, null));
					System.out.println("認證成功!");
					return "認證成功!";				
				}
			}
			
			else {
				System.out.println("認證碼錯誤");
				return "認證碼錯誤";
			}
			
		}
		catch(NumberFormatException ex) {
			System.out.println("exception");
			return "認證碼錯誤";
		}
		
		//確認驗證碼
		//status change
		//if 5mins X
	}

	@RequestMapping(value = "/shopping/CheckStatus.go",produces="text/html;charset=UTF-8", method = {
			RequestMethod.POST })
	@ResponseBody
	public String method5(String account) {
		System.out.println("-----------------載入二");
		Long nowtime =System.currentTimeMillis();
		if((service.selectAcc(account).getDbAStatus().getDbSName())=="未啟用") {
		if(service.selectAcc(account).getVerification()!=null) {
			if((nowtime-(service.selectAcc(account).getVerificationTime())>7200000)){
				System.out.println("timeout");
				return "timeout";
			}
			else {
				System.out.println("驗證中");
				return "display"; //驗證中				
			}
	
		}
		else {
			System.out.println("尚未驗證");
		    return "hide";	//尚未驗證
		}
	}
	else {
		System.out.println("已驗證");
		return "verified"; //已驗證
	}
		
	}
	@RequestMapping(value = "/shopping/LoadCity.go",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String method6() {
		System.out.println(cityService.selectAllCity());
		return new Gson().toJson(cityService.selectAllCity());
	}
	
	@RequestMapping(value = "/shopping/LoadRegion.go",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String method7(String city) {
			Integer cid=Integer.parseInt(city);
			System.out.println(cityService.selectAllRegion(cid));			
			return new Gson().toJson(cityService.selectAllRegion(cid));	

	}
	
	@RequestMapping(value = "/shopping/AutoSend.go",produces="text/html;charset=UTF-8",method = {
			RequestMethod.POST })
	@ResponseBody
	public String method8(String account) {
		System.out.println(account);

			if(service.selectAcc(account).getVerification()!=null) { 
				System.out.println("QQQQQ");
				return "alreadysend";
			}
			if(service.selectAcc(account).getDbAAccount().equals(service.selectAcc(account).getDbAPassword())) {
				return "fng";
			}
			else {
				final String url = "http://script.google.com/macros/s/AKfycbzzPGuPr-qjB8DudchtvvW9HZ9mj-5s5GZZIFYHlW5kxCmOJ6wM/exec";
				final String urls = "https://script.google.com/macros/s/AKfycbzzPGuPr-qjB8DudchtvvW9HZ9mj-5s5GZZIFYHlW5kxCmOJ6wM/exec";
				Integer num=(int) (Math.random()*1000000+1); //存進資料庫
				Long nowtime =System.currentTimeMillis();
				try {		
					String responseString = new HttpClientTest().httpPostForm(account,num);//email to user
					service.memVerifiy(account, num, nowtime);
					System.out.println(account+num);
					System.out.println("OOOOOOOOOOOOOOOOOOOOOOOO");
					return "success";
				} catch (KeyManagementException e) {
					System.out.println("error");
				} catch (NoSuchAlgorithmException e) {
					System.out.println("error");
				}
				return "fail";
			}
	}
	
	@RequestMapping(value = "/shopping/OrderDetail.go",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String method9(String account) {	
		List<OrderBean> a = orderService.selectOne(account);

		System.out.println(a.get(0).getDbOOrderDate());
			return new Gson().toJson(orderService.selectOne(account));	

	}
	
	

}

//#                       _oo0oo_
//#                      o8888888o
//#                      88" . "88
//#                      (| -_- |)
//#                      0\  =  /0
//#                    ___/`---'\___
//#                  .' \\|     |# '.
//#                 / \\|||  :  |||# \
//#                / _||||| -:- |||||- \
//#               |   | \\\  -  #/ |   |
//#               | \_|  ''\---/''  |_/ |
//#               \  .-\__  '-'  ___/-. /
//#             ___'. .'  /--.--\  `. .'___
//#          ."" '<  `.___\_<|>_/___.' >' "".
//#         | | :  `- \`.;`\ _ /`;.`/ - ` : | |
//#         \  \ `_.   \_ __\ /__ _/   .-` /  /
//#     =====`-.____`.___ \_____/___.-`___.-'=====
//#                       `=---='
//#     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
