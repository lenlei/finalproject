package controller.accountUser;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import model.AccountUserBean;
import model.AccountUser.AccountUserService;

@Controller
public class DeleteAccountUser {
	@Autowired
	private AccountUserService aus;

	@RequestMapping(path = "/backstage/DeleteAccountUser.do")
	public String deleteAccountUser(AccountUserBean aub) {
		aus.delete(aub);
		return "accountuserIndex";
	}
}
