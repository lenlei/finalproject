package controller.accountUser;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import model.AccountUserBean;
import model.AccountUser.AccountUserService;

@Controller
public class showUserInfo {
	
	@Autowired
	private AccountUserService accountUserService;
	
	@RequestMapping(path = "/shopping/showUserInfo", method = RequestMethod.POST,produces="text/html;charset=UTF-8")
	@ResponseBody
	public String findUserInfo(String dbAAccount) {
		System.out.println("dbAAccount=="+dbAAccount);
		AccountUserBean userInfo = accountUserService.findUserInfo(dbAAccount);
		return new Gson().toJson(userInfo);
	}

}
