package controller.accountUser;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.google.gson.Gson;

import model.Order.OrderService;
import model.OrderDetail.OrderDetail_Service;

@Controller
public class AccOrderDetail {

	@Autowired
	private OrderDetail_Service orderDetailService;
	
	@Autowired
	private OrderService orderService;
	
	@RequestMapping(value = "/shopping/OrderDetails.go", produces="text/html;charset=UTF-8")
	@ResponseBody
	public String method1(String oid) {
		Integer oidd=Integer.parseInt(oid);
		System.out.println(orderDetailService.selectOrderDetail(oidd));
		return new Gson().toJson(orderDetailService.selectOrderDetail(oidd));
	}
	
	
	@RequestMapping(value = "/shopping/OrderTotal.go", produces="text/html;charset=UTF-8")
	@ResponseBody
	public String method2(String oid) {
		Integer oidd = Integer.parseInt(oid);
		System.out.println(orderService.selectOneByOid(oidd));
		return new Gson().toJson(orderService.selectOneByOid(oidd));
	}
}
