package controller.accountUser;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

public class HttpClientTest {

	
    /**
     * get方法
     */
    public void httpGet() {  
//        CloseableHttpClient httpclient = HttpClients.createDefault(); 
        CloseableHttpClient httpclient = HttpClientUtil.createSSLClientDefault();
        try {  
            // 创建httpget.    
            HttpGet httpget = new HttpGet(InYearHttpPost.url);
            
            System.out.println("executing request " + httpget.getURI());  
            // 执行get请求.    
            CloseableHttpResponse response = httpclient.execute(httpget);  
            try {  
                // 获取响应实体    
                HttpEntity entity = response.getEntity(); 
                
                System.out.println("--------------------------------------");  
                // 打印响应状态    
                System.out.println(response.getStatusLine());  
                if (entity != null) {  
                    // 打印响应内容长度    
                    System.out.println("Response content length: " + entity.getContentLength());  
                    // 打印响应内容    
                    System.out.println("Response content: " + EntityUtils.toString(entity));  
                }  
                System.out.println("------------------------------------");  
            } finally {  
                response.close();  
            }  
        } catch (ClientProtocolException e) {  
            e.printStackTrace();  
        } catch (ParseException e) {  
            e.printStackTrace();  
        } catch (IOException e) {  
            e.printStackTrace();  
        } finally {  
            // 关闭连接,释放资源    
            try {  
                httpclient.close();  
            } catch (IOException e) {  
                e.printStackTrace();  
            }  
        }  
    }  
    
    //驗證碼
    /**
     * post方法(表单提交)
     * @throws NoSuchAlgorithmException 
     * @throws KeyManagementException 
     */
    public String httpPostForm(String umail,Integer num) throws NoSuchAlgorithmException, KeyManagementException {
    	
        // 创建默认的httpClient实例.    
//        CloseableHttpClient httpclient = HttpClients.createDefault();  
        CloseableHttpClient httpclient = HttpClientUtil.createSSLClientDefault();
        
        // 创建httppost    
        HttpPost httppost = new HttpPost(InYearHttpPost.urls);
        //int num=(int)(Math.random()*100000+1);
        // 创建参数队列    
        List<NameValuePair> formParams = new ArrayList<NameValuePair>();  
        formParams.add(new BasicNameValuePair("mmail", "hsiaoelf8895@gmail.com")); // 權限帳號 xxxx@gmail.com 
        formParams.add(new BasicNameValuePair("umail", umail)); // 發送的email aaa@gmail 或是 aaa@gmail,bbb@gmail    https://www.shareicon.net/data/128x128/2016/11/14/852139_camera_512x512.png
        formParams.add(new BasicNameValuePair("subject", "【時光文創】會員註冊驗證信 ")); // mail 的主旨
        formParams.add(new BasicNameValuePair("body",
        		"<div style=\"padding:0;width:100%!important;margin:0\" marginheight=\"0\" marginwidth=\"0\"><center><table cellpadding=\"8\" cellspacing=\"0\" style=\"padding:0;width:100%!important;background:#ffffff;margin:0;background-color:#ffffff\" border=\"0\"><tbody><tr><td valign=\"top\">\r\n" + 
        		"<table cellpadding=\"0\" cellspacing=\"0\" style=\"border-radius:4px;border:1px #A8D8B9 solid\" border=\"0\" align=\"center\">\r\n" + 
        		"<tbody><tr><td colspan=\"3\" height=\"6\"></td></tr>\r\n" + 
        		"<tr style=\"line-height:0px\"><td width=\"100%\" style=\"font-size:0px\" align=\"center\" height=\"1\"><img src=\"https://www.shareicon.net/data/128x128/2016/11/14/852139_camera_512x512.png\" style=\"max-height:73px;width:40px\" alt=\"\" width=\"40px\" class=\"CToWUd\"></td></tr> <tr><td><table cellpadding=\"0\" cellspacing=\"0\" style=\"line-height:25px\" border=\"0\" align=\"center\">\r\n" + 
        		"<tbody><tr><td colspan=\"3\" height=\"30\"></td></tr>\r\n" + 
        		"<tr>\r\n" + 
        		"<td width=\"36\"></td>\r\n" + 
        		"<td width=\"454\" align=\"left\" style=\"color:#444444;border-collapse:collapse;font-size:11pt;font-family:proxima_nova,'Open Sans','Lucida Grande','Segoe UI',Arial,Verdana,'Lucida Sans Unicode',Tahoma,'Sans Serif';max-width:454px\" valign=\"top\">親愛的貴賓，您好：<br>歡迎您加入成為【時光文創】的會員，請於收到此信件後2個小時內完成註冊流程。<br><br><table style=\"border-radius:4px;width:100%!important;background:#A8D8B9\">\r\n" + 
        		"<tbody><tr>\r\n" + 
        		"<td height=\"16px\"></td>\r\n" + 
        		"<td height=\"16px\"></td>\r\n" + 
        		"<td height=\"16px\"></td>\r\n" + 
        		"</tr>\r\n" + 
        		"<tr>\r\n" + 
        		"<td width=\"20px\"></td>\r\n" + 
        		"<td>\r\n" + 
        		"<span style=\"color:#444;text-align:center\"> <b> 請將驗證碼輸入於註冊驗證頁面！</b> </span><table cellpadding=\"0\" border=\"0\" style=\"color:#444;font-size:14px\" cellspacing=\"0\" align=\"center\">\r\n" + 
        		"<tbody><tr>\r\n" + 
        		"<td height=\"10px\"></td>\r\n" + 
        		"<td height=\"10px\"></td>\r\n" + 
        		"</tr>\r\n" + 
        		"<tr valign=\"top\">\r\n" + 	
        		"<td width=\"90px\">驗證碼：</td>\r\n" + 
        		"<td><b>"+num+"</b></td>\r\n" + 
        		"</tr>\r\n" + 
        		"<tr>\r\n" + 
        		"<td height=\"16px\"></td>\r\n" + 
        		"<td height=\"16px\"></td>\r\n" + 
        		"</tr>\r\n" + 
        		"</tbody></table>\r\n" + 
        		"<table cellpadding=\"0\" border=\"0\" align=\"center\" cellspacing=\"0\" style=\"text-align:center\"><tbody><tr>\r\n" + 
        		"<td width=\"124px\"><a style=\"border-radius:3px;font-size:14px;border-right:1px #b1b1b1 solid;border-bottom:1px #aaaaaa solid;padding:7px 7px 7px 7px;border-top:1px #bfbfbf solid;max-width:97px;font-family:proxima_nova,'Open Sans','lucida grande','Segoe UI',arial,verdana,'lucida sans unicode',tahoma,sans-serif;color:#777777;text-align:center;background-image:-webkit-gradient(linear,0% 0%,0% 100%,from(rgb(251,251,251)),to(rgb(228,228,228)));text-decoration:none;width:97px;border-left:1px #b1b1b1 solid;margin:0;display:block;background-color:#f3f3f3\" href=\"http://localhost:8080/FinalProject/shopping/member_verification.html\"\">註冊驗證頁</a></td>\r\n" + 
        		"<td></td>\r\n" + 
        		"</tr></tbody></ta	ble>\r\n" + 
        		"<table cellpadding=\"0\" border=\"0\" align=\"left\" cellspacing=\"0\" style=\"text-align:left\"><tbody><tr align=\"left\">\r\n" + 
        		"<td width=\"97px\" height=\"0px\"></td>\r\n" + 
        		"</tr></tbody></table>\r\n" + 
        		"<br>\r\n" + 
        		"</td>\r\n" + 
        		"<td width=\"20px\"></td>\r\n" + 
        		"</tr>\r\n" + 
        		"<tr>\r\n" + 
        		"<td height=\"20px\"></td>\r\n" + 
        		"</tr>\r\n" + 
        		"</tbody></table>\r\n" + 
        		"<br>謝謝！<br>時光文創工作團隊<br>\r\n" + 
        		"</td>\r\n" + 
        		"<td width=\"36\"></td>\r\n" + 
        		"</tr>\r\n" + 
        		"<tr><td colspan=\"3\" height=\"36\"></td></tr>\r\n" + 
        		"</tbody></table></td></tr>\r\n" + 
        		"</tbody></table>\r\n" + 
        		"<table cellpadding=\"0\" cellspacing=\"0\" align=\"center\" border=\"0\">\r\n" + 
        		"<tbody><tr><td height=\"10\"></td></tr>\r\n" + 
        		"<tr><td style=\"padding:0;border-collapse:collapse\"><table cellpadding=\"0\" cellspacing=\"0\" align=\"center\" border=\"0\"><tbody><tr style=\"color:#a8b9c6;font-size:11px;font-family:proxima_nova,'Open Sans','Lucida Grande','Segoe UI',Arial,Verdana,'Lucida Sans Unicode',Tahoma,'Sans Serif'\">\r\n" + 
        		"<td width=\"400\" align=\"left\"></td>\r\n" + 
        		"<td width=\"128\" align=\"right\">© 2018 時光文創</td>\r\n" + 
        		"</tr></tbody></table></td></tr>\r\n" + 
        		"</tbody></table>\r\n" + 
        		"</td></tr></tbody></table></center></div>")); // mail內容
  
        
        
//        formParams.add(new BasicNameValuePair("mmail", "hsiaoelf8895@gmail.com")); // 權限帳號 xxxx@gmail.com 
//        formParams.add(new BasicNameValuePair("umail", "member email")); // 發送的email aaa@gmail 或是 aaa@gmail,bbb@gmail
//        formParams.add(new BasicNameValuePair("subject", "XXX文創動手做 會員註冊驗證 ")); // mail 的主旨
//        formParams.add(new BasicNameValuePair("body", "Hello XXX :<br>歡迎使用XXX文創動手做 ，我們需要快速驗證您的電子郵件地址"+num+"。<br><br><a href=\"http://localhost:8080/FinalProject/shopping/MemberCenterList.html\">驗證您的電子郵件</a><br><br>")); // mail內容
//        formParams.add(new BasicNameValuePair("imageUrl", "http://chart.apis.google.com/chart?cht=qr&chl="+文字+"&chs=240x240")); 
//        formParams.add(new BasicNameValuePair("imageUrl", "https://i.gbc.tw/gb_img/3642539l.jpg")); // mail圖片
        
        
        
        
        UrlEncodedFormEntity uefEntity;  
        String responseString = "";
        
        try {
            uefEntity = new UrlEncodedFormEntity(formParams, "UTF-8");  
            httppost.setEntity(uefEntity);  
            System.out.println("executing request " + httppost.getURI());  
            CloseableHttpResponse response = httpclient.execute(httppost);  
            try {  
                HttpEntity entity = response.getEntity();  
                if (entity != null) {  
                	responseString = EntityUtils.toString(entity, "UTF-8");
                    System.out.println("--------------------------------------");  
                    System.out.println("Response content: " + responseString);  
                    System.out.println("--------------------------------------");  
                }  
            } finally {  
                response.close();  
            }  
        } catch (ClientProtocolException e) {  
            e.printStackTrace();  
        } catch (UnsupportedEncodingException e1) {  
            e1.printStackTrace();  
        } catch (IOException e) {  
            e.printStackTrace();  
        } finally {  
            // 关闭连接,释放资源    
            try {  
                httpclient.close();  
            } catch (IOException e) {  
                e.printStackTrace();  
            }  
        }  
        
        return responseString;
        
    }  
    
    //忘記密碼
    /**
     * post方法(表单提交)
     * @throws NoSuchAlgorithmException 
     * @throws KeyManagementException 
     */
    public String httpPostFormPswSend(String umail,String psw) throws NoSuchAlgorithmException, KeyManagementException {
    	
        // 创建默认的httpClient实例.    
//        CloseableHttpClient httpclient = HttpClients.createDefault();  
        CloseableHttpClient httpclient = HttpClientUtil.createSSLClientDefault();
        
        // 创建httppost    
        HttpPost httppost = new HttpPost(InYearHttpPost.urls);
        //int num=(int)(Math.random()*100000+1);
        // 创建参数队列    
        List<NameValuePair> formParams = new ArrayList<NameValuePair>();  
        formParams.add(new BasicNameValuePair("mmail", "hsiaoelf8895@gmail.com")); // 權限帳號 xxxx@gmail.com 
        formParams.add(new BasicNameValuePair("umail", umail)); // 發送的email aaa@gmail 或是 aaa@gmail,bbb@gmail
        formParams.add(new BasicNameValuePair("subject", "【時光文創】會員臨時密碼信  ")); // mail 的主旨
        formParams.add(new BasicNameValuePair("body",
        		"<div style=\"padding:0;width:100%!important;margin:0\" marginheight=\"0\" marginwidth=\"0\"><center><table cellpadding=\"8\" cellspacing=\"0\" style=\"padding:0;width:100%!important;background:#ffffff;margin:0;background-color:#ffffff\" border=\"0\"><tbody><tr><td valign=\"top\">\r\n" + 
        		"<table cellpadding=\"0\" cellspacing=\"0\" style=\"border-radius:4px;border:1px #A8D8B9 solid\" border=\"0\" align=\"center\">\r\n" + 
        		"<tbody><tr><td colspan=\"3\" height=\"6\"></td></tr>\r\n" + 
        		"<tr style=\"line-height:0px\"><td width=\"100%\" style=\"font-size:0px\" align=\"center\" height=\"1\"><img src=\"https://www.shareicon.net/data/128x128/2016/11/14/852139_camera_512x512.png\" style=\"max-height:73px;width:40px\" alt=\"\" width=\"40px\" class=\"CToWUd\"></td></tr> <tr><td><table cellpadding=\"0\" cellspacing=\"0\" style=\"line-height:25px\" border=\"0\" align=\"center\">\r\n" + 
        		"<tbody><tr><td colspan=\"3\" height=\"30\"></td></tr>\r\n" + 
        		"<tr>\r\n" + 
        		"<td width=\"36\"></td>\r\n" + 
        		"<td width=\"454\" align=\"left\" style=\"color:#444444;border-collapse:collapse;font-size:11pt;font-family:proxima_nova,'Open Sans','Lucida Grande','Segoe UI',Arial,Verdana,'Lucida Sans Unicode',Tahoma,'Sans Serif';max-width:454px\" valign=\"top\">親愛的貴賓，您好：<br>建議您即刻更改帳號新密碼，以免再次又忘記密碼囉！<br><br><table style=\"border-radius:4px;width:100%!important;background:#A8D8B9\">\r\n" + 
        		"<tbody><tr>\r\n" + 
        		"<td height=\"16px\"></td>\r\n" + 
        		"<td height=\"16px\"></td>\r\n" + 
        		"<td height=\"16px\"></td>\r\n" + 
        		"</tr>\r\n" + 
        		"<tr>\r\n" + 
        		"<td width=\"20px\"></td>\r\n" + 
        		"<td>\r\n" + 
        		"<span style=\"color:#444;text-align:center\"> <b> 請利用臨時密碼登入您的帳號！</b> </span><table cellpadding=\"0\" border=\"0\" style=\"color:#444;font-size:14px\" cellspacing=\"0\" align=\"center\">\r\n" + 
        		"<tbody><tr>\r\n" + 
        		"<td height=\"10px\"></td>\r\n" + 
        		"<td height=\"10px\"></td>\r\n" + 
        		"</tr>\r\n" + 
        		"<tr valign=\"top\">\r\n" + 	
        		"<td width=\"90px\">臨時密碼：</td>\r\n" + 
        		"<td><b>"+psw+"</b></td>\r\n" + 
        		"</tr>\r\n" + 
        		"<tr>\r\n" + 
        		"<td height=\"16px\"></td>\r\n" + 
        		"<td height=\"16px\"></td>\r\n" + 
        		"</tr>\r\n" + 
        		"</tbody></table>\r\n" + 
        		"<table cellpadding=\"0\" border=\"0\" align=\"center\" cellspacing=\"0\" style=\"text-align:center\"><tbody><tr>\r\n" + 
        		"<td width=\"124px\"><a style=\"border-radius:3px;font-size:14px;border-right:1px #b1b1b1 solid;border-bottom:1px #aaaaaa solid;padding:7px 7px 7px 7px;border-top:1px #bfbfbf solid;max-width:97px;font-family:proxima_nova,'Open Sans','lucida grande','Segoe UI',arial,verdana,'lucida sans unicode',tahoma,sans-serif;color:#777777;text-align:center;background-image:-webkit-gradient(linear,0% 0%,0% 100%,from(rgb(251,251,251)),to(rgb(228,228,228)));text-decoration:none;width:97px;border-left:1px #b1b1b1 solid;margin:0;display:block;background-color:#f3f3f3\" href=\"http://localhost:8080/FinalProject/RegisterandLogin/Register.html\"\">立刻登入</a></td>\r\n" + 
        		"<td></td>\r\n" + 
        		"</tr></tbody></ta	ble>\r\n" + 
        		"<table cellpadding=\"0\" border=\"0\" align=\"left\" cellspacing=\"0\" style=\"text-align:left\"><tbody><tr align=\"left\">\r\n" + 
        		"<td width=\"97px\" height=\"0px\"></td>\r\n" + 
        		"</tr></tbody></table>\r\n" + 
        		"<br>\r\n" + 
        		"</td>\r\n" + 
        		"<td width=\"20px\"></td>\r\n" + 
        		"</tr>\r\n" + 
        		"<tr>\r\n" + 
        		"<td height=\"20px\"></td>\r\n" + 
        		"</tr>\r\n" + 
        		"</tbody></table>\r\n" + 
        		"<br>時光文創工作團隊關心您！<br>\r\n" + 
        		"</td>\r\n" + 
        		"<td width=\"36\"></td>\r\n" + 
        		"</tr>\r\n" + 
        		"<tr><td colspan=\"3\" height=\"36\"></td></tr>\r\n" + 
        		"</tbody></table></td></tr>\r\n" + 
        		"</tbody></table>\r\n" + 
        		"<table cellpadding=\"0\" cellspacing=\"0\" align=\"center\" border=\"0\">\r\n" + 
        		"<tbody><tr><td height=\"10\"></td></tr>\r\n" + 
        		"<tr><td style=\"padding:0;border-collapse:collapse\"><table cellpadding=\"0\" cellspacing=\"0\" align=\"center\" border=\"0\"><tbody><tr style=\"color:#a8b9c6;font-size:11px;font-family:proxima_nova,'Open Sans','Lucida Grande','Segoe UI',Arial,Verdana,'Lucida Sans Unicode',Tahoma,'Sans Serif'\">\r\n" + 
        		"<td width=\"400\" align=\"left\"></td>\r\n" + 
        		"<td width=\"128\" align=\"right\">© 2018 時光文創</td>\r\n" + 
        		"</tr></tbody></table></td></tr>\r\n" + 
        		"</tbody></table>\r\n" + 
        		"</td></tr></tbody></table></center></div>")); // mail內容
  
        
        
//        formParams.add(new BasicNameValuePair("mmail", "hsiaoelf8895@gmail.com")); // 權限帳號 xxxx@gmail.com 
//        formParams.add(new BasicNameValuePair("umail", "member email")); // 發送的email aaa@gmail 或是 aaa@gmail,bbb@gmail
//        formParams.add(new BasicNameValuePair("subject", "XXX文創動手做 會員註冊驗證 ")); // mail 的主旨
//        formParams.add(new BasicNameValuePair("body", "Hello XXX :<br>歡迎使用XXX文創動手做 ，我們需要快速驗證您的電子郵件地址"+num+"。<br><br><a href=\"http://localhost:8080/FinalProject/shopping/MemberCenterList.html\">驗證您的電子郵件</a><br><br>")); // mail內容
//        formParams.add(new BasicNameValuePair("imageUrl", "http://chart.apis.google.com/chart?cht=qr&chl="+文字+"&chs=240x240")); 
//        formParams.add(new BasicNameValuePair("imageUrl", "https://i.gbc.tw/gb_img/3642539l.jpg")); // mail圖片
        
        
        
        
        UrlEncodedFormEntity uefEntity;  
        String responseString = "";
        
        try {
            uefEntity = new UrlEncodedFormEntity(formParams, "UTF-8");  
            httppost.setEntity(uefEntity);  
            System.out.println("executing request " + httppost.getURI());  
            CloseableHttpResponse response = httpclient.execute(httppost);  
            try {  
                HttpEntity entity = response.getEntity();  
                if (entity != null) {  
                	responseString = EntityUtils.toString(entity, "UTF-8");
                    System.out.println("--------------------------------------");  
                    System.out.println("Response content: " + responseString);  
                    System.out.println("--------------------------------------");  
                }  
            } finally {  
                response.close();  
            }  
        } catch (ClientProtocolException e) {  
            e.printStackTrace();  
        } catch (UnsupportedEncodingException e1) {  
            e1.printStackTrace();  
        } catch (IOException e) {  
            e.printStackTrace();  
        } finally {  
            // 关闭连接,释放资源    
            try {  
                httpclient.close();
            } catch (IOException e) {  
                e.printStackTrace();
            }  
        }  
        
        return responseString;
        
    }  
    
}
