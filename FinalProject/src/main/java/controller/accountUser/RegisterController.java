package controller.accountUser;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import model.AccountUserBean;
import model.StatusBean;
import model.AccountUser.AccountUser_Service;
@Controller
public class RegisterController {

	@Autowired
	private AccountUser_Service accountService;

	@RequestMapping(value = "/shopping/RegisterServlet.go", produces = "text/html;charset=utf-8", method = {
			RequestMethod.POST })
	@ResponseBody
	public String method(String email, String psw1, String psw2) {
		if (!"".equals(email) && !"".equals(psw1) && !"".equals(psw2)) {
			AccountUserBean result=accountService.selectAcc(email);

			if (result==null) {
				if(psw1.equals(psw2)) {
					accountService.Register(new AccountUserBean(email,psw1,new StatusBean((byte) 1, null)));
					return "註冊成功!";					
				}
				else {
					return "密碼不一致!";
				}
			}

			else {

				return "此帳號已註冊!";
			}
		}
		return "請輸入完整!";
	}
	
	
	
	@RequestMapping(value = "/shopping/FbLogin.go", produces = "text/html;charset=utf-8", method = {
			RequestMethod.POST })
	@ResponseBody
	public String method1(String femail,String fpsw) {
		AccountUserBean result=accountService.selectAcc(femail);
		if (result==null) {
//			System.out.println(femail+fpsw);
			accountService.Register(new AccountUserBean(femail,fpsw,new StatusBean((byte) 2, null)));
			return "已啟用";
		}
		else {
			return "已是會員";		
		}
		
	}
	
	@RequestMapping(value = "/shopping/GoogleLogin.go", produces = "text/html;charset=utf-8", method = {
			RequestMethod.POST })
	@ResponseBody
	public String method2(String gemail,String gpsw) {
		AccountUserBean result=accountService.selectAcc(gemail);
		if (result==null) {
//			System.out.println(gemail+gpsw);
			
			accountService.Register(new AccountUserBean(gemail, gpsw,new StatusBean((byte) 2, null)));
			return "已啟用";
		}

		else {

				return "已是會員";					
			
		}
	}
}
