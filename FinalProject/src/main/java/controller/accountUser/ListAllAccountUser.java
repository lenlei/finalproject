package controller.accountUser;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import model.AccountUser.AccountUserService;

@Controller
public class ListAllAccountUser {
	@Autowired
	private AccountUserService aus;
	
	@RequestMapping(path="/backstage/ListAllAccountUser.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String listAllAccountUser() {
		return new Gson().toJson(aus.selectAll());
	}
	@RequestMapping(path="/backstage/ListAllAccountUserDesc.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String selectAllByEmailDesc() {
		return new Gson().toJson(aus.selectAllByEmailDesc());
	}
	@RequestMapping(path="/backstage/ListAllAccountUserAsc.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String selectAllByEmailAsc() {
		return new Gson().toJson(aus.selectAllByEmailAsc());
	}
	@RequestMapping(path="/backstage/ListKeyAccountUserAsc.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String selectKeyupEmailAsc2(String searchKeyword) {
		System.out.println(searchKeyword);
		return new Gson().toJson(aus.selectKeyupEmailAsc2(searchKeyword));
	}
	@RequestMapping(path="/backstage/ListKeyAccountUserDesc.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String selectKeyupEmailDesc2(String searchKeyword) {
		return new Gson().toJson(aus.selectKeyupEmailDesc2(searchKeyword));
}
}
