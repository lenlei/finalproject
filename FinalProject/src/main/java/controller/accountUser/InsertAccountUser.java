package controller.accountUser;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import model.AccountUserBean;
import model.StatusBean;
import model.AccountUser.AccountUserService;

@Controller
public class InsertAccountUser {

	@Autowired
	private AccountUserService aus;
	@RequestMapping(path="/backstage/registerAccountUser.do",method=RequestMethod.POST)
	public String insertAccountUser(AccountUserBean aub, String dbSId) {
		
		if(dbSId==null) {
			aub.setDbAStatus(new StatusBean((byte)7, null));
		}else {			
			aub.setDbAStatus(new StatusBean((byte)2, null));
		}
		
		aus.insert(aub);
		return "accountuserIndex";
	}
	
}
