package controller.accountUser;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import model.AccountUserBean;
import model.StatusBean;
import model.AccountUser.AccountUserService;

@Controller
public class UpdateAccountUser {
		@Autowired
		private AccountUserService aus;
		@RequestMapping(path="/backstage/updateAccountUser.do",method=RequestMethod.POST)
		public String insertAccountUser(AccountUserBean aub,String dbSId) {
					
			if(dbSId==null) {
				aub.setDbAStatus(new StatusBean((byte)7, null));
			}else {			
				aub.setDbAStatus(new StatusBean((byte)2, null));
			}
			aus.update(aub);
			return "accountuserIndex";	
		}
		
		@RequestMapping(path="/backstage/selectUpdateAccountUser.do",method=RequestMethod.POST,produces="text/html;charset=UTF-8")
		@ResponseBody
		public String selectUpdateAccountUser(AccountUserBean aub) {
			return new Gson().toJson(aus.selectUpdate(aub));
		}
}
