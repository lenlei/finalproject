package controller.address;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import model.cityAndRegion.CityService;

@Controller
public class CityAndRegion {
	@Autowired
	private CityService cityService;
	@RequestMapping(path="/test/test.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String listAllCity() {
		System.out.println(new Gson().toJson(cityService.selectAllCity()));
		return new Gson().toJson(cityService.selectAllCity());
	}
	
	@RequestMapping(path="/backstage/ListCity.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String listAllCity1() {
		System.out.println(new Gson().toJson(cityService.selectAllCity()));
		return new Gson().toJson(cityService.selectAllCity());
	}
}
