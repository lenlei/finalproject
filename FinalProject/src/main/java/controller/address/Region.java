package controller.address;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import model.cityAndRegion.CityService;
@Controller
public class Region {
	@Autowired
	private CityService cityService;
	@RequestMapping(path="/test/findCityName",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String listAllRegion(int cid) {
	
		return new Gson().toJson(cityService.selectAllRegion(cid));
	}
	
	@RequestMapping(path="/backstage/findCityName.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String listAllRegion1(Integer cid) {
		System.out.println(cid);
		if(cid==null) {
			cid=1;
		}
		return new Gson().toJson(cityService.selectAllRegion(cid));
	}
}
