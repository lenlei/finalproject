package configuration;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.support.ServletContextResource;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.XmlViewResolver;

@Configuration
@ComponentScan(basePackages= {"controller"})
@EnableWebMvc
public class SpringMVCConfiguration implements WebMvcConfigurer{
	@Autowired
	private ServletContext servletContext;

	@Override
	public void configureViewResolvers(ViewResolverRegistry registry) {
		XmlViewResolver xvr = new XmlViewResolver();
		xvr.setLocation(new ServletContextResource(servletContext, "\\WEB-INF\\spring-view.xml"));
		registry.viewResolver(xvr);
		
		MultipartResolver mr = new CommonsMultipartResolver() ;
		
		
		InternalResourceViewResolver irvr = new InternalResourceViewResolver();
		irvr.setPrefix("/");

		irvr.setSuffix(".html");
		registry.viewResolver(irvr);
	}
	
	@Override					//告訴MVC你的檔案會在根目錄內
	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer)
	{configurer.enable();}
	
//	@Bean
//	public MessageSource messageSource() {
//		ResourceBundleMessageSource rbms = new ResourceBundleMessageSource();
//		rbms.setBasename("errorMessage");
//		return rbms;
//	}
	
	// Bean name must be "multipartResolver", by default Spring uses method name as bean name.
	@Bean
	public CommonsMultipartResolver multipartResolver() {
	    CommonsMultipartResolver resolver=new CommonsMultipartResolver();
	    resolver.setDefaultEncoding("utf-8");
	    return resolver;
	}
	
	
}
