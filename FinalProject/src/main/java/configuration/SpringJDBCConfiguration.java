package configuration;

import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jndi.JndiObjectFactoryBean;
import org.springframework.orm.hibernate5.LocalSessionFactoryBuilder;

@Configuration
@ComponentScan(basePackages= {"model"})
public class SpringJDBCConfiguration {
	@Bean
	public DataSource dataSource() {
		JndiObjectFactoryBean datasource = new JndiObjectFactoryBean();
		datasource.setJndiName("java:comp/env/jdbc/ShoppingWeb");
		datasource.setProxyInterface(DataSource.class);
		try {
		datasource.afterPropertiesSet();
		} catch (Exception e) {
		e.printStackTrace();
		}
		return (DataSource) datasource.getObject();
	}
	
	@Bean
	public SessionFactory sessionFactory() {
		LocalSessionFactoryBuilder builder = new LocalSessionFactoryBuilder(dataSource());
		
		Properties props = new Properties();
		props.put("hibernate.hbm2ddl.auto", "update");
		props.put("hibernate.show_sql", "true");
		props.put("hibernate.format_sql", "true");
		props.put("hibernate.current_session_context_class", "thread");
		props.put("hibernate.dialect", "org.hibernate.dialect.SQLServer2012Dialect");
		builder.addProperties(props);

		builder.scanPackages("model");
			
		return builder.buildSessionFactory();
	}
}
