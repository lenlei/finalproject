package model.seo;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import model.SEODescriptionBean;
import model.SEOKeyWordBean;
import model.SEOSetBean;
@Repository
public class SeoDAO implements SeoIDAO {
	@Autowired
	private SessionFactory sessionFactory;
	@Override
	public void insert(SEOSetBean sb) {
		sessionFactory.getCurrentSession().save(sb);
	}

	@Override
	public void update(SEOSetBean sb) {

		SEOSetBean seobean = sessionFactory.getCurrentSession().get(SEOSetBean.class, 1);
		seobean.getSEODescription().setDbSDescription(sb.getSEODescription().getDbSDescription());
		seobean.getSEODescription().setDbSStatus(sb.getSEODescription().getDbSStatus());
		seobean.getSEOKeyWord().setDbSKeyWord(sb.getSEOKeyWord().getDbSKeyWord());
		seobean.getSEOKeyWord().setDbSStatus(sb.getSEOKeyWord().getDbSStatus());

	}
	

	@Override
	public void delete(SEOSetBean sb) {
		sessionFactory.getCurrentSession().delete(sb);
	}

	@SuppressWarnings("unchecked")
	@Override

	public List<SEODescriptionBean> selectDspt() {

		String HQL = "From SEODescriptionBean";
		List<SEODescriptionBean> sb=sessionFactory.getCurrentSession().createQuery(HQL).getResultList();

		return sb;
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<SEOKeyWordBean> selectKeyword() {

		String HQL = "From SEOKeyWordBean";
		List<SEOKeyWordBean> sk=sessionFactory.getCurrentSession().createQuery(HQL).getResultList();
	
		return sk;
	}
	

	
}
