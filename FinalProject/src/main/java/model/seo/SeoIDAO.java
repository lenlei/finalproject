package model.seo;

import java.util.List;

import model.SEODescriptionBean;
import model.SEOKeyWordBean;
import model.SEOSetBean;

public interface SeoIDAO {

	public void delete(SEOSetBean sb);
	public void insert(SEOSetBean sb);
	public void update(SEOSetBean sb);

	public List<SEODescriptionBean> selectDspt();
	public List<SEOKeyWordBean> selectKeyword();
	


	
}
