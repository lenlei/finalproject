package model.seo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import model.SEODescriptionBean;
import model.SEOKeyWordBean;
import model.SEOSetBean;

@Service
public class SeoService {

	@Autowired
	private SeoIDAO SDAO;

	public void insert(SEOSetBean sb) {
		SDAO.insert(sb);
	}

	public void update(SEOSetBean sb) {
		SDAO.update(sb);
	}

	

	public void delete(SEOSetBean sb) {
		SDAO.delete(sb);
	}

	public List<SEODescriptionBean> selectDspt() {
		return SDAO.selectDspt();
	}
	public List<SEOKeyWordBean> selectKeyword() {
		
		return SDAO.selectKeyword();
	}



}
