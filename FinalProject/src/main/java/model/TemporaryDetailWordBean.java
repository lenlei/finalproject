package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="TemporaryDetailWord")
public class TemporaryDetailWordBean {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer pk;
	@Column(columnDefinition="int")
	private Integer dbTDWPage;
	@Column(columnDefinition="nvarchar(100)")
	private String dbTDWLocation;
	@Column(columnDefinition="nvarchar(100)")
	private String dbTDWWord;
	@Column(columnDefinition="nvarchar(100)")
	private String dbTDWWordSize;
	@Column(columnDefinition="nvarchar(100)")
	private String dbTDWWordColor;
	@Column(columnDefinition="nvarchar(100)")
	private String dbTDWWordFamily;
	@Column(columnDefinition="nvarchar(100)")
	private String dbTDWWordWeight;
	@ManyToOne
	@JoinColumn(name="dbTDWProductId")
	private ProductBean dbTDIProductId;
	@Column(columnDefinition="int")
	private Integer dbTDWId;
	
	public TemporaryDetailWordBean() {
		super();
	}

	public TemporaryDetailWordBean(Integer dbTDWPage, String dbTDWLocation, String dbTDWWord, String dbTDWWordSize,
			String dbTDWWordColor, String dbTDWWordFamily, String dbTDWWordWeight, ProductBean dbTDIProductId,
			Integer dbTDWId) {
		super();
		this.dbTDWPage = dbTDWPage;
		this.dbTDWLocation = dbTDWLocation;
		this.dbTDWWord = dbTDWWord;
		this.dbTDWWordSize = dbTDWWordSize;
		this.dbTDWWordColor = dbTDWWordColor;
		this.dbTDWWordFamily = dbTDWWordFamily;
		this.dbTDWWordWeight = dbTDWWordWeight;
		this.dbTDIProductId = dbTDIProductId;
		this.dbTDWId = dbTDWId;
	}

	public Integer getPk() {return pk;}
	public void setPk(Integer pk) {this.pk = pk;}

	public String getDbTDWLocation() {return dbTDWLocation;}
	public void setDbTDWLocation(String dbTDWLocation) {this.dbTDWLocation = dbTDWLocation;}

	public String getDbTDWWord() {return dbTDWWord;}
	public void setDbTDWWord(String dbTDWWord) {this.dbTDWWord = dbTDWWord;	}

	public ProductBean getDbTDIProductId() {return dbTDIProductId;}
	public void setDbTDIProductId(ProductBean dbTDIProductId) {this.dbTDIProductId = dbTDIProductId;}

	public Integer getDbTDIId() {return dbTDWId;}
	public void setDbTDIId(Integer dbTDWId) {this.dbTDWId = dbTDWId;}

	public Integer getDbTDWPage() {return dbTDWPage;}
	public void setDbTDWPage(Integer dbTDWPage) {this.dbTDWPage = dbTDWPage;}
	
	public String getDbTDWWordSize() {return dbTDWWordSize;}
	public void setDbTDWWordSize(String dbTDWWordSize) {this.dbTDWWordSize = dbTDWWordSize;}

	public String getDbTDWWordColor() {return dbTDWWordColor;}
	public void setDbTDWWordColor(String dbTDWWordColor) {this.dbTDWWordColor = dbTDWWordColor;}

	public String getDbTDWWordFamily() {return dbTDWWordFamily;}
	public void setDbTDWWordFamily(String dbTDWWordFamily) {this.dbTDWWordFamily = dbTDWWordFamily;}

	public String getDbTDWWordWeight() {return dbTDWWordWeight;}
	public void setDbTDWWordWeight(String dbTDWWordWeight) {this.dbTDWWordWeight = dbTDWWordWeight;}

	public Integer getDbTDWId() {return dbTDWId;}
	public void setDbTDWId(Integer dbTDWId) {this.dbTDWId = dbTDWId;}
	
	
}
