package model.temporaryPreview;

import java.util.List;

import model.TemporaryPreviewBean;

public interface I_tempPreviewDAO {
	public List<TemporaryPreviewBean> PreviewImgsBydbTPId(Integer dbTPId);
	public TemporaryPreviewBean PreviewImgsFilmBydbTPId(Integer dbTPId);
}
