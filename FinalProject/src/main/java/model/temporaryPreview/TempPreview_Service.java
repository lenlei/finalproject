package model.temporaryPreview;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import model.TemporaryPreviewBean;
@Service
public class TempPreview_Service {
	@Autowired
	private I_tempPreviewDAO tempPreviewDAO;

	public List<TemporaryPreviewBean> PreviewImgsBydbTPId(Integer dbTPId){
		return tempPreviewDAO.PreviewImgsBydbTPId(dbTPId);
	}
	
	public TemporaryPreviewBean PreviewImgsFilmBydbTPId(Integer dbTPId) {
		return tempPreviewDAO.PreviewImgsFilmBydbTPId(dbTPId);
	}
}
