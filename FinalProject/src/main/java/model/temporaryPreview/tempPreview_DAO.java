package model.temporaryPreview;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import model.TemporaryPreviewBean;
@Repository
public class tempPreview_DAO implements I_tempPreviewDAO {
	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<TemporaryPreviewBean> PreviewImgsBydbTPId(Integer dbTPId) {
		String HQL = "From TemporaryPreviewBean where dbTPId = :PId order by dbTPage";
		List<TemporaryPreviewBean> previewImgs = sessionFactory.getCurrentSession().createQuery(HQL).setParameter("PId", dbTPId).getResultList();
		return previewImgs;
	}

	@Override
	public TemporaryPreviewBean PreviewImgsFilmBydbTPId(Integer dbTPId) {
		String HQL = "From TemporaryPreviewBean where dbTPId = :id";
		TemporaryPreviewBean bean = (TemporaryPreviewBean) sessionFactory.getCurrentSession().createQuery(HQL).setParameter("id", dbTPId).getSingleResult();
		return bean;
	}

}
