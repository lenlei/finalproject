package model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="TemporaryWork")
public class TemporaryBean {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer dbTId;
	@Column(columnDefinition="nvarchar(50)")
	private String dbTTheme = "";
	@Column(columnDefinition="nvarchar(200)")
	private String dbTIntroduction = ""; 
	@Column(columnDefinition = "varchar(40)")
	private String dbAAccount;
	@Column(columnDefinition="int")
	private Integer dbPId;
	@ManyToOne
	@JoinColumn(name="dbTStatus")
	private StatusBean dbTStatus;
	@OneToMany(mappedBy="dbTDIId")
	private List<TemporaryDetailImageBean> TemporaryDetailImage;
	@OneToMany(mappedBy="dbTDWId")
	private List<TemporaryDetailWordBean> TemporaryDetailWord;
	@OneToMany(mappedBy="dbTPId")
	private List<TemporaryPreviewBean> TemporaryPreview;
	
	public TemporaryBean() {
		super();
	}
	
	public TemporaryBean(String dbTTheme, String dbTIntroduction, String dbAAccount,
			StatusBean dbTStatus) {
		super();
		this.dbTTheme = dbTTheme;
		this.dbTIntroduction = dbTIntroduction;
		this.dbAAccount = dbAAccount;
		this.dbTStatus = dbTStatus;
	}
		
	public TemporaryBean(Integer dbTId, String dbTTheme, String dbTIntroduction, String dbAAccount,
			StatusBean dbTStatus, List<TemporaryDetailImageBean> temporaryDetailImage,
			List<TemporaryDetailWordBean> temporaryDetailWord,List<TemporaryPreviewBean> TemporaryPreview) {
		super();
		this.dbTId = dbTId;
		this.dbTTheme = dbTTheme;
		this.dbTIntroduction = dbTIntroduction;
		this.dbAAccount = dbAAccount;
		this.dbTStatus = dbTStatus;
		this.TemporaryDetailImage = temporaryDetailImage;
		this.TemporaryDetailWord = temporaryDetailWord;
		this.TemporaryPreview = TemporaryPreview;
	}
	
	



	public Integer getDbPId() {
		return dbPId;
	}

	public void setDbPId(Integer dbPId) {
		this.dbPId = dbPId;
	}

	public Integer getDbTId() {return dbTId;}
	public void setDbTId(Integer dbTId) {this.dbTId = dbTId;}
	
	public String getDbTTheme() {return dbTTheme;}
	public void setDbTTheme(String dbTTheme) {this.dbTTheme = dbTTheme;}

	public String getDbTIntroduction() {return dbTIntroduction;}
	public void setDbTIntroduction(String dbTIntroduction) {this.dbTIntroduction = dbTIntroduction;}


	public String getDbAAccount() {
		return dbAAccount;
	}

	public void setDbAAccount(String dbAAccount) {
		this.dbAAccount = dbAAccount;
	}

	public List<TemporaryDetailImageBean> getTemporaryDetailImage() {
		return TemporaryDetailImage;
	}

	public void setTemporaryDetailImage(List<TemporaryDetailImageBean> temporaryDetailImage) {
		TemporaryDetailImage = temporaryDetailImage;
	}

	public List<TemporaryDetailWordBean> getTemporaryDetailWord() {
		return TemporaryDetailWord;
	}

	public void setTemporaryDetailWord(List<TemporaryDetailWordBean> temporaryDetailWord) {
		TemporaryDetailWord = temporaryDetailWord;
	}

	public StatusBean getDbTStatus() {return dbTStatus;}
	public void setDbTStatus(StatusBean dbTStatus) {this.dbTStatus = dbTStatus;}

	public List<TemporaryPreviewBean> getTemporaryPreview() {return TemporaryPreview;}
	public void setTemporaryPreview(List<TemporaryPreviewBean> temporaryPreview) {TemporaryPreview = temporaryPreview;}
	
	
	
}

