package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
@Entity
@Table(name="CompanyProcessPic")
public class CompanyProcessPicBean {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer pk;
	@Column(columnDefinition="nvarchar(max)")
	private String dbIProcesspics; 
	@ManyToOne
	@JoinColumn(name="dbIStatus",nullable=false)
	private StatusBean dbIStatus;
	
	public CompanyProcessPicBean() {
		super();
	}

	public CompanyProcessPicBean(Integer pk, String dbIProcesspics, StatusBean dbIStatus) {
		super();
		this.pk = pk;
		this.dbIProcesspics = dbIProcesspics;
		this.dbIStatus = dbIStatus;
	}

	public Integer getPk() {return pk;}
	public void setPk(Integer pk) {this.pk = pk;}

	public String getDbIProcesspics() {return dbIProcesspics;}
	public void setDbIProcesspics(String dbIProcesspics) {this.dbIProcesspics = dbIProcesspics;}

	public StatusBean getDbIStatus() {return dbIStatus;}
	public void setDbIStatus(StatusBean dbIStatus) {this.dbIStatus = dbIStatus;}
	
}
