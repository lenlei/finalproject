package model.AccountUser;

import model.AccountUserBean;
import model.CityBean;
import model.RegionBean;
import model.StatusBean;

public interface I_AccountUserDAO {
	// 判斷dbAAccount(會員帳號)是否已經被現有客戶使用，如果是，傳回true，表示此帳號不能使用，否則傳回false，表示此帳號可使用。
	public boolean userExists(String dbAAccount);

	// 註冊AccountUserBean物件，將參數ac新增到AccountUser表格內。
	public void Register(AccountUserBean ac);

	// 由參數 dbAAccount (會員帳號) 到AccountUser表格中
	// 取得某個會員的所有資料，傳回值為一個AccountUserBean物件，如果找不到對應的會員資料，傳回值為null。
	public AccountUserBean queryUser(String dbAAccount);

	// 檢查使用者在登入時輸入的帳號 & 密碼是否正確。如果正確，傳回該帳號所對應的AccountUserBean物件，否則傳回 null。
	public AccountUserBean checkPassword(String dbAAccount, String dbAPassword);

	// 更新會員資料
	public AccountUserBean updateUserDetails(AccountUserBean userBean);
	
	//註冊使用
	public AccountUserBean selectAcc(String  acc);
	
	public AccountUserBean updateMemInfo(String dbAAccount, String dbAPassword, String dbAName, String dbAPickupDetail,
			CityBean dbAPickpCity, RegionBean dbAPickupRegion, String dbAMobile,StatusBean dbAStatus);

	public AccountUserBean memVerifiy(String acc, Integer verification,Long verificationtime);
	
	public AccountUserBean memVerifiySuccess(String acc,StatusBean status);
	
	public String selectStatus(String  acc);
	
	public AccountUserBean changePsw(String acc,String psw);
	
	public void updatePickUpDetail(AccountUserBean ac);//edit by Eric
}
