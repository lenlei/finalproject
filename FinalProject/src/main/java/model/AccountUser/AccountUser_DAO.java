package model.AccountUser;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import model.AccountUserBean;
import model.CityBean;
import model.RegionBean;
import model.StatusBean;

@Repository
public class AccountUser_DAO implements I_AccountUserDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public Session getSession() {
		// 透過sessionFactory取得getCurrentSession()與資料庫交易
		return this.sessionFactory.getCurrentSession();
	}

	public AccountUser_DAO() {

	}

	// 判斷dbAAccount(會員帳號)是否已經被現有客戶使用，如果是，傳回true，表示此帳號不能使用，否則傳回false，表示此帳號可使用。
	@Override
	public boolean userExists(String dbAAccount) {
		boolean isExist = false;
		String hql = "FROM AccountUserBean au WHERE au.dbAAccount = :account";
		AccountUserBean user = (AccountUserBean) this.getSession().createQuery(hql).setParameter("account", dbAAccount)
				.getSingleResult();
		if (user != null) {
			isExist = true;
		} else {
			isExist = false;
		}
		return isExist;
	}

	// 註冊AccountUserBean物件，將參數ac新增到AccountUser表格內。
	@Override
	public void Register(AccountUserBean ac) {
		this.getSession().save(ac);
	}

	// 由參數 dbAAccount (會員帳號) 到AccountUser表格中
	// 取得某個會員的所有資料，傳回值為一個AccountUserBean物件，如果找不到對應的會員資料，傳回值為null。

	@SuppressWarnings("rawtypes")
	@Override
	public AccountUserBean queryUser(String dbAAccount) {
		AccountUserBean user = null;
		String hql = "FROM AccountUserBean au WHERE au.dbAAccount = :account";
		Query qry = this.getSession().createQuery(hql).setParameter("account", dbAAccount);

		try {
			user = (AccountUserBean) qry.getSingleResult();
		} catch (NoResultException e) {
			user = null;
		}
		return user;
	}

	// 檢查使用者在登入時輸入的帳號 & 密碼是否正確。如果正確，傳回該帳號所對應的AccountUserBean物件，否則傳回 null。
	@Override
	public AccountUserBean checkPassword(String dbAAccount, String dbAPassword) {
		AccountUserBean userBean = null;
		String hql = "FROM AccountUserBean au WHERE au.dbAAccount = :account and au.dbAPassword = :pswd";
		try {
			userBean = (AccountUserBean) this.getSession().createQuery(hql).setParameter("account", dbAAccount)
					.setParameter("pswd", dbAPassword).getSingleResult();
		} catch (NoResultException e) {
			userBean = null;
		}
		return userBean;
	}

	// 更新會員資料
	@Override
	public AccountUserBean updateUserDetails(AccountUserBean bean) {
		AccountUserBean accbean = this.getSession().get(AccountUserBean.class, bean.getDbAAccount());
		accbean.setDbAMobile(bean.getDbAMobile());
		accbean.setDbAName(bean.getDbAName());
		accbean.setDbAPassword(bean.getDbAPassword());
		accbean.setDbAPickupDetail(bean.getDbAPickupDetail());
		accbean.setDbAPickpCity(bean.getDbAPickpCity());
		accbean.setDbAPickupRegion(bean.getDbAPickupRegion());
		return accbean;
	}

	@Override
	public AccountUserBean selectAcc(String acc) {
		String hql = "FROM AccountUserBean WHERE dbAAccount=:acc";
		@SuppressWarnings("unchecked")
		TypedQuery<AccountUserBean> query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("acc", acc);
		try {
			AccountUserBean result = query.getSingleResult();
			return result;
		} catch (NoResultException e) {
			return null;
		}

	}

	@Override
	public AccountUserBean updateMemInfo(String acc, String psw, String name, String detail, CityBean dbAPickpCity,
			RegionBean dbAPickupRegion, String tel, StatusBean dbAStatus) {
		AccountUserBean temp = this.getSession().get(AccountUserBean.class, acc);
		temp.setDbAPassword(psw);
		temp.setDbAName(name);
		temp.setDbAMobile(tel);
		temp.setDbAPickpCity(dbAPickpCity);
		temp.setDbAPickupDetail(detail);
		temp.setDbAPickupRegion(dbAPickupRegion);
		temp.setDbAStatus(dbAStatus);
		return temp;
	}

	@Override
	public AccountUserBean memVerifiy(String acc, Integer verification, Long verificationtime) {
		AccountUserBean temp = this.getSession().get(AccountUserBean.class, acc);
		temp.setVerification(verification);
		temp.setVerificationTime(verificationtime);
		return temp;
	}

	@Override
	public AccountUserBean memVerifiySuccess(String acc, StatusBean status) {
		AccountUserBean temp = this.getSession().get(AccountUserBean.class, acc);
		temp.setDbAStatus(status);
		return temp;
	}

	@Override
	public String selectStatus(String acc) {
		System.out.println(this.getSession().get(AccountUserBean.class, acc).getDbAStatus().getDbSName());
		return this.getSession().get(AccountUserBean.class, acc).getDbAStatus().getDbSName();
	}

	@Override
	public AccountUserBean changePsw(String acc, String psw) {
		AccountUserBean temp = this.getSession().get(AccountUserBean.class, acc);
		temp.setDbAPassword(psw);
		return temp;
	}

	@Override
	public void updatePickUpDetail(AccountUserBean ac) {	
		System.out.println("AccountUser準備update...");
		AccountUserBean temp = this.getSession().get(AccountUserBean.class, ac.getDbAAccount());
		temp.setDbAName(ac.getDbAName());
		System.out.println("密碼"+temp.getDbAPassword());
		temp.setDbAPassword(temp.getDbAPassword());
		temp.setDbAMobile(ac.getDbAMobile());
		temp.setDbAPickpCity(new CityBean(ac.getDbAPickpCity().getDbCid()));
		temp.setDbAPickupRegion(new RegionBean(ac.getDbAPickupRegion().getDbRid()));
		temp.setDbAPickupDetail(ac.getDbAPickupDetail());
	}

}
