package model.AccountUser;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import model.AccountUserBean;
import model.CityBean;
import model.RegionBean;
import model.StatusBean;

@Service
public class AccountUser_Service {

	@Autowired
	private I_AccountUserDAO accountUserDAO;

	public boolean userExists(String dbAAccount) {
		 return accountUserDAO.userExists(dbAAccount);
	}

	public void Register(AccountUserBean ac) {
		accountUserDAO.Register(ac);
	}

	public AccountUserBean queryUser(String dbAAccount) {
		return accountUserDAO.queryUser(dbAAccount);
	}

	public AccountUserBean checkPassword(String dbAAccount, String dbAPassword) {
		return accountUserDAO.checkPassword(dbAAccount, dbAPassword);
	}

	public AccountUserBean updateUserDetails(AccountUserBean userBean) {
		return accountUserDAO.updateUserDetails(userBean);
	}
	
	public AccountUserBean selectAcc(String acc) {
		return accountUserDAO.selectAcc(acc);
	}
	
	public AccountUserBean updateMem(String acc,String psw,String name,String detail,
			CityBean dbAPickpCity, RegionBean dbAPickupRegion, String tel,StatusBean dbAStatus) {
		return accountUserDAO.updateMemInfo(acc, psw, name, detail,dbAPickpCity,dbAPickupRegion,tel,dbAStatus);
	}

	public AccountUserBean memVerifiy(String acc,Integer verification,Long verificationtime) {
		return accountUserDAO.memVerifiy(acc, verification, verificationtime);
	}
	
	public AccountUserBean memVerifiySuccess(String acc,StatusBean status) {
		return accountUserDAO.memVerifiySuccess(acc, status);
	}
	
	public String selectStatus(String acc) {
		return accountUserDAO.selectStatus(acc);
	}
	
	public AccountUserBean changePsw(String acc,String psw) {
		return accountUserDAO.changePsw(acc, psw);
	}
	//更新使用者取貨資訊  by Eric
	public void updatePickUpDetail(AccountUserBean ac) {
		accountUserDAO.updatePickUpDetail(ac);
	}


}
