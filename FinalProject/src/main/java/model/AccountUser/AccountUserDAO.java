package model.AccountUser;

import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import model.AccountUserBean;
@Repository
public class AccountUserDAO implements AccountUserIDAO {
	
	@Autowired
	private SessionFactory sessionFactory;
	

	@Override
	public void delete(AccountUserBean aub) {
		sessionFactory.getCurrentSession().delete(sessionFactory.getCurrentSession().get(AccountUserBean.class, aub.getDbAAccount()));
	}

	@Override
	public void insert(AccountUserBean aub) {
		sessionFactory.getCurrentSession().save(aub);
		System.out.println("33333333333");
	}
	
	@Override
	public void update(AccountUserBean aub) {
		AccountUserBean temp = sessionFactory.getCurrentSession().get(AccountUserBean.class, aub.getDbAAccount());
		temp.setDbAStatus(aub.getDbAStatus());
	}
	
	@Override
	public AccountUserBean selectUpdate(AccountUserBean aub) {
		return sessionFactory.getCurrentSession().get(AccountUserBean.class, aub.getDbAAccount());
	}
	//搜尋全部資料
	@SuppressWarnings("unchecked")
	@Override
	public List<AccountUserBean> selectAll() {
		
		String HQL = "From AccountUserBean";
		return sessionFactory.getCurrentSession().createQuery(HQL).getResultList();
	}
	@SuppressWarnings("unchecked")
	@Override
	//查詢全部(用會員E-MAIL由大至小排序)
	public List<AccountUserBean> selectAllByEmailDesc() {
		String HQL = "From AccountUserBean Order By dbAAccount DESC";
		TypedQuery<AccountUserBean> query = sessionFactory.getCurrentSession().createQuery(HQL);
		List<AccountUserBean> list = query.getResultList();
		System.out.println(list);
		System.out.println("11111111");
		return list;
//		return sessionfactory.getCurrentSession().createQuery(HQL).getResultList();
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	//查詢全部(用會員E-MAIL由小至大排序)
	public List<AccountUserBean> selectAllByEmailAsc() {
		String HQL = "From AccountUserBean Order By dbAAccount ASC";
		TypedQuery<AccountUserBean> query = sessionFactory.getCurrentSession().createQuery(HQL);
		List<AccountUserBean> list = query.getResultList();
		System.out.println(list);
		System.out.println("2222222");
		return list;
//		return sessionfactory.getCurrentSession().createQuery(HQL).getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	//查詢全部(用會員E-MAIL由大至小排序)
	public List<AccountUserBean> selectKeyupEmailDesc2(String searchKeyword) {
		String HQL = "From AccountUserBean where dbAAccount like :key Order By dbAAccount DESC";
		TypedQuery<AccountUserBean> query = sessionFactory.getCurrentSession().createQuery(HQL);
		query.setParameter("key", searchKeyword+"%");
		List<AccountUserBean> list = query.getResultList();
		System.out.println(list);
		System.out.println("11111111");
		return list;
//		return sessionfactory.getCurrentSession().createQuery(HQL).getResultList();
	}
	@SuppressWarnings("unchecked")
	@Override
	//查詢全部(用會員E-MAIL由小至大排序)
	public List<AccountUserBean> selectKeyupEmailAsc2(String searchKeyword) {
		System.out.println(searchKeyword+"111000");
		String HQL = "From AccountUserBean where dbAAccount like :key Order By dbAAccount ASC";
		TypedQuery<AccountUserBean> query = sessionFactory.getCurrentSession().createQuery(HQL);
		query.setParameter("key", searchKeyword+"%");
		List<AccountUserBean> list = query.getResultList();
		System.out.println("2222222");
		return list;
//		return sessionfactory.getCurrentSession().createQuery(HQL).getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	//查詢所鍵入的會員E-MAIL
	public List<AccountUserBean> selectSomeoneEmail(AccountUserBean aub) {
		String HQL = "From AccountUserBean where dbAAccount = :email";
		Session session = sessionFactory.getCurrentSession();
		TypedQuery<AccountUserBean> query = session.createQuery(HQL);
		query.setParameter("email", aub.getDbAAccount());
		List<AccountUserBean> list = query.getResultList();	
		return list;
	}

	@Override
	public AccountUserBean findUserInfo(String dbAAccount) {
		String HQL = "From AccountUserBean where dbAAccount = :dbAAccount";
		return (AccountUserBean) sessionFactory.getCurrentSession().createQuery(HQL).setParameter("dbAAccount", dbAAccount).getSingleResult(); 
	}

}
