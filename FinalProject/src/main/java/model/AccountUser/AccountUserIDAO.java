package model.AccountUser;

import java.util.List;

import model.AccountUserBean;

public interface AccountUserIDAO {

	public List<AccountUserBean> selectAll();
	public List<AccountUserBean> selectAllByEmailDesc();
	public List<AccountUserBean> selectAllByEmailAsc();
	public List<AccountUserBean> selectSomeoneEmail(AccountUserBean aub);
	public void delete(AccountUserBean aub);
	public void insert(AccountUserBean aub);
	public void update(AccountUserBean aub);
	public AccountUserBean selectUpdate(AccountUserBean aub);
	public List<AccountUserBean> selectKeyupEmailDesc2(String searchKeyword);
	public List<AccountUserBean> selectKeyupEmailAsc2(String searchKeyword);
	public AccountUserBean findUserInfo(String dbAAccount);
}
