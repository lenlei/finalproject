package model.AccountUser;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import model.AccountUserBean;

@Service
public class AccountUserService {
	
	@Autowired
	private AccountUserIDAO AUDAO;
	
	public void insert(AccountUserBean aub) {
		AUDAO.insert(aub);
		System.out.println("2222222222");
	}
	
	public void update(AccountUserBean aub) {
		AUDAO.update(aub);
	}
	
	public void delete(AccountUserBean aub) {
		AUDAO.delete(aub);
	}
	
	public List<AccountUserBean> selectAll(){
		return AUDAO.selectAll();
	}

	public List<AccountUserBean> selectAllByEmailDesc(){
		return AUDAO.selectAllByEmailDesc();
	}
	
	public List<AccountUserBean> selectAllByEmailAsc(){
		return AUDAO.selectAllByEmailAsc();
	}
	
	public List<AccountUserBean> selectSomeoneEmail(AccountUserBean aub){
		return AUDAO.selectSomeoneEmail(aub);
	}
	
	public AccountUserBean selectUpdate(AccountUserBean aub) {
		return AUDAO.selectUpdate(aub);
	}
	public List<AccountUserBean> selectKeyupEmailDesc2(String searchKeyword){
		return AUDAO.selectKeyupEmailDesc2(searchKeyword);
	}
	public List<AccountUserBean> selectKeyupEmailAsc2(String searchKeyword){
		System.out.println(searchKeyword+"111");
		return AUDAO.selectKeyupEmailAsc2(searchKeyword);
	}
	public AccountUserBean findUserInfo(String dbAAccount) {
		return AUDAO.findUserInfo(dbAAccount);
	}
}
