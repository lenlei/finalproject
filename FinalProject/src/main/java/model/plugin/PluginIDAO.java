package model.plugin;

import java.util.List;

import model.PluginBean;

public interface PluginIDAO {
	public List<PluginBean> selectPlugin(PluginBean pb);
	public List<PluginBean> selectPlugin2();
	public void delete(PluginBean pb);
	public void insert(PluginBean pb);
	public void update(PluginBean pb);
}
