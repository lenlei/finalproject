package model.plugin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import model.PluginBean;

@Service
public class PluginService {

	@Autowired
	private PluginIDAO PDAO;
	
	public List<PluginBean> selectPlugin(PluginBean pb){
		return PDAO.selectPlugin(pb);
	}
	public List<PluginBean> selectPlugin2(){
		return PDAO.selectPlugin2();
	}
	public void delete(PluginBean pb) {
		PDAO.delete(pb);
	}
	public void insert(PluginBean pb) {
		PDAO.insert(pb);
	}
	public void update(PluginBean pb) {
		PDAO.update(pb);
	}
}
