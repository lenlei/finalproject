package model.plugin;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import model.PluginBean;
import model.StatusBean;
@Repository
public class PluginDAO implements PluginIDAO {
	@Autowired
	private SessionFactory sessionFactory;
	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PluginBean> selectPlugin(PluginBean pb) {
		String HQL = "from PluginBean where dbPIName = :PluginName and dbPIStatus =2";
		return getSession().createQuery(HQL).setParameter("PluginName", pb.getDbPIName()).getResultList();
	}

	@Override
	public List<PluginBean> selectPlugin2() {
		String HQL = "From PluginBean";
		@SuppressWarnings("unchecked")
		List<PluginBean> pb=sessionFactory.getCurrentSession().createQuery(HQL).getResultList();
		return pb;
	}
	@Override
	public void delete(PluginBean pb) {
		pb.setDbPIStatus(new StatusBean());
		sessionFactory.getCurrentSession().delete(sessionFactory.getCurrentSession().get(PluginBean.class,pb.getDbPIId()));
	}
	@Override
	public void insert(PluginBean pb) {
		sessionFactory.getCurrentSession().save(pb);
	}
	@Override
	public void update(PluginBean pb) {
		PluginBean temp = sessionFactory.getCurrentSession().get(PluginBean.class, pb.getDbPIId());
		if(!"".equals(pb.getDbPIImage())) {
			temp.setDbPIImage(pb.getDbPIImage());			
		}
		temp.setDbPIName(pb.getDbPIName());

		temp.setDbPIStatus(new StatusBean(pb.getDbPIStatus().getDbSId(),null));
	}
	
}
