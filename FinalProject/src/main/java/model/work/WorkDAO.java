package model.work;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import model.WorkBean;
@Repository
public class WorkDAO implements WorkIDAO {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void update(WorkBean wb) {
		WorkBean temp = sessionFactory.getCurrentSession().get(WorkBean.class, wb.getDbWIntroduction());
		temp.setDbWStatus(wb.getDbWStatus());
	}

	@Override
	public void delete(WorkBean wb) {
		sessionFactory.getCurrentSession().delete(sessionFactory.getCurrentSession().get(WorkBean.class, wb.getDbWIntroduction()));
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<WorkBean> selectAll() {
		String HQL = "From WorkBean";
		List<WorkBean> wb = sessionFactory.getCurrentSession().createQuery(HQL).getResultList();
		return wb;
	}

}