package model.work;

import java.util.List;

import model.WorkBean;

public interface WorkIDAO {
	public void update(WorkBean wb);
	public void delete(WorkBean wb);
	public List<WorkBean> selectAll();
}
