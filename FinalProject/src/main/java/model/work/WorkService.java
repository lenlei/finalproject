package model.work;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import model.WorkBean;

@Service
public class WorkService {
	@Autowired
	private WorkIDAO WDAO;
	
	public void update(WorkBean wb) {
		WDAO.update(wb);
	}
	
	public void delete(WorkBean wb) {
		WDAO.delete(wb);
	}
	
	public List<WorkBean> selectAll(){
		return WDAO.selectAll();
	}
}
