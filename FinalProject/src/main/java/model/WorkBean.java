package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
@Entity
@Table(name="Work")
public class WorkBean {
	@Id
	@Column(columnDefinition="nvarchar(100)",nullable=false)
	private String dbWIntroduction ="";
	@Column(columnDefinition="nvarchar(max)")
	private String dbWImage;
	@ManyToOne
	@JoinColumn(name="dbWStatus",nullable=false)
	private StatusBean dbWStatus;
	@Column(columnDefinition = "varchar(40)")
	private String  dbWAccount;
	
	public WorkBean() {}
	
	public WorkBean(int pk, String dbWImage, String dbWIntroduction, StatusBean dbWStatus) {
		this.dbWImage = dbWImage;
		this.dbWIntroduction = dbWIntroduction;
		this.dbWStatus = dbWStatus;
	}
	public WorkBean(String dbWIntroduction, String dbWImage, StatusBean dbWStatus, String dbWAccount) {
		super();
		this.dbWIntroduction = dbWIntroduction;
		this.dbWImage = dbWImage;
		this.dbWStatus = dbWStatus;
		this.dbWAccount = dbWAccount;
	}
	
	public String getDbWImage() {return dbWImage;}
	public void setDbWImage(String dbWImage) {this.dbWImage = dbWImage;}
	
	public String getDbWIntroduction() {return dbWIntroduction;}
	public void setDbWIntroduction(String dbWIntroduction) {this.dbWIntroduction = dbWIntroduction;}
	
	public StatusBean getDbWStatus() {return dbWStatus;}
	public void setDbWStatus(StatusBean dbWStatus) {this.dbWStatus = dbWStatus;}

	public String getDbWAccount() {
		return dbWAccount;
	}

	public void setDbWAccount(String dbWAccount) {
		this.dbWAccount = dbWAccount;
	}

	
}


