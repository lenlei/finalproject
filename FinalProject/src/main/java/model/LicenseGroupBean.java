package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="LicenseGroup")
public class LicenseGroupBean {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer dbLGId;
	@Column(columnDefinition="nvarchar(20)")
	private String dbLGName;
	@ManyToOne
	@JoinColumn(name="dbLGStatus",nullable=false)
	private StatusBean dbLGStatus;
	@ManyToOne
	@JoinColumn(name="dbLGSetBanner",nullable=false)
	private StatusBean dbLGSetBanner;
	@ManyToOne
	@JoinColumn(name="dbLGSetProd",nullable=false)
	private StatusBean dbLGSetProd;
	@ManyToOne
	@JoinColumn(name="dbLGSetPopProd",nullable=false)
	private StatusBean dbLGSetPopProd;
	@ManyToOne
	@JoinColumn(name="dbLGSetShare",nullable=false)
	private StatusBean dbLGSetShare;
	@ManyToOne
	@JoinColumn(name="dbLGSetComProfile",nullable=false)
	private StatusBean dbLGSetComProfile;
	@ManyToOne
	@JoinColumn(name="dbLGSetOrder",nullable=false)
	private StatusBean dbLGSetOrder;
	@ManyToOne
	@JoinColumn(name="dbLGSetCust",nullable=false)
	private StatusBean dbLGSetCust;
	@ManyToOne
	@JoinColumn(name="dbLGSetSEO",nullable=false)
	private StatusBean dbLGSetSEO;
	@ManyToOne
	@JoinColumn(name="dbLGSetLic",nullable=false)
	private StatusBean dbLGSetLic;
	
	public LicenseGroupBean() {}
	public LicenseGroupBean(Integer dbLGId) {
		super();
		this.dbLGId = dbLGId;
	}
	public LicenseGroupBean(Integer dbLGId, String dbLGName, StatusBean dbLGStatus, StatusBean dbLGSetBanner,
			StatusBean dbLGSetProd, StatusBean dbLGSetPopProd, StatusBean dbLGSetShare, StatusBean dbLGSetComProfile,
			StatusBean dbLGSetOrder, StatusBean dbLGSetCust, StatusBean dbLGSetSEO, StatusBean dbLGSetLic) {
		super();
		this.dbLGId = dbLGId;
		this.dbLGName = dbLGName;
		this.dbLGStatus = dbLGStatus;
		this.dbLGSetBanner = dbLGSetBanner;
		this.dbLGSetProd = dbLGSetProd;
		this.dbLGSetPopProd = dbLGSetPopProd;
		this.dbLGSetShare = dbLGSetShare;
		this.dbLGSetComProfile = dbLGSetComProfile;
		this.dbLGSetOrder = dbLGSetOrder;
		this.dbLGSetCust = dbLGSetCust;
		this.dbLGSetSEO = dbLGSetSEO;
		this.dbLGSetLic = dbLGSetLic;
	}

	public Integer getDbLGId() {return dbLGId;}
	public void setDbLGId(Integer dbLGId) {this.dbLGId = dbLGId;}

	public String getDbLGName() {return dbLGName;}
	public void setDbLGName(String dbLGName) {this.dbLGName = dbLGName;}

	public StatusBean getDbLGStatus() {return dbLGStatus;}
	public void setDbLGStatus(StatusBean dbLGStatus) {this.dbLGStatus = dbLGStatus;}

	public StatusBean getDbLGSetBanner() {return dbLGSetBanner;}
	public void setDbLGSetBanner(StatusBean dbLGSetBanner) {this.dbLGSetBanner = dbLGSetBanner;}

	public StatusBean getDbLGSetProd() {return dbLGSetProd;}
	public void setDbLGSetProd(StatusBean dbLGSetProd) {this.dbLGSetProd = dbLGSetProd;}

	public StatusBean getDbLGSetPopProd() {return dbLGSetPopProd;}
	public void setDbLGSetPopProd(StatusBean dbLGSetPopProd) {this.dbLGSetPopProd = dbLGSetPopProd;}

	public StatusBean getDbLGSetShare() {return dbLGSetShare;}
	public void setDbLGSetShare(StatusBean dbLGSetShare) {this.dbLGSetShare = dbLGSetShare;}

	public StatusBean getDbLGSetComProfile() {return dbLGSetComProfile;}
	public void setDbLGSetComProfile(StatusBean dbLGSetComProfile) {this.dbLGSetComProfile = dbLGSetComProfile;}

	public StatusBean getDbLGSetOrder() {return dbLGSetOrder;}
	public void setDbLGSetOrder(StatusBean dbLGSetOrder) {this.dbLGSetOrder = dbLGSetOrder;}
	
	public StatusBean getDbLGSetCust() {return dbLGSetCust;}
	public void setDbLGSetCust(StatusBean dbLGSetCust) {this.dbLGSetCust = dbLGSetCust;}
	
	public StatusBean getDbLGSetSEO() {return dbLGSetSEO;}
	public void setDbLGSetSEO(StatusBean dbLGSetSEO) {this.dbLGSetSEO = dbLGSetSEO;}

	public StatusBean getDbLGSetLic() {return dbLGSetLic;}
	public void setDbLGSetLic(StatusBean dbLGSetLic) {this.dbLGSetLic = dbLGSetLic;};
	

	
}

