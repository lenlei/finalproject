package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
@Entity
@Table(name="CompanySite")
public class CompanySiteMapBean {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer pk;
	@Column(columnDefinition="nvarchar(300)")
	private String dbISitemap = "";
	@ManyToOne
	@JoinColumn(name="dbIStatus",nullable=false)
	private StatusBean dbIStatus;
	
	public CompanySiteMapBean() {
		super();
	}
	
	public CompanySiteMapBean(Integer pk, String dbISitemap, StatusBean dbIStatus) {
		super();
		this.pk = pk;
		this.dbISitemap = dbISitemap;
		this.dbIStatus = dbIStatus;
	}
	
	public Integer getPk() {return pk;}	
	public void setPk(Integer pk) {this.pk = pk;}
	
	public String getDbIISitemap() {return dbISitemap;}
	public void setDbIISitemap(String dbISitemap) {this.dbISitemap = dbISitemap;}
	
	public StatusBean getDbIStatus() {return dbIStatus;}
	public void setDbIStatus(StatusBean dbIStatus) {this.dbIStatus = dbIStatus;}
	
}
