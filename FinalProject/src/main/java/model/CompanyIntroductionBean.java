package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="CompanyIntroduction")
public class CompanyIntroductionBean {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer pk;
	@Column(columnDefinition="nvarchar(300)",nullable=false)
	private String dbIIntroduction ="";
	@ManyToOne
	@JoinColumn(name="dbIStatus",nullable=false)
	private StatusBean dbIStatus;
	
	public CompanyIntroductionBean() {
		super();
	}
	public CompanyIntroductionBean(Integer pk, String dbIIntroduction, StatusBean dbIStatus) {
		super();
		this.pk = pk;
		this.dbIIntroduction = dbIIntroduction;
		this.dbIStatus = dbIStatus;
	}
	
	public Integer getPk() {return pk;}
	public void setPk(Integer pk) {this.pk = pk;}
	
	public String getDbIIntroduction() {return dbIIntroduction;}
	public void setDbIIntroduction(String dbIIntroduction) {this.dbIIntroduction = dbIIntroduction;}
	
	public StatusBean getDbIStatus() {return dbIStatus;}
	public void setDbIStatus(StatusBean dbIStatus) {this.dbIStatus = dbIStatus;}
	
}
