package model.product;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import model.ProductBean;
import model.ProductNameBean;
@Repository
public class ProductDAO implements ProductIDAO {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void insertProduct(ProductBean pb) {
		sessionFactory.getCurrentSession().save(pb);
	}
	
	@Override
	public void insertProductName(ProductNameBean pnb) {
		sessionFactory.getCurrentSession().save(pnb);
	}

	@Override
	public void update(ProductBean pb) {
		sessionFactory.getCurrentSession().update(pb);
		ProductBean temp = sessionFactory.getCurrentSession().get(ProductBean.class, pb.getDbPId());
		if(pb.getDbPImage()!="") {
			temp.setDbPImage(pb.getDbPImage());
		}
		if(pb.getDbPTypeImage()!="") {
			temp.setDbPTypeImage(pb.getDbPTypeImage());
		}
		temp.setDbPIntroduction(pb.getDbPIntroduction());
		temp.setDbPUnitPrice(pb.getDbPUnitPrice());
		temp.setDbProductName(pb.getDbProductName());
		temp.setDbPStatus(pb.getDbPStatus());
//		BannerBean temp = sessionfactory.getCurrentSession().get(BannerBean.class, bb.getDbBId());
//		if(!"".equals(bb.getDbBPicture())) {
//			temp.setDbBPicture(bb.getDbBPicture());			
//		}
//		temp.setDbBHyperLink(bb.getDbBHyperLink());
//		temp.setDbBIntroduction(bb.getDbBIntroduction());
//		temp.setDbBStatus(new StatusBean(bb.getDbBStatus().getDbSId(),bb.getDbBStatus().getDbSName()));
	}

	@Override
	public ProductBean selectUpdate(ProductBean pb) {
		return sessionFactory.getCurrentSession().get(ProductBean.class, pb.getDbPId());
	}
	
	@Override
	public void delete(ProductBean pb) {
		//方法1
//		pb.setDbProductName(new ProductNameBean(1, null));
//		pb.setDbPStatus(new StatusBean());
//		sessionFactory.getCurrentSession().delete(pb);
		//方法2
//		sessionFactory.getCurrentSession().delete(sessionFactory.getCurrentSession().get(ProductBean.class, pb.getDbPId()));
		//方法3
//		String HQL = "delete ProductBean where dbPId = :id";
//		Query query = sessionFactory.getCurrentSession().createQuery(HQL);
//		query.setParameter("id", pb.getDbPId());
//		query.executeUpdate();
		sessionFactory.getCurrentSession().delete(sessionFactory.getCurrentSession().get(ProductBean.class,pb.getDbPId()));
				
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ProductBean> selectAll() {
		String HQL = "From ProductBean where dbPName = 1 and dbPStatus=2";
		return  sessionFactory.getCurrentSession().createQuery(HQL).getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<ProductBean> selectAll_status2(){
		String HQL = "FROM ProductBean WHERE dbPStatus=2";
		return sessionFactory.getCurrentSession().createQuery(HQL).getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ProductNameBean> selectAllPName() {
		String HQL = "From ProductNameBean";
		return sessionFactory.getCurrentSession().createQuery(HQL).getResultList();
	}
	
	@Override
	public ProductBean selectByPId(Integer dbPId) {
		ProductBean pb = sessionFactory.getCurrentSession().get(ProductBean.class, dbPId);
		return pb;
	}

	public ProductBean selectOneProduct(Integer dbPId) {
		String HQL = "From ProductBean where dbPId = :pId and dbPStatus = 2";
		return (ProductBean) sessionFactory.getCurrentSession().createQuery(HQL).setParameter("pId", dbPId).getSingleResult(); 

	}

	@Override
	public List<ProductBean> selectAll_status2Film() {
		String HQL = "FROM ProductBean WHERE dbPStatus=2 AND dbPName=2";
		return sessionFactory.getCurrentSession().createQuery(HQL).getResultList();
	}

}