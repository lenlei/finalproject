package model.product;

import java.util.List;

import model.ProductBean;
import model.ProductNameBean;


public interface ProductIDAO {
	public void insertProduct(ProductBean pb);
	public void insertProductName(ProductNameBean pnb);
	public void update(ProductBean pb);
	public void delete(ProductBean pb);
	public ProductBean selectUpdate(ProductBean pb);
	public List<ProductBean> selectAll();
	public List<ProductNameBean> selectAllPName();
	ProductBean selectByPId(Integer dbPId);
	public List<ProductBean> selectAll_status2();
	public ProductBean selectOneProduct(Integer dbPId);
	public List<ProductBean> selectAll_status2Film();
	
}