package model.product;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import model.ProductBean;
import model.ProductNameBean;

@Service
public class ProductService {
	@Autowired
	private ProductIDAO PDAO;
	
	public void insertProduct(ProductBean pb) {
		PDAO.insertProduct(pb);
	}
	public void insertProductName(ProductNameBean pnb) {
		PDAO.insertProductName(pnb);
	}
	public void update(ProductBean pb) {
		PDAO.update(pb);
	}
	public void delete(ProductBean pb) {
		PDAO.delete(pb);
	}
	public List<ProductBean> selectAll(){
		return PDAO.selectAll();
	}
	public ProductBean selectOneProduct(Integer dbPId) {
		ProductBean pb = PDAO.selectOneProduct(dbPId);
		return pb;

	}
	public List<ProductNameBean> selectAllPName(){
		return PDAO.selectAllPName();
	}
	public ProductBean selectUpdate(ProductBean pb) {
		return PDAO.selectUpdate(pb);
	}
	
	public ProductBean selectByPId(Integer dbPId) {
		return PDAO.selectByPId(dbPId);
	}
	
	public List<ProductBean> selectAll_status2(){
		return PDAO.selectAll_status2();
	}
	
	public List<ProductBean> selectAll_status2Film() {
		return PDAO.selectAll_status2Film();
	}

}