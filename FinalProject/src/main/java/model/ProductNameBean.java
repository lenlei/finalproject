package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ProductName")
public class ProductNameBean {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer dbPNId;
	@Column(columnDefinition="nvarchar(20)")
	private String dbPNName;
	
	public ProductNameBean() {
		super();
	}

	public ProductNameBean(Integer dbPNId, String dbPNName) {
		super();
		this.dbPNId = dbPNId;
		this.dbPNName = dbPNName;
	}

	public Integer getDbPNId() {return dbPNId;}
	public void setDbPNId(Integer dbPNId) {this.dbPNId = dbPNId;}

	public String getDbPNName() {return dbPNName;}
	public void setDbPNName(String dbPNName) {this.dbPNName = dbPNName;}
	
	
	
}
