package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="Product")
public class ProductBean {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer dbPId;
	@Column(columnDefinition="nvarchar(20)",nullable=false)
	private String dbPType = "";
	@Column(columnDefinition="nvarchar(max)")
	private String dbPTypeImage = "";
	@Column(columnDefinition="nvarchar(300)",nullable=false)
	private String dbPIntroduction = "";
	@Column(columnDefinition="nvarchar(max)")
	private String dbPImage = "";
	@Column(columnDefinition="int",nullable=false)
	private Integer dbPUnitPrice = 0;	
	@ManyToOne
	@JoinColumn(name="dbPStatus",nullable=false)
	private StatusBean dbPStatus;
	@ManyToOne
	@JoinColumn(name="dbPName")
	private ProductNameBean dbPName;

	
	public ProductBean() {}
	
	public ProductBean(Integer dbPId,ProductNameBean dbPName) {
		this.dbPId = dbPId;
		this.dbPName = dbPName;
	}
	
	public ProductBean(Integer dbPId, String dbPType, String dbPTypeImage, String dbPIntroduction, String dbPImage,
			Integer dbPUnitPrice, StatusBean dbPStatus, ProductNameBean dbPName) {
		super();
		this.dbPId = dbPId;
		this.dbPType = dbPType;
		this.dbPTypeImage = dbPTypeImage;
		this.dbPIntroduction = dbPIntroduction;
		this.dbPImage = dbPImage;
		this.dbPUnitPrice = dbPUnitPrice;
		this.dbPStatus = dbPStatus;
		this.dbPName = dbPName;
	}
	
	public String getDbPType() {return dbPType;}
	public void setDbPType(String dbPType) {this.dbPType = dbPType;}
	
	public String getDbPTypeImage() {return dbPTypeImage;}
	public void setDbPTypeImage(String dbPTypeImage) {this.dbPTypeImage = dbPTypeImage;}
	
	public String getDbPTIntroduction() {return dbPIntroduction;}
	public void setDbPTIntroduction(String dbPTIntroduction) {this.dbPIntroduction = dbPTIntroduction;}
	
	public String getDbPImage() {return dbPImage;}
	public void setDbPImage(String dbPImage) {this.dbPImage = dbPImage;}
	
	public Integer getDbPUnitPrice() {return dbPUnitPrice;}
	public void setDbPUnitPrice(Integer dbPUnitPrice) {this.dbPUnitPrice = dbPUnitPrice;}
	
	public StatusBean getDbPStatus() {return dbPStatus;}
	public void setDbPStatus(StatusBean dbPStatus) {this.dbPStatus = dbPStatus;}

	public Integer getDbPId() {return dbPId;}
	public void setDbPId(Integer dbPId) {this.dbPId = dbPId;}

	public String getDbPIntroduction() {return dbPIntroduction;}
	public void setDbPIntroduction(String dbPIntroduction) {this.dbPIntroduction = dbPIntroduction;}

	public ProductNameBean getDbProductName() {return dbPName;}
	public void setDbProductName(ProductNameBean dbPName) {this.dbPName = dbPName;}
	
	
}


