package model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Region")
public class RegionBean {
	@Id
	@Column(columnDefinition = "int")
	private Integer dbRid;
	@Column(columnDefinition="nvarchar(20)")
	private String dbRName;
	@Column(columnDefinition="int")
	private Integer dbCid;
	
	public RegionBean() {
		super();
	}
	
	public RegionBean(Integer dbRid) {
		super();
		this.dbRid = dbRid;
	}

	public RegionBean(Integer dbRid, String dbRName, Integer dbCid) {
		super();
		this.dbRid = dbRid;
		this.dbRName = dbRName;
		this.dbCid = dbCid;
	}
	
	public Integer getDbRid() {return dbRid;}
	public void setDbRid(Integer dbRid) {this.dbRid = dbRid;}
	
	public String getDbRName() {return dbRName;}
	public void setDbRName(String dbRName) {this.dbRName = dbRName;}
	
	public Integer getDbCid() {return dbCid;}
	public void setDbCid(Integer dbCid) {this.dbCid = dbCid;}
	
}
