package model.temp;


import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import model.StatusBean;
import model.TemporaryBean;
import model.TemporaryDetailImageBean;
import model.TemporaryDetailWordBean;
import model.TemporaryPreviewBean;
@Repository
public class TempDAO implements TempIDAO {
	@Autowired
	private SessionFactory sessionfactory;
	
	public TempDAO() {
		super();
	}

	private Session getSession() {
		return sessionfactory.getCurrentSession();
	}

	@Override
	public Serializable insertNewTemp(TemporaryBean tb) {
		return getSession().save(tb);
	}
	
	

	@SuppressWarnings("unused")
	@Override
	public void insertTempImage(List<TemporaryDetailImageBean> ltdib,Integer dbTDId,Integer page,Integer dbPId) {
		String HQL = "Delete TemporaryDetailImageBean where dbTDIId = :dbTDId and dbTDIPage = :page ORDER BY dbTDILocation";
		getSession().createQuery(HQL).setParameter("dbTDId", dbTDId).setParameter("page", page).executeUpdate();
		Iterator<TemporaryDetailImageBean> it = ltdib.iterator();
		int i = 0;
		while(it.hasNext()) {
			i++;
			getSession().save(it.next());
//			if(i%10==0) {
//				getSession().flush();
//				getSession().clear();
//			}
		} 
	}

	@Override
	public void insertTempWord(List<TemporaryDetailWordBean> ltdwb,Integer dbTDId,Integer page) {
		String HQL = "Delete TemporaryDetailWordBean where dbTDWId = :dbTDId and dbTDWPage = :page";
		getSession().createQuery(HQL).setParameter("dbTDId", dbTDId).setParameter("page", page).executeUpdate();
		Iterator<TemporaryDetailWordBean> it = ltdwb.iterator();
		int i = 0;
		while(it.hasNext()) {
			i++;
			getSession().save(it.next());
			if(i%10==0) {
				getSession().flush();
				getSession().clear();
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TemporaryDetailImageBean> selectAllImages(Integer dbTId,Integer page) {
		String HQL = "From TemporaryDetailImageBean where dbTDIId = :TDIId and dbTDIPage = :page";
		return getSession().createQuery(HQL).setParameter("TDIId", dbTId).setParameter("page", page).getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TemporaryDetailWordBean> selectAllWords(Integer dbTId,Integer page) {
		String HQL = "From TemporaryDetailWordBean where dbTDWId = :TDWId and dbTDWPage = :page";
		return getSession().createQuery(HQL).setParameter("TDWId", dbTId).setParameter("page", page).getResultList();
	}

	@Override
	public void insertTempPreview(TemporaryPreviewBean tpb) {
		String HQL = "Delete TemporaryPreviewBean where dbTPId = :dbTPId and dbTPage = :dbTPage";
		getSession().createQuery(HQL).setParameter("dbTPId", tpb.getDbTPId()).setParameter("dbTPage", tpb.getDbTPage()).executeUpdate();
		getSession().save(tpb);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TemporaryBean> selectTemporary(String account) {
		String HQL = "From TemporaryBean where dbAAccount = :Account";
		return getSession().createQuery(HQL).setParameter("Account", account).getResultList();
	}

	@Override
	public TemporaryBean insertFilm(TemporaryBean tb) {
		TemporaryBean bean=getSession().get(TemporaryBean.class, tb.getDbTId());
		bean.setDbTStatus(tb.getDbTStatus());;
		
		return bean;
	}

	public TemporaryBean selectTemp(Integer dbTId) {
		String HQL = "From TemporaryBean where dbTId = :TId";
		return (TemporaryBean) getSession().createQuery(HQL).setParameter("TId", dbTId).getSingleResult();
	}
	
	public void updateTempInfo(TemporaryBean temp) {
		TemporaryBean tp=getSession().get(TemporaryBean.class, temp.getDbTId());
		tp.setDbTTheme(temp.getDbTTheme());
		tp.setDbTIntroduction(temp.getDbTIntroduction());
	}
	
	public TemporaryBean selectOneTemp(Integer dbTPId) {
		return getSession().get(TemporaryBean.class, dbTPId);	
	}//by Eric

	@Override
	public void updateTempStatus(Integer dbTPId) {
		TemporaryBean t = getSession().get(TemporaryBean.class, dbTPId);
		t.setDbTStatus(new StatusBean((byte) 9,null));
	}//by Eric
	
}
