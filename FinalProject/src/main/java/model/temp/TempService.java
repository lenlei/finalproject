package model.temp;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.Base64;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;

import model.ProductBean;
import model.ProductNameBean;
import model.TemporaryBean;
import model.TemporaryDetailImageBean;
import model.TemporaryDetailWordBean;
import model.TemporaryPreviewBean;

@Service
public class TempService {
	@Autowired
	private TempIDAO TDAO;

	//↓承學的路徑
//	private static String UPLOADED_FOLDER = "C:\\FinalProject\\Repository\\FinalProject\\src\\main\\webapp\\Images\\";
	private static String UPLOADED_FOLDER = "C:\\Users\\user\\git\\repository\\FinalProject\\src\\main\\webapp\\Images\\";
	//↓曉彤的路徑
//	private static String UPLOADED_FOLDERFILM = "C:\\Users\\TUNG\\git\\repository\\FinalProject\\src\\main\\webapp\\Images\\";
	private static String DEFAULT_PDF="C:\\Users\\user\\git\\repository\\FinalProject\\src\\main\\webapp\\PDF\\calendarPrinter.pdf";

//	private static String UPLOADED_PDFFOLDER = "C:\\FinalProject\\Repository\\FinalProject\\src\\main\\webapp\\PDF\\";
	private static String UPLOADED_PDFFOLDER = "C:\\Users\\user\\git\\repository\\FinalProject\\src\\main\\webapp\\PDF\\";

//	private static String UPLOADED_PDFFOLDERFilm="C:\\Users\\TUNG\\git\\repository\\FinalProject\\src\\main\\webapp\\PDF\\";
	                                                 
	public TempService() {
		super();
	}

	public Serializable insertNewTemp(TemporaryBean tb) {
		return TDAO.insertNewTemp(tb);
	}

	public String insertTempImage(String Account,String Images, Integer dbTDId,Integer page,Integer dbPId) {
		Iterator<Object> it = new JSONArray(Images).iterator();
		LinkedList<TemporaryDetailImageBean> ltdib = new LinkedList<TemporaryDetailImageBean>();
		FileOutputStream fos = null;
		String base64 = null;
		String fileName = null;
		TemporaryDetailImageBean tdib = null;
		while(it.hasNext()) {
			JSONObject jo = (JSONObject) it.next();
			if(jo.getString("dbTDIImage").startsWith("data")) {
				try {
					fileName = (Account.split("@")[0] + new Date().getTime() + "."+jo.getString("dbTDIImage").split(",")[0].split(";")[0].substring(11));
					base64 = jo.getString("dbTDIImage").split(",")[1];
					fos = new FileOutputStream(UPLOADED_FOLDER+fileName);
					fos.write(Base64.getDecoder().decode(base64));
				} catch (FileNotFoundException | JSONException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
					
				tdib= new TemporaryDetailImageBean(jo.getInt("dbTDIPage"),jo.getString("dbTDILocation"),
					fileName, jo.getString("dbTDIZoom"), jo.getInt("dbTDIImageX"),jo.getInt("dbTDIImageY"),
					jo.getInt("dbTDIImageZ"),new ProductBean(1,new ProductNameBean(1,null)),dbTDId);
				ltdib.add(tdib);
			}else {
				tdib = new TemporaryDetailImageBean(jo.getInt("dbTDIPage"),jo.getString("dbTDILocation"),
				jo.getString("dbTDIImage"), jo.getString("dbTDIZoom"), jo.getInt("dbTDIImageX"), 
				jo.getInt("dbTDIImageY"), jo.getInt("dbTDIImageZ"),new ProductBean(dbPId,new ProductNameBean(1,null)), dbTDId);
				ltdib.add(tdib);
			}
		}
		TDAO.insertTempImage(ltdib,dbTDId,page,dbPId);
		return fileName;
	}
	
	public void insertTempWord(String Account,String Words, Integer dbTDIId,Integer page,Integer dbPId) {
		Iterator<Object> it = new JSONArray(Words).iterator();
		LinkedList<TemporaryDetailWordBean> ltdwb = new LinkedList<TemporaryDetailWordBean>();
		TemporaryDetailWordBean tdwb = null;
		while(it.hasNext()) {
			JSONObject jo = (JSONObject) it.next();
			tdwb = new TemporaryDetailWordBean(jo.getInt("dbTDWPage"), jo.getString("dbTDWLocation"), jo.getString("dbTDWWord"), jo.getString("dbTDWWordSize"), jo.getString("dbTDWWordColor"), jo.getString("dbTDWWordFamily"), jo.getString("dbTDWWordWeight"), new ProductBean(dbPId, new ProductNameBean(1, null)), dbTDIId);
			ltdwb.add(tdwb);
		}
		TDAO.insertTempWord(ltdwb,dbTDIId,page);
	}

	public List<TemporaryDetailImageBean> selectAllImages(Integer dbPId,Integer page) {
		return TDAO.selectAllImages(dbPId,page);
	}

	public List<TemporaryDetailWordBean> selectAllWords(Integer dbPId,Integer page) {
		return TDAO.selectAllWords(dbPId,page);
	}
	
	public String selectTemp(String Account,Integer dbTId) {
		String pageViewFilename = "";
		String calendarViewFilename = "";
		String PDFFileName = Account.split("@")[0] + new Date().getTime() + ".pdf";
		Image imagePage = null;
		Image imageCalendar = null;
		Integer i = 0;
		Integer page = 1;
		String monthData = null;
		TemporaryBean tb = TDAO.selectTemp(dbTId);
		Iterator<TemporaryPreviewBean> it = tb.getTemporaryPreview().iterator();
        try {
        	BaseFont bfChinese = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
        	Font fontBig = new Font(bfChinese, 18, Font.BOLD);
        	PdfReader reader = new PdfReader(DEFAULT_PDF);
			PdfStamper ps = new PdfStamper(reader,new FileOutputStream(UPLOADED_PDFFOLDER + PDFFileName));
			PdfContentByte under = ps.getOverContent(page);
			
		while(it.hasNext()) {
			i++;
			monthData = it.next().getDbTPageImage();
			pageViewFilename = monthData.split(",")[0];
			calendarViewFilename = monthData.split(",")[1];
			imagePage = Image.getInstance(UPLOADED_FOLDER+pageViewFilename);
			imageCalendar = Image.getInstance(UPLOADED_FOLDER+calendarViewFilename);
				switch (i) {
				case 1:
					//左上角設定日曆AbsolutePosition(25,225),畫板AbsolutePosition(25,375)
					imagePage.setAbsolutePosition((float)29,(float)619.25);
					imageCalendar.setAbsolutePosition((float)29,(float)451.5);
					imagePage.scaleAbsolute((float)239.94, (float)167.5);
					imageCalendar.scaleAbsolute((float)239.94, (float)167.5);
					under.addImage(imagePage);
					under.addImage(imageCalendar);
					continue;
				case 2:
					//右上角設定日曆AbsolutePosition(287.5,225),畫板AbsolutePosition(287.5,375)
					imagePage.setAbsolutePosition((float)326.75,(float)619.25);
					imageCalendar.setAbsolutePosition((float)326.75,(float)451.5);
					imagePage.scaleAbsolute((float)239.94, (float)167.5);
					imageCalendar.scaleAbsolute((float)239.94, (float)167.5);
					under.addImage(imagePage);
					under.addImage(imageCalendar);
					break; 
				case 3:
					//左下角設定日曆AbsolutePosition(25,25),畫板AbsolutePosition(25,175)
					imagePage.setAbsolutePosition((float)29,(float)198.5);
					imageCalendar.setAbsolutePosition((float)29,(float)30.5);
					imagePage.scaleAbsolute((float)239.94, (float)167.5);
					imageCalendar.scaleAbsolute((float)239.94, (float)167.5);
					under.addImage(imagePage);
					under.addImage(imageCalendar);
					continue;
				case 4:
					//右下角設定日曆AbsolutePosition(287.5,25),畫板AbsolutePosition(287.5,175)
					imagePage.setAbsolutePosition((float)326.75,(float)198.5);
					imageCalendar.setAbsolutePosition((float)326.75,(float)30.5); 
					imagePage.scaleAbsolute((float)239.94, (float)167.5);
					imageCalendar.scaleAbsolute((float)239.94, (float)167.5);
					under.addImage(imagePage);
					under.addImage(imageCalendar);
					
					i = 0;
					page++;
					under = ps.getOverContent(page);
					if(page==4) {
						Image imageCover = Image.getInstance(UPLOADED_FOLDER+tb.getTemporaryDetailImage().iterator().next().getDbTDIProductId().getDbPTypeImage());
						imageCover.setAbsolutePosition((float)29,(float)614.25);
						imageCover.scaleAbsolute((float)239.94, (float)167.5);
						under.addImage(imageCover);
						
//						under.beginText();
//						under.setFontAndSize(bfChinese, 16);
//						under.setTextMatrix((float)95,(float)551.5); // 设置文字在页面中的坐标
//						under.newlineShowText(tb.getDbTTheme());
//						under.endText();
						
//						under.beginText();
//						under.setFontAndSize(bfChinese, 20);
//						under.setTextMatrix((float)95,(float)501.5); // 设置文字在页面中的坐标
//						under.showText(tb.getDbTIntroduction());
//						under.endText();

					}
					continue;	
				}
				
		}
		
		ps.close();
		reader.close();
		} catch (DocumentException | IOException e) {
			e.printStackTrace();
		}

		return PDFFileName;
	}
	
	public void insertTempPreview(String Account,TemporaryPreviewBean tpb,String PageView,String CalendarView) {
		FileOutputStream fos = null;
		
		String pageViewBase64 = PageView.split(",")[1];
		String calendarViewBase64 = CalendarView.split(",")[1];
		String pageViewFilename = (Account.split("@")[0] + new Date().getTime() + "-pageView."+PageView.split(",")[0].split(";")[0].substring(11));
		String calendarViewFilename = (Account.split("@")[0] + new Date().getTime() + "-calendarView."+CalendarView.split(",")[0].split(";")[0].substring(11));

		try {
			fos = new FileOutputStream(UPLOADED_FOLDER+pageViewFilename);
			fos.write(Base64.getDecoder().decode(pageViewBase64));
			fos = new FileOutputStream(UPLOADED_FOLDER+calendarViewFilename);
			fos.write(Base64.getDecoder().decode(calendarViewBase64));
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}catch (IOException e) {
			e.printStackTrace();
		}
		tpb.setDbTPageImage(pageViewFilename+","+calendarViewFilename);
		TDAO.insertTempPreview(tpb);
	}
	
	
	public String insertTempImageFilm(String Account,String Images, Integer dbTDId,Integer page,Integer dbPId) {
		Iterator<Object> it = new JSONArray(Images).iterator();
		LinkedList<TemporaryDetailImageBean> ltdib = new LinkedList<TemporaryDetailImageBean>();
		FileOutputStream fos = null;
		String base64 = null;
		String fileName = null;
		TemporaryDetailImageBean tdib = null;
		while(it.hasNext()) {
			JSONObject jo = (JSONObject) it.next();
			if(jo.getString("dbTDIImage").startsWith("data")) {
				try {
					fileName = (Account.split("@")[0] + new Date().getTime() + "."+jo.getString("dbTDIImage").split(",")[0].split(";")[0].substring(11));
					base64 = jo.getString("dbTDIImage").split(",")[1];
					fos = new FileOutputStream(UPLOADED_FOLDER+fileName);
					fos.write(Base64.getDecoder().decode(base64));
				} catch (FileNotFoundException | JSONException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
					
				tdib= new TemporaryDetailImageBean(jo.getInt("dbTDIPage"),jo.getString("dbTDILocation"),
					fileName, jo.getString("dbTDIZoom"), jo.getInt("dbTDIImageX"),jo.getInt("dbTDIImageY"),
					jo.getInt("dbTDIImageZ"),new ProductBean(dbPId,new ProductNameBean(2,null)),dbTDId);
				ltdib.add(tdib);
			}else {
				tdib = new TemporaryDetailImageBean(jo.getInt("dbTDIPage"),jo.getString("dbTDILocation"),
				jo.getString("dbTDIImage"), jo.getString("dbTDIZoom"), jo.getInt("dbTDIImageX"), 
				jo.getInt("dbTDIImageY"), jo.getInt("dbTDIImageZ"),new ProductBean(dbPId,new ProductNameBean(2,null)), dbTDId);
				ltdib.add(tdib);
			}
		}
		TDAO.insertTempImage(ltdib,dbTDId,page,dbPId);
		System.out.println(UPLOADED_FOLDER+fileName+"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		return fileName;
	}
	
	
	
	public String insertTempPreviewFilm(String Account,TemporaryPreviewBean tpb,String CalendarView) {
		FileOutputStream fos = null;
		String calendarViewBase64 = CalendarView.split(",")[1];
		String calendarViewFilename = (Account.split("@")[0] + new Date().getTime() + "-FilmView."+CalendarView.split(",")[0].split(";")[0].substring(11));
		
		try {
			
			fos = new FileOutputStream(UPLOADED_FOLDER+calendarViewFilename);
			fos.write(Base64.getDecoder().decode(calendarViewBase64));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}catch (IOException e) {
			e.printStackTrace();
		}
		tpb.setDbTPageImage(calendarViewFilename);
		
		TDAO.insertTempPreview(tpb);
		return calendarViewFilename;
	}

	public List<TemporaryBean> selectTemporary(String account) {
		return TDAO.selectTemporary(account);

	}
	
	public TemporaryBean insertFilm(TemporaryBean tb) {
		return TDAO.insertFilm(tb);
	}
		

	
	public String selectTempFilm(String Account,Integer dbTId) {
		String pageViewFilename = "";
		Document document = new Document(PageSize.A4.rotate());
		String PDFFileName = Account.split("@")[0] + new Date().getTime() + ".pdf";
		Image imagePage = null;
		Integer page = 1;
		TemporaryBean tb = TDAO.selectTemp(dbTId);
		Iterator<TemporaryPreviewBean> it = tb.getTemporaryPreview().iterator();
        try {
			PdfWriter.getInstance(document, new FileOutputStream(UPLOADED_PDFFOLDER + PDFFileName));
			document.open();
			document.newPage();

		while(it.hasNext()) {
			pageViewFilename = it.next().getDbTPageImage();			
			imagePage = Image.getInstance(UPLOADED_FOLDER+pageViewFilename);
			imagePage.setAbsolutePosition((float)12.5, 120);

			imagePage.scalePercent(42);
			imagePage.scaleAbsoluteHeight(100);

			document.add(imagePage);
		}		
		document.close();
		} catch (DocumentException | IOException e) {
			e.printStackTrace();
		}

		return PDFFileName;
	}

	public void updateTempInfo(TemporaryBean temp) {
		TDAO.updateTempInfo(temp);
	}
	
	public TemporaryBean selectOneTemp(Integer dbTPId) {
		return TDAO.selectOneTemp(dbTPId);
	}//by Eric
	
	public void updateTempStatus(Integer dbTPId) {
		TDAO.updateTempStatus(dbTPId);
	}//by Eric
	
}
