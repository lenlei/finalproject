package model.temp;

import java.util.List;
import java.io.Serializable;

import model.TemporaryBean;
import model.TemporaryDetailImageBean;
import model.TemporaryDetailWordBean;
import model.TemporaryPreviewBean;

public interface TempIDAO {

	public void insertTempImage(List<TemporaryDetailImageBean> ltdib,Integer dbTDId,Integer page,Integer dbPId);
	public void insertTempWord(List<TemporaryDetailWordBean> ltdwb,Integer dbTDId,Integer page);
	public List<TemporaryDetailImageBean> selectAllImages(Integer dbTId,Integer page);
	public List<TemporaryDetailWordBean> selectAllWords(Integer dbTId,Integer page);
	public Serializable insertNewTemp(TemporaryBean tb);
	public void insertTempPreview(TemporaryPreviewBean tpb);
	public List<TemporaryBean> selectTemporary(String account);
	public TemporaryBean insertFilm(TemporaryBean tb);
	public TemporaryBean selectTemp(Integer dbTId);
	public void updateTempInfo(TemporaryBean temp);
	public TemporaryBean selectOneTemp(Integer dbTPId);
	public void updateTempStatus(Integer dbTPId);
}
