package model;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
@Entity
@Table(name="Introduction")
public class IntroductionSetBean {
 @Id
 private Integer pk = 1;
 @OneToOne
 @JoinColumn(name="dbISIntroduction")
 private CompanyIntroductionBean dbISIntroduction;
 @OneToOne
 @JoinColumn(name="dbISTel")
 private CompanyTelBean dbISTel;
 @OneToOne
 @JoinColumn(name="dbISEmail")
 private CompanyEmailBean dbISEmail; 
 @OneToOne
 @JoinColumn(name="dbISAddress")
 private CompanyAddressBean dbISAddress;
 @OneToOne
 @JoinColumn(name="dbISProductprocess")
 private CompanyProductProcessBean dbISProductprocess;
 @OneToOne
 @JoinColumn(name="dbISProcesspics")
 private CompanyProcessPicBean dbISProcesspics; 
 @OneToOne
 @JoinColumn(name="dbISitemap")
 private CompanySiteMapBean dbISitemap;
 
 public IntroductionSetBean() {}
 

//
//@Override
//public String toString() {
//	return "IntroductionSetBean [pk=" + pk + ", dbISIntroduction=" + dbISIntroduction + ", dbISTel=" + dbISTel
//			+ ", dbISEmail=" + dbISEmail + ", dbISAddress=" + dbISAddress + ", dbISProductprocess=" + dbISProductprocess
//			+ ", dbISProcesspics=" + dbISProcesspics + ", dbISitemap=" + dbISitemap + "]";
//}



public IntroductionSetBean(Integer pk, CompanyIntroductionBean dbISIntroduction, CompanyTelBean dbISTel,
   CompanyEmailBean dbISEmail, CompanyAddressBean dbISAddress, CompanyProductProcessBean dbISProductprocess,
   CompanyProcessPicBean dbISProcesspics, CompanySiteMapBean dbISitemap) {
  super();
  this.pk = pk;
  this.dbISIntroduction = dbISIntroduction;
  this.dbISTel = dbISTel;
  this.dbISEmail = dbISEmail;
  this.dbISAddress = dbISAddress;
  this.dbISProductprocess = dbISProductprocess;
  this.dbISProcesspics = dbISProcesspics;
  this.dbISitemap = dbISitemap;
 }


 public Integer getPk() {return pk;}
 public void setPk(Integer pk) {this.pk = pk;}


 public CompanyIntroductionBean getDbISIntroduction() {return dbISIntroduction;}
 public void setDbISIntroduction(CompanyIntroductionBean dbISIntroduction) {this.dbISIntroduction = dbISIntroduction;}


 public CompanyTelBean getDbISTel() {return dbISTel;}
 public void setDbISTel(CompanyTelBean dbISTel) {this.dbISTel = dbISTel;}


 public CompanyEmailBean getDbISEmail() {return dbISEmail;}
 public void setDbISEmail(CompanyEmailBean dbISEmail) {this.dbISEmail = dbISEmail;}


 public CompanyAddressBean getDbISAddress() {return dbISAddress;}
 public void setDbISAddress(CompanyAddressBean dbISAddress) {this.dbISAddress = dbISAddress;}


 public CompanyProductProcessBean getDbISProductprocess() {return dbISProductprocess;}
 public void setDbISProductprocess(CompanyProductProcessBean dbISProductprocess) {this.dbISProductprocess = dbISProductprocess;}


 public CompanyProcessPicBean getDbISProcesspics() {return dbISProcesspics;}
 public void setDbISProcesspics(CompanyProcessPicBean dbISProcesspics) {this.dbISProcesspics = dbISProcesspics;}


 public CompanySiteMapBean getDbISitemap() {return dbISitemap;}
 public void setDbISitemap(CompanySiteMapBean dbISitemap) {this.dbISitemap = dbISitemap;}

 
}