package model;

public class LGmodel {
private String backstageMenu;
private String backstageMenu_last;

public String getBackstageMenu() {
	return backstageMenu;
}

public String getBackstageMenu_last() {
	return backstageMenu_last;
}

public void setBackstageMenu_last(String backstageMenu_last) {
	this.backstageMenu_last = backstageMenu_last;
}

public void setBackstageMenu(String backstageMenu) {
	this.backstageMenu = backstageMenu;
}

}
