package model.licenseaccount;

import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import model.LicenseAccountBean;
import model.LicenseGroupBean;

@Repository
public class LicenseAccountDAO implements LicenseAccountIDAO {
	@Autowired
	private SessionFactory sessionFactory;
	

	public Session getSession() {
		// 透過sessionFactory取得getCurrentSession()與資料庫交易
		return this.sessionFactory.getCurrentSession();
	}
	@Override
	public void insert(LicenseAccountBean lab) {
		sessionFactory.getCurrentSession().save(lab);
	}

	@Override
	public void update(LicenseAccountBean lab) {
//		sessionFactory.getCurrentSession().update(lab);
		LicenseAccountBean temp = sessionFactory.getCurrentSession().get(LicenseAccountBean.class, lab.getDbLAAccount());
//		temp.setDbLAAccount(lab.getDbLAAccount());
		temp.setDbLAUserName(lab.getDbLAUserName());
		temp.setDbLAPassword(lab.getDbLAPassword());
		temp.setDbLAGroupDefine(lab.getDbLAGroupDefine());
		temp.setDbLAStatus(lab.getDbLAStatus());
	}

	@Override
	public void delete(LicenseAccountBean lab) {
		sessionFactory.getCurrentSession().delete(sessionFactory.getCurrentSession().get(LicenseAccountBean.class, lab.getDbLAAccount()));
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<LicenseAccountBean> selectAll() {
		String HQL = "From LicenseAccountBean";
		return sessionFactory.getCurrentSession().createQuery(HQL).getResultList();
	}
	
	@Override
	public LicenseAccountBean selectUpdate (LicenseAccountBean lab) {
		return sessionFactory.getCurrentSession().get(LicenseAccountBean.class, lab.getDbLAAccount());
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<LicenseGroupBean> selectAllLGName() {
		String HQL = "From LicenseGroupBean";
		return sessionFactory.getCurrentSession().createQuery(HQL).getResultList();
	}
		
	@Override
	public LicenseAccountBean selectLAUpdate(LicenseAccountBean lab) {
		return sessionFactory.getCurrentSession().get(LicenseAccountBean.class, lab.getDbLAAccount());
	}
	


	// 檢查管理者在登入時輸入的帳號 & 密碼是否正確。如果正確，傳回該帳號所對應的LicenseAccountBean物件，否則傳回 null。
	@Override
	public LicenseAccountBean checkPassword(LicenseAccountBean lab) {
		LicenseAccountBean userBean = null;
		String hql = "FROM LicenseAccountBean  WHERE dbLAAccount = :account and dbLAPassword = :pswd";
		try {
			userBean = (LicenseAccountBean) this.getSession().createQuery(hql).setParameter("account", lab.getDbLAAccount())
					.setParameter("pswd", lab.getDbLAPassword()).getSingleResult();
			System.out.println("--------------------"+userBean.getDbLAStatus().getDbSId());
		} catch (NoResultException e) {
			userBean = null;
		}
		
		return userBean;
	}
}
