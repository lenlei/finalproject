package model.licenseaccount;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import model.LicenseAccountBean;
import model.LicenseGroupBean;

@Service
public class LicenseAccountService {
	
	@Autowired
	private LicenseAccountIDAO LADAO;
	
	public void insert(LicenseAccountBean lab) {
		LADAO.insert(lab);
	}
	
	public void update(LicenseAccountBean lab) {
		LADAO.update(lab);
	}
	
	public void delete(LicenseAccountBean lab) {
		LADAO.delete(lab);
	}
	
	public List<LicenseAccountBean> selectAll(){
		return LADAO.selectAll();
	}
	
	public LicenseAccountBean selectUpdate(LicenseAccountBean lab) {
		return LADAO.selectUpdate(lab);
	}
	
	public List<LicenseGroupBean> selectAllLGName(){
		return LADAO.selectAllLGName();
	}
	
	public LicenseAccountBean selectLAUpdate(LicenseAccountBean lab) {
		return LADAO.selectLAUpdate(lab);
	}

	public LicenseAccountBean checkPassword(LicenseAccountBean lab) {
		return LADAO.checkPassword(lab);
	}
	
}
