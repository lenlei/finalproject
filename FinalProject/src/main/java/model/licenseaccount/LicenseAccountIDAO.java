package model.licenseaccount;

import java.util.List;

import model.LicenseAccountBean;
import model.LicenseGroupBean;

public interface LicenseAccountIDAO {

	public List<LicenseAccountBean> selectAll();
	public void delete(LicenseAccountBean lab);
	public void insert(LicenseAccountBean lab);
	public void update(LicenseAccountBean lab);
	public LicenseAccountBean selectUpdate(LicenseAccountBean lab);
	public List<LicenseGroupBean> selectAllLGName();
	public LicenseAccountBean selectLAUpdate(LicenseAccountBean lab);
	public LicenseAccountBean checkPassword(LicenseAccountBean lab);
	
}
