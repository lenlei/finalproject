package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="SEOKeyWord")
public class SEOKeyWordBean {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer pk;
	@Column(columnDefinition="nvarchar(100)",nullable=false)
	private String dbSKeyWord = "";
	@ManyToOne
	@JoinColumn(name="dbSStatus",nullable=false)
	private StatusBean dbSStatus;
	
	public SEOKeyWordBean() {
		super();
	}

	public SEOKeyWordBean(Integer pk, String dbSKeyWord, StatusBean dbSStatus) {
		super();
		this.pk = pk;
		this.dbSKeyWord = dbSKeyWord;
		this.dbSStatus = dbSStatus;
	}
	
	public Integer getPk() {return pk;}
	public void setPk(Integer pk) {this.pk = pk;}
	
	public String getDbSKeyWord() {return dbSKeyWord;}
	public void setDbSKeyWord(String dbSKeyWord) {this.dbSKeyWord = dbSKeyWord;}
	
	public StatusBean getDbSStatus() {return dbSStatus;}
	public void setDbSStatus(StatusBean dbSStatus) {this.dbSStatus = dbSStatus;}
	
	
}
