package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
@Entity
@Table(name="CompanyEmail")
public class CompanyEmailBean {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer pk;
	@Column(columnDefinition="varchar(40)")
	private String dbIEmail;
	@ManyToOne
	@JoinColumn(name="dbIStatus",nullable=false)	
    private StatusBean dbIStatus;
	public CompanyEmailBean() {
		super();
	}
	
	public CompanyEmailBean(Integer pk, String dbIEmail, StatusBean dbIStatus) {
		super();
		this.pk = pk;
		this.dbIEmail = dbIEmail;
		this.dbIStatus = dbIStatus;
	}
	
	public Integer getPk() {return pk;}
	public void setPk(Integer pk) {this.pk = pk;}
	
	public String getDbIEmail() {return dbIEmail;}
	public void setDbIEmail(String dbIEmail) {this.dbIEmail = dbIEmail;}
	
	public StatusBean getDbIStatus() {return dbIStatus;}
	public void setDbIStatus(StatusBean dbIStatus) {this.dbIStatus = dbIStatus;}

}
