package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="TemporaryDetailImage")
public class TemporaryDetailImageBean {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer pk;
	@Column(columnDefinition="int")
	private Integer dbTDIPage;
	@Column(columnDefinition="nvarchar(100)")
	private String dbTDILocation;
	@Column(columnDefinition="nvarchar(max)")
	private String dbTDIImage;
	@Column(columnDefinition="nvarchar(20)")
	private String dbTDIZoom;
	@Column(columnDefinition="int")
	private Integer dbTDIImageX;
	@Column(columnDefinition="int")
	private Integer dbTDIImageY;
	@Column(columnDefinition="int")
	private Integer dbTDIImageZ;
	@ManyToOne
	@JoinColumn(name="dbTDIProductId")
	private ProductBean dbTDIProductId;
	@Column(columnDefinition="int")
	private Integer dbTDIId;
	
	public TemporaryDetailImageBean() {
		super();
	}

	public TemporaryDetailImageBean(Integer dbTDIPage, String dbTDILocation, String dbTDIImage,
			String dbTDIZoom, Integer dbTDIImageX, Integer dbTDIImageY, Integer dbTDIImageZ, ProductBean dbTDIProductId,
			Integer dbTDIId) {
		super();
		this.dbTDIPage = dbTDIPage;
		this.dbTDILocation = dbTDILocation;
		this.dbTDIImage = dbTDIImage;
		this.dbTDIZoom = dbTDIZoom;
		this.dbTDIImageX = dbTDIImageX;
		this.dbTDIImageY = dbTDIImageY;
		this.dbTDIImageZ = dbTDIImageZ;
		this.dbTDIProductId = dbTDIProductId;
		this.dbTDIId = dbTDIId;
	}

	public Integer getPk() {return pk;}
	public void setPk(Integer pk) {this.pk = pk;}
	
	public Integer getDbTDIPage() {return dbTDIPage;}
	public void setDbTDIPage(Integer dbTDIPage) {this.dbTDIPage = dbTDIPage;}

	public String getDbTDIImage() {return dbTDIImage;}
	public void setDbTDIImage(String dbTDIImage) {this.dbTDIImage = dbTDIImage;}

	public String getDbTDILocation() {return dbTDILocation;}
	public void setDbTDILocation(String dbTDILocation) {this.dbTDILocation = dbTDILocation;}

	public String getDbTDIZoom() {return dbTDIZoom;}
	public void setDbTDIZoom(String dbTDIZoom) {this.dbTDIZoom = dbTDIZoom;}

	public Integer getDbTDIImageX() {return dbTDIImageX;}
	public void setDbTDIImageX(Integer dbTDIImageX) {this.dbTDIImageX = dbTDIImageX;}

	public Integer getDbTDIImageY() {return dbTDIImageY;}
	public void setDbTDIImageY(Integer dbTDIImageY) {this.dbTDIImageY = dbTDIImageY;}

	public Integer getDbTDIImageZ() {return dbTDIImageZ;}
	public void setDbTDIImageZ(Integer dbTDIImageZ) {this.dbTDIImageZ = dbTDIImageZ;}

	public ProductBean getDbTDIProductId() {return dbTDIProductId;}
	public void setDbTDIProductId(ProductBean dbTDIProductId) {this.dbTDIProductId = dbTDIProductId;}

	public Integer getDbTDIId() {
		return dbTDIId;
	}

	public void setDbTDIId(Integer dbTDIId) {
		this.dbTDIId = dbTDIId;
	}

	

}
