package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
@Entity
@Table(name="CompanyProductProcess")
public class CompanyProductProcessBean {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer pk;
	@Column(columnDefinition="nvarchar(300)")
	private String dbIProductprocess = "";
	@ManyToOne
	@JoinColumn(name="dbIStatus",nullable=false)
	private StatusBean dbIStatus;
	
	public CompanyProductProcessBean() {
		super();
	}
	
	public CompanyProductProcessBean(Integer pk, String dbIProductprocess, StatusBean dbIStatus) {
		super();
		this.pk = pk;
		this.dbIProductprocess = dbIProductprocess;
		this.dbIStatus = dbIStatus;
	}

	public Integer getPk() {return pk;}
	public void setPk(Integer pk) {this.pk = pk;}

	public String getDbIProductprocess() {return dbIProductprocess;}
	public void setDbIProductprocess(String dbIProductprocess) {this.dbIProductprocess = dbIProductprocess;}

	public StatusBean getDbIStatus() {return dbIStatus;}
	public void setDbIStatus(StatusBean dbIStatus) {this.dbIStatus = dbIStatus;}
	
}
