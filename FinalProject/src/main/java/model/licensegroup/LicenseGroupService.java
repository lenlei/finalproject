package model.licensegroup;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import model.LicenseGroupBean;

@Service
public class LicenseGroupService {
	
	@Autowired
	private LicenseGroupIDAO LGDAO;
	
	public void insert(LicenseGroupBean lgb) {
		LGDAO.insert(lgb);
	}
	
	public void update(LicenseGroupBean lgb) {
		LGDAO.update(lgb);
	}
	
	public void delete(LicenseGroupBean lgb) {
		LGDAO.delete(lgb);
	}
	
	public List<LicenseGroupBean> selectAll(){
		return LGDAO.selectAll();
	}
	
	public void insertLicenseGroup(LicenseGroupBean lgb) {
		LGDAO.insertLicenseGroup(lgb);
	}
	
	public LicenseGroupBean selectLGUpdate(LicenseGroupBean lgb) {
		return LGDAO.selectLGUpdate(lgb);
	}
	
}
