package model.licensegroup;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import model.LicenseGroupBean;

@Repository
public class LicenseGroupDAO implements LicenseGroupIDAO {
	@Autowired
	private SessionFactory sessionFactory;
	@Override
	public void insert(LicenseGroupBean lgb) {
		sessionFactory.getCurrentSession().save(lgb);
	}

	@Override
	public void update(LicenseGroupBean lgb) {
//		sessionFactory.getCurrentSession().update(lab);
		LicenseGroupBean temp = sessionFactory.getCurrentSession().get(LicenseGroupBean.class, lgb.getDbLGId());
		temp.setDbLGName(lgb.getDbLGName());
		temp.setDbLGSetBanner(lgb.getDbLGSetBanner());
		temp.setDbLGSetComProfile(lgb.getDbLGSetComProfile());
		temp.setDbLGSetCust(lgb.getDbLGSetCust());
		temp.setDbLGSetLic(lgb.getDbLGSetLic());
		temp.setDbLGSetOrder(lgb.getDbLGSetOrder());
		temp.setDbLGSetPopProd(lgb.getDbLGSetPopProd());
		temp.setDbLGSetProd(lgb.getDbLGSetProd());
		temp.setDbLGSetSEO(lgb.getDbLGSetSEO());
		temp.setDbLGSetShare(lgb.getDbLGSetShare());
		temp.setDbLGStatus(lgb.getDbLGStatus());
	}

	@Override
	public void delete(LicenseGroupBean lgb) {
		sessionFactory.getCurrentSession().delete(sessionFactory.getCurrentSession().get(LicenseGroupBean.class, lgb.getDbLGId()));
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<LicenseGroupBean> selectAll() {
		String HQL = "From LicenseGroupBean";
		return sessionFactory.getCurrentSession().createQuery(HQL).getResultList();
	}
	
	@Override
	public void insertLicenseGroup(LicenseGroupBean lgb) {
		sessionFactory.getCurrentSession().save(lgb);
	}

	@Override
	public LicenseGroupBean selectLGUpdate(LicenseGroupBean lgb) {
		return sessionFactory.getCurrentSession().get(LicenseGroupBean.class, lgb.getDbLGId());
	}
}
