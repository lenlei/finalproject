package model.licensegroup;

import java.util.List;

import model.LicenseGroupBean;

public interface LicenseGroupIDAO {

	public List<LicenseGroupBean> selectAll();
	public void delete(LicenseGroupBean lgb);
	public void insert(LicenseGroupBean lgb);
	public void update(LicenseGroupBean lgb);
	public void insertLicenseGroup(LicenseGroupBean lgb);
	public LicenseGroupBean selectLGUpdate(LicenseGroupBean lgb);
}
