package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="OrderDetail")
public class OrderDetailBean{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int pk;
	@Column(columnDefinition="nvarchar(max)")
	private String dbODProductPicture;
	@Column(columnDefinition="int",nullable=false)
	private Integer dbODAmount = 0;
	@Column(columnDefinition="int",nullable=false)
	private Integer dbODQty = 0;
	@Column(columnDefinition="nvarchar(max)",nullable=false)
	private String dbODProductRequire = "";
	@Column(columnDefinition="nvarchar(20)",nullable=false)
	private String dbODpaymentMethod = "";//付款方式
	@Column(columnDefinition="nvarchar(20)",nullable=false)
	private String dbODtransferMethod = "";//配送方式
	@Column(name="dbODOrder",nullable=false,columnDefinition="int")
	private Integer dbODOrder;
	@ManyToOne
	@JoinColumn(name="dbPId",nullable=false)
	private ProductBean dbODProduct;

	
	public OrderDetailBean() {}

	public OrderDetailBean(int pk, String dbODProductPicture, Integer dbODAmount, Integer dbODQty,
			String dbODProductRequire, String dbODpaymentMethod, String dbODtransferMethod, Integer dbODOrder,
			ProductBean dbODProduct) {
		super();
		this.pk = pk;
		this.dbODProductPicture = dbODProductPicture;
		this.dbODAmount = dbODAmount;
		this.dbODQty = dbODQty;
		this.dbODProductRequire = dbODProductRequire;
		this.dbODpaymentMethod = dbODpaymentMethod;
		this.dbODtransferMethod = dbODtransferMethod;
		this.dbODOrder = dbODOrder;
		this.dbODProduct = dbODProduct;
	}

	public int getPk() {return pk;}
	public void setPk(int pk) {this.pk = pk;}	
	
	public String getDbODProductPicture() {return dbODProductPicture;}
	public void setDbODProductPicture(String dbODProductPicture) {this.dbODProductPicture = dbODProductPicture;}
	
	public Integer getDbODAmount() {return dbODAmount;}
	public void setDbODAmount(Integer dbODAmount) {this.dbODAmount = dbODAmount;}
	
	public Integer getDbODQty() {return dbODQty;}
	public void setDbODQty(Integer dbODQty) {this.dbODQty = dbODQty;}
	
	public String getDbODProductRequire() {return dbODProductRequire;}
	public void setDbODProductRequire(String dbODProductRequire) {this.dbODProductRequire = dbODProductRequire;}
	
	public Integer getDbODOrder() {return dbODOrder;}
	public void setDbODOrder(Integer dbODOrder) {this.dbODOrder = dbODOrder;}

	public ProductBean getDbODProduct() {return dbODProduct;}
	public void setDbODProduct(ProductBean dbODProduct) {this.dbODProduct = dbODProduct;}

	public String getDbODpaymentMethod() {return dbODpaymentMethod;}
	public void setDbODpaymentMethod(String dbODpaymentMethod) {this.dbODpaymentMethod = dbODpaymentMethod;}

	public String getDbODtransferMethod() {return dbODtransferMethod;}
	public void setDbODtransferMethod(String dbODtransferMethod) {this.dbODtransferMethod = dbODtransferMethod;}	
	
}

