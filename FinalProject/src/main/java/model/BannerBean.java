package model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
@Entity
@Table(name="Banner",uniqueConstraints= {@UniqueConstraint(columnNames= {"dbBType","dbBSort"})})
public class BannerBean {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer dbBId;
	@Column(columnDefinition="nvarchar(10)",nullable=false)
	private String dbBType = "";
	@Column(columnDefinition="int",nullable=false)
	private Integer dbBSort = 0 ;
	@Column(columnDefinition="nvarchar(max)",nullable=false)
	private String dbBHyperLink = "";
	@Column(columnDefinition="nvarchar(max)")
	private String dbBPicture;
	@Column(columnDefinition="nvarchar(50)",nullable=false)
	private String dbBIntroduction = "";
	@Column(columnDefinition="datetime",nullable=false)
	private Date dbBPictureDate = new Date();
	@ManyToOne
	@JoinColumn(name="dbStatus",nullable=false,columnDefinition="tinyint")
	private StatusBean dbBStatus;
	
	public BannerBean() {}
	
	public BannerBean(Integer dbBId) {}
	
	public BannerBean(Integer dbBSort, String dbBType, String dbBHyperLink, String dbBPicture,
			String dbBIntroduction, Date dbBPictureDate, StatusBean dbBStatus) {
		super();
		this.dbBSort = dbBSort;
		this.dbBType = dbBType;
		this.dbBHyperLink = dbBHyperLink;
		this.dbBPicture = dbBPicture;
		this.dbBIntroduction = dbBIntroduction;
		this.dbBPictureDate = dbBPictureDate;
		this.dbBStatus = dbBStatus;
	}

	public BannerBean(Integer dbBId, String dbBType, Integer dbBSort, String dbBHyperLink, String dbBPicture,
			String dbBIntroduction, Date dbBPictureDate, StatusBean dbBStatus) {
		super();
		this.dbBId = dbBId;
		this.dbBType = dbBType;
		this.dbBSort = dbBSort;
		this.dbBHyperLink = dbBHyperLink;
		this.dbBPicture = dbBPicture;
		this.dbBIntroduction = dbBIntroduction;
		this.dbBPictureDate = dbBPictureDate;
		this.dbBStatus = dbBStatus;
	}

	public Integer getDbBId() {return dbBId;}
	public void setDbBId(Integer dbBId) {this.dbBId = dbBId;}

	public String getDbBHyperLink() {return dbBHyperLink;}
	public void setDbBHyperLink(String dbBHyperLink) {this.dbBHyperLink = dbBHyperLink;}
	
	public String getDbBPicture() {return dbBPicture;}
	public void setDbBPicture(String dbBPicture) {this.dbBPicture = dbBPicture;}
	
	public String getDbBType() {return dbBType;}
	public void setDbBType(String dbBType) {this.dbBType = dbBType;}
	
	public String getDbBIntroduction() {return dbBIntroduction;}
	public void setDbBIntroduction(String dbBIntroduction) {this.dbBIntroduction = dbBIntroduction;}
	
	public Date getDbBPictureDate() {return dbBPictureDate;}
	public void setDbBPictureDate(Date dbBPictureDate) {this.dbBPictureDate = dbBPictureDate;}
	
	public Integer getDbBSort() {return dbBSort;}
	public void setDbBSort(Integer dbBSort) {this.dbBSort = dbBSort;}

	public StatusBean getDbBStatus() {return dbBStatus;}
	public void setDbBStatus(StatusBean dbBStatus) {this.dbBStatus = dbBStatus;}

}


