package model.Order;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import model.OrderBean;
import model.OrderDetailBean;
import model.ProductBean;
import model.ProductNameBean;

@Service
public class OrderService {
	
	@Autowired
	private OrderIDAO ODAO;
	
	public Serializable insert(OrderBean ob) {
		return ODAO.insert(ob);
	}
	
	public void update(OrderBean ob) {
		ODAO.update(ob);
	}
	
	public void updatetotalprice(OrderBean ob) {
		ODAO.updatetotalprice(ob);
	}
	
	public void delete(OrderBean ob) {
		ODAO.delete(ob);
	}
	
	public List<OrderBean> selectAll(){
		return ODAO.selectAll();
	}

	public List<OrderBean> selectAllByOrderIdDesc(){
		return ODAO.selectAllByOrderIdDesc();
	}
	
	public List<OrderBean> selectAllByOrderIdAsc(){
		return ODAO.selectAllByOrderIdAsc();
	}
	
	public List<OrderBean> selectStatus(OrderBean ob){
		return ODAO.selectStatus(ob);
	}
	
	public List<OrderBean> selectSomeoneEmail(OrderBean ob){
		return ODAO.selectSomeoneEmail(ob);
	}
	
	public List<OrderBean> selectSomeoneOrderId(OrderBean ob){
		return ODAO.selectSomeoneOrderId(ob);
	}
	
	public List<OrderBean> selectOne(String acc){
		return ODAO.selectOne(acc);
	}
	
	public List<OrderBean> selectOneByOid(Integer oid){
		return ODAO.selectOneByOid(oid);
	}
	
	public String opayPayment(Integer oid,Integer allPrice) {
		return ODAO.genAioCheckOutDevide(oid,allPrice);
	}
	public List<OrderBean> selectKeyupOrderDesc2(Integer searchKeyOrder){
		return ODAO.selectKeyupOrderDesc2(searchKeyOrder);	
	}
	public List<OrderBean> selectKeyupOrderAsc2(Integer searchKeyOrder) {
		return ODAO.selectKeyupOrderAsc2(searchKeyOrder);
		
	}

	
	public OrderBean selectUpdate(OrderBean ob) {
		return ODAO.selectUpdate(ob);
	}
	
	public List<OrderBean> selectAlldbOStatus(){
		return ODAO.selectAlldbOStatus();
	}
	
	public OrderDetailBean selectUpdateOrderDetail(OrderDetailBean odb) {
		return ODAO.selectUpdateOrderDetail(odb);
	}

	public List<OrderBean> selectKeyupOrderByNameDesc2(String searchKeyName) {
		return ODAO.selectKeyupOrderByNameDesc2(searchKeyName);
		
	}
	public List<OrderBean> selectKeyupOrderByNameAsc2(String searchKeyName) {
		return ODAO.selectKeyupOrderByNameAsc2(searchKeyName);	
	}
	public List<OrderBean> selectOrderByType(OrderBean ob){
		return ODAO.selectOrderByType(ob);
	}
	public List<OrderBean> selectOrderByType2(Byte OrderKey){
		return ODAO.selectOrderByType2(OrderKey);
	}
	public void updateOrderSetPage(OrderBean ob) {
		ODAO.updateOrderSetPage(ob);
	}
}
