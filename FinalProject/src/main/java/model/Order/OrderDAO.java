package model.Order;


import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import allPay.payment.integration.AllInOne;
import allPay.payment.integration.domain.AioCheckOutDevide;
import model.LicenseGroupBean;
import model.OrderBean;
import model.OrderDetailBean;
import model.ProductBean;
import model.ProductNameBean;
@Repository
public class OrderDAO implements OrderIDAO {
	
	@Autowired
	private SessionFactory sessionfactory;
	@Override
	public List<OrderBean> selectAll() {
		String HQL = "From OrderBean";
		@SuppressWarnings("unchecked")
		TypedQuery<OrderBean> query = sessionfactory.getCurrentSession().createQuery(HQL);
		List<OrderBean> list = query.getResultList();
		return list;
//		return sessionfactory.getCurrentSession().createQuery(HQL).getResultList();
	}

	@Override
	public void delete(OrderBean ob) {
		sessionfactory.getCurrentSession().delete(sessionfactory.getCurrentSession().get(OrderBean.class, ob.getDbOId()));
	}

	@Override
	public Serializable insert(OrderBean ob) {
		return sessionfactory.getCurrentSession().save(ob);
	}
	
	@Override
	public void update(OrderBean ob) {
		sessionfactory.getCurrentSession().update(ob);
	}
	
	@Override
	public void updatetotalprice(OrderBean ob) {
		OrderBean temp = sessionfactory.getCurrentSession().get(OrderBean.class, ob.getDbOId());
		temp.setDbOTotalPrice(ob.getDbOTotalPrice());
	}
	
	@SuppressWarnings("unchecked")
	@Override
	//查詢全部(用訂單編號由大至小排序)
	public List<OrderBean> selectAllByOrderIdDesc() {
		String HQL = "From OrderBean Order By dbOId Desc";
		TypedQuery<OrderBean> query = sessionfactory.getCurrentSession().createQuery(HQL);
		List<OrderBean> list = query.getResultList();
		return list;
//		return sessionfactory.getCurrentSession().createQuery(HQL).getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	//查詢全部(用訂單編號由小至大排序)
	public List<OrderBean> selectAllByOrderIdAsc() {
		String HQL = "From OrderBean Order By dbOId Asc";
		TypedQuery<OrderBean> query = sessionfactory.getCurrentSession().createQuery(HQL);
		List<OrderBean> list = query.getResultList();
		return list;
//		return sessionfactory.getCurrentSession().createQuery(HQL).getResultList();
	}

	
	@SuppressWarnings("unchecked")
	@Override
	//查詢所選擇的訂單狀態
	public List<OrderBean> selectStatus(OrderBean ob) {
		String HQL = "From OrderBean where dbOStatus = :status";
		Session session = sessionfactory.getCurrentSession();
		TypedQuery<OrderBean> query = session.createQuery(HQL);
		query.setParameter("status", ob.getDbOStatus());
		List<OrderBean> list = query.getResultList();	
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	//查詢所鍵入的會員E-MAIL
	public List<OrderBean> selectSomeoneEmail(OrderBean ob) {
		String HQL = "From OrderBean where dbAAccount = :email";
		Session session = sessionfactory.getCurrentSession();
		TypedQuery<OrderBean> query = session.createQuery(HQL);
		query.setParameter("email", ob.getDbOMemberEmail());
		List<OrderBean> list = query.getResultList();	
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	//查詢所鍵入的訂單編號
	public List<OrderBean> selectSomeoneOrderId(OrderBean ob) {
		String HQL = "From OrderBean where dbOId = :oid";
		Session session = sessionfactory.getCurrentSession();
		TypedQuery<OrderBean> query = session.createQuery(HQL);
		query.setParameter("oid", ob.getDbOId());
		List<OrderBean> list = query.getResultList();	
		return list;
	}

	@Override  //會員中心訂單查詢
	public List<OrderBean> selectOne(String acc) {
		String HQL = "From OrderBean where dbOMemberEmail = :acc ORDER BY dbOOrderDate";
		@SuppressWarnings("unchecked")
		TypedQuery<OrderBean> query=sessionfactory.getCurrentSession().createQuery(HQL);
		query.setParameter("acc", acc);
		List<OrderBean> result = query.getResultList();
		System.out.println(result);
		return result;
	}

	@Override //會中心細項用
	public List<OrderBean> selectOneByOid(Integer oid) {
		String HQL = "FROM OrderBean WHERE dbOId=:oid";
		@SuppressWarnings("unchecked")
		TypedQuery<OrderBean> query=sessionfactory.getCurrentSession().createQuery(HQL);
		query.setParameter("oid", oid);
		List<OrderBean> list = query.getResultList();
		return list;
	}

	@Override
	public String genAioCheckOutDevide(Integer oid,Integer allPrice){
//		String index = "http://localhost:8080/FinalProject/shopping/index.html";
		String index = "http://localhost:8080/FinalProject/shopping/sop_orderlist.html?oid="+oid;
		String HQL = "From OrderBean where dbOId = :oid";
		OrderBean ob = (OrderBean) sessionfactory.getCurrentSession().createQuery(HQL).setParameter("oid",oid).getSingleResult();
		Iterator<OrderDetailBean> it = ob.getDbOOrderDetail().iterator();
		String productDetail = "";
		AllInOne all = new AllInOne("");
		AioCheckOutDevide obj = new AioCheckOutDevide();
		int i = 20-"Order".length()-((""+oid).length());
		obj.setMerchantTradeNo("Order" + (""+new Date().getTime()).substring(0,i) + oid);//訂單編號(必須有大小寫英文+數字)
		obj.setMerchantTradeDate(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date())); //下定時間
		obj.setTotalAmount(""+allPrice);//訂購金額(目前是最小值)別改!!!!!
		obj.setTradeDesc("test Description");//下定交易描述
		while (it.hasNext()) {
			productDetail += it.next().getDbODProduct().getDbProductName().getDbPNName();
			productDetail += "#";
		}
		obj.setItemName(productDetail);//商品名稱(需在金流選擇頁	一行一行顯示商品名稱的話，商品名稱請以符號#分隔)
		obj.setReturnURL(index);//結帳完導入的頁面
		obj.setClientBackURL(index);
//		obj.setNeedExtraPaidInfo("N");//以下為紅利等等...
//		obj.setHoldTradeAMT("0");
//		obj.setUseRedeem("N");
//		obj.setCreditInstallment("3,6,12,18,24");
		String form = all.aioCheckOut(obj, null);
		return form;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	//查詢全部(用會員編號由大至小排序)
	public List<OrderBean> selectKeyupOrderDesc2(Integer searchKeyOrder) {
		String key = searchKeyOrder+"%";
		String HQL = "From OrderBean where dbOId like '"+key+"' Order By dbOId DESC";
		TypedQuery<OrderBean> query = sessionfactory.getCurrentSession().createQuery(HQL);
//		query.setParameter("key", searchKeyOrder);
		List<OrderBean> list = query.getResultList();
		System.out.println(list);
		System.out.println("11111111");
		return list;
//		return sessionfactory.getCurrentSession().createQuery(HQL).getResultList();
	}
	@SuppressWarnings("unchecked")
	@Override
	//查詢全部(用會員編號由小至大排序)
	public List<OrderBean> selectKeyupOrderAsc2(Integer searchKeyOrder) {
		String key = searchKeyOrder+"%";
		System.out.println(searchKeyOrder+"111000");
		String HQL = "From OrderBean where dbOId like '"+key+"' Order By dbOId ASC";
		TypedQuery<OrderBean> query = sessionfactory.getCurrentSession().createQuery(HQL);
//		query.setParameter("key", searchKeyOrder);
		List<OrderBean> list = query.getResultList();
		System.out.println("2222222");
		return list;
//		return sessionfactory.getCurrentSession().createQuery(HQL).getResultList();
	}
	

	@Override
	public OrderBean selectUpdate(OrderBean ob) {
		return sessionfactory.getCurrentSession().get(OrderBean.class, ob.getDbOId());
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<OrderBean> selectAlldbOStatus() {
		String HQL = "From OrderBean";
		return sessionfactory.getCurrentSession().createQuery(HQL).getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	//查詢全部(用會員E-mail由大至小排序)
	public List<OrderBean> selectKeyupOrderByNameDesc2(String searchKeyName) {
		String HQL = "From OrderBean where dbOMemberEmail like :key Order By dbOMemberEmail DESC";
		TypedQuery<OrderBean> query = sessionfactory.getCurrentSession().createQuery(HQL);
        query.setParameter("key", searchKeyName+"%");
		List<OrderBean> list = query.getResultList();
		System.out.println(list);
		System.out.println(searchKeyName);
		return list;
//		return sessionfactory.getCurrentSession().createQuery(HQL).getResultList();
	}
	@SuppressWarnings("unchecked")
	@Override
	//查詢全部(用會員E-mail由小至大排序)
	public List<OrderBean> selectKeyupOrderByNameAsc2(String searchKeyName) {

		String HQL = "From OrderBean where dbOMemberEmail like :key Order By dbOMemberEmail ASC";
		TypedQuery<OrderBean> query = sessionfactory.getCurrentSession().createQuery(HQL);
		 query.setParameter("key", searchKeyName+"%");
		List<OrderBean> list = query.getResultList();
		System.out.println(searchKeyName+"333333333333");
		System.out.println("2222222");
		return list;
//		return sessionfactory.getCurrentSession().createQuery(HQL).getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<OrderBean> selectOrderByType(OrderBean ob) {
		String HQL = "From OrderBean where dbOStatus = :type ";
		Session session = sessionfactory.getCurrentSession();
		TypedQuery<OrderBean> query = session.createQuery(HQL);
		query.setParameter("type", ob.getDbOStatus());
		System.out.println("111111111");
		System.out.println(ob.getDbOStatus());
		return query.getResultList();
	
	}
	
	@SuppressWarnings("unchecked")
	@Override
	//查詢全部(用會員編號由小至大排序)
	public List<OrderBean> selectOrderByType2(Byte OrderKey) {
		String HQL = "From OrderBean where dbOStatus = '"+OrderKey+"'";
		TypedQuery<OrderBean> query = sessionfactory.getCurrentSession().createQuery(HQL);
	//query.setParameter("key", OrderKey);
		List<OrderBean> list = query.getResultList();
		System.out.println(OrderKey+"2222222");
		return list;
//		return sessionfactory.getCurrentSession().createQuery(HQL).getResultList();
	}
	

	@Override
	public OrderDetailBean selectUpdateOrderDetail(OrderDetailBean odb) {
		String HQL = "From ProductBean where dbODOrder = :dbODOrder";
		return (OrderDetailBean) sessionfactory.getCurrentSession().createQuery(HQL).setParameter("dbODOrder", odb.getDbODOrder()).getSingleResult();
	}
	
	@Override
	public void updateOrderSetPage(OrderBean ob) {
		OrderBean temp = sessionfactory.getCurrentSession().get(OrderBean.class, ob.getDbOId());
		temp.setDbOPickpCity(ob.getDbOPickpCity());
		temp.setDbOPickupRegion(ob.getDbOPickupRegion());
		temp.setDbOPickUpDetail(ob.getDbOPickUpDetail());
		temp.setDbOTotalPrice(ob.getDbOTotalPrice());
		temp.setDbOStatus(ob.getDbOStatus());
	}
	
}
