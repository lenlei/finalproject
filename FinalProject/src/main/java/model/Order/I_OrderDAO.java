package model.Order;

import java.util.List;

import model.OrderBean;
import model.StatusBean;

public interface I_OrderDAO {


	// 以下方法屬於 訂單查詢
	public OrderBean getOrder(Integer dbOId);// 透過訂單編號dbOId找到訂單

	public List<OrderBean> getAllOrders();// 取得所有訂單資訊

	// 透過會員Email dbOMemberEmail 及 訂單狀態StatusBean dbOStatus 找到訂單
	public List<OrderBean> getOrdersByEmail_And_Status(String dbOMemberEmail, Byte status);

	public List<OrderBean> getOrdersByEmail(String dbOMemberEmail);// 透過會員Email dbOMemberEmail找到訂單

	public List<OrderBean> getOrdersByStatus(Byte status);// 透過訂單狀態StatusBean dbOStatus找到訂單

	// 以下方法屬於 新增訂單
	public void insertOrder(OrderBean ob);

	// 透過資料庫中的order表格內的訂貨日期排序得到最新的訂單編號
	// 取得目前資料庫內最新的訂單編號後(例如001)，經過java程式碼的處理再編新的訂單編號(例如002)給新訂單
	// 然後用OrderBean的建構子新建一筆OrderBean後insert進資料庫的表格

	// 以下方法屬於 刪除訂單
	public void delete(OrderBean ob);// 透過訂單編號dbOId 刪除訂單

	// 以下方法屬於 修改訂單 取貨地址 & 訂單狀態 可修改
	public OrderBean update(OrderBean ob, String dbOPickUpAddress, StatusBean dbOStatus);
	// by Eric
	public void updateAddress(OrderBean ob,Integer dbODOrder);
	// by Eric
	public void changeStatusTo_6(Integer oid);

}