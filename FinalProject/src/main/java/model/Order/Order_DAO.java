package model.Order;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import model.CityBean;
import model.OrderBean;
import model.RegionBean;
import model.StatusBean;

@Repository
public class Order_DAO implements I_OrderDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public Session getSession() {
		// 透過sessionFactory取得getCurrentSession()與資料庫交易
		return this.sessionFactory.getCurrentSession();
	}

	public Order_DAO() {

	}

////////////////////////////////////////查詢訂單//////////////////////////////////////////////////////////
	@SuppressWarnings("unchecked")
	@Override
	public List<OrderBean> getAllOrders() { // 取得所有訂單資訊
		List<OrderBean> list = new ArrayList<OrderBean>();
		String hql = "FROM OrderBean";
		list = (List<OrderBean>) getSession().createQuery(hql).getResultList();
		return list;
	}

	@Override // 透過會員Email dbOMemberEmail 及 訂單狀態StatusBean dbOStatus 找到訂單
	public List<OrderBean> getOrdersByEmail_And_Status(String dbOMemberEmail, Byte status) {
		String hql = "FROM OrderBean ob ORDER BY ob.dbOOrderDate DESC WHERE ob.dbAAccount = :email AND ob.dbOStatus = :status";
		@SuppressWarnings("unchecked")
		List<OrderBean> list = this.getSession().createQuery(hql).setParameter("email", dbOMemberEmail).setParameter("status", status).getResultList();
		return list;
	}

	@Override
	public OrderBean getOrder(Integer dbOId) { // 透過訂單編號dbOId找到訂單
		OrderBean ob = this.getSession().get(OrderBean.class, dbOId);
		return ob;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<OrderBean> getOrdersByEmail(String dbOMemberEmail) { // 透過會員Email dbOMemberEmail找到訂單
		String hql = "FROM OrderBean ob ORDER BY ob.dbOOrderDate DESC WHERE ob.dbAAccount = :email";
		List<OrderBean> list = this.getSession().createQuery(hql).setParameter("email", dbOMemberEmail).getResultList();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<OrderBean> getOrdersByStatus(Byte status) { // 透過訂單狀態StatusBean dbOStatus找到訂單
		String hql = "FROM OrderBean ob ORDER BY ob.dbOOrderDate DESC WHERE ob.dbOStatus = :status";
		List<OrderBean> list = this.getSession().createQuery(hql).setParameter("status", status).getResultList();
		return list;
	}

	//////////////////////////////////////// 新增訂單//////////////////////////////////////////////////////////
	@Override
	public void insertOrder(OrderBean ob) {
		if (ob != null) {
			OrderBean temp = this.getSession().get(OrderBean.class, ob.getDbOId());
			if (temp == null) { // 確定目前的表格內沒有該訂單(ob)才可新增進去
				this.getSession().save(ob);
				System.out.println("新增訂單成功");
			} else {
				System.out.println("新增失敗  訂單已存在!");
			}
		}
	}

	//////////////////////////////////////// 刪除訂單//////////////////////////////////////////////////////////
	@Override
	public void delete(OrderBean ob) {
		this.getSession().delete(ob);
	}

	//////////////////////////////////////// 修改訂單//////////////////////////////////////////////////////////
	@Override
	public OrderBean update(OrderBean ob, String dbOPickUpAddress, StatusBean dbOStatus) {
		// 取貨地址 & 訂單狀態 可修改String dbOPickUpAddress, StatusBean dbOStatus
		//ob.setDbOPickUpAddress(dbOPickUpAddress);
		ob.setDbOStatus(dbOStatus);
		this.getSession().update(ob);
		return ob;
	}

	@Override
	public void updateAddress(OrderBean ob,Integer dbODOrder) {
		System.out.println("ob.getDbOId()====>"+ob.getDbOId());
			OrderBean o = this.getSession().get(OrderBean.class, ob.getDbOId());
			o.setDbOPickpCity(new CityBean(ob.getDbOPickpCity().getDbCid(),null));
			o.setDbOPickupRegion(new RegionBean(ob.getDbOPickupRegion().getDbRid()));
			o.setDbOPickUpDetail(ob.getDbOPickUpDetail());		
	}

	@Override
	public void changeStatusTo_6(Integer oid) {//確認付款後，將訂單狀態改成6 (已完成)
			System.out.println("changeStatusTo_6的oid==>"+oid);
			OrderBean o = this.getSession().get(OrderBean.class, oid);
//			o.setDbOStatus(new StatusBean((byte) 6, null));		
			o.setDbOStatus(new StatusBean((byte) 6, "已完成"));		
	}

}