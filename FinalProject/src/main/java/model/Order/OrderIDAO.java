package model.Order;

import java.io.Serializable;
import java.util.List;

import model.OrderBean;
import model.OrderDetailBean;
import model.ProductBean;
import model.ProductNameBean;

public interface OrderIDAO {

	public List<OrderBean> selectAll();
	public List<OrderBean> selectAllByOrderIdDesc();
	public List<OrderBean> selectAllByOrderIdAsc();
	public List<OrderBean> selectStatus(OrderBean ob);
	public List<OrderBean> selectSomeoneEmail(OrderBean ob);
	public List<OrderBean> selectSomeoneOrderId(OrderBean ob);
	public void delete(OrderBean ob);
	public Serializable insert(OrderBean ob);
	public void update(OrderBean ob);
	public void updatetotalprice(OrderBean ob);
	public List<OrderBean> selectOne(String acc);
	public List<OrderBean> selectOneByOid(Integer oid);
	public String genAioCheckOutDevide(Integer oid,Integer allPrice);
	public List<OrderBean> selectKeyupOrderDesc2(Integer searchKeyOrder);
	public List<OrderBean> selectKeyupOrderAsc2(Integer searchKeyOrder);

	public List<OrderBean> selectAlldbOStatus();
	public OrderBean selectUpdate(OrderBean ob);
	public OrderDetailBean selectUpdateOrderDetail(OrderDetailBean odb);

	public List<OrderBean> selectKeyupOrderByNameDesc2(String searchKeyName);
	public List<OrderBean> selectKeyupOrderByNameAsc2(String searchKeyName);
	public List<OrderBean> selectOrderByType(OrderBean ob);
	public List<OrderBean> selectOrderByType2(Byte OrderKey);
	public void updateOrderSetPage(OrderBean ob);
	

}
