package model.introduction;

import java.util.List;

import model.CompanyAddressBean;
import model.CompanyEmailBean;
import model.CompanyIntroductionBean;
import model.CompanyProcessPicBean;
import model.CompanyProductProcessBean;
import model.CompanySiteMapBean;
import model.CompanyTelBean;
import model.IntroductionSetBean;

public interface IntroductionIDAO {
	
	//selectAll 撈全部資料
	public void selectalldetails(IntroductionSetBean ib);
	// 因為selectAll 是全部撈進來，但無法執行，先用撈每個小bean
	public List<CompanyAddressBean> selectAddress();
	public List<CompanyEmailBean> selectEmail();
	public List<CompanyIntroductionBean> selectIntroduction();
	public List<CompanyProcessPicBean> selectProcessPic();
	public List<CompanyProductProcessBean> selectProductProcess();
	public List<CompanySiteMapBean> selectSiteMap();
	public List<CompanyTelBean> selectTel();
	
	//前台
	public IntroductionSetBean selectAll();
	//後台
	public IntroductionSetBean selectAllback();
	//update全部資料
	public void updatealldetail(IntroductionSetBean ib);
}