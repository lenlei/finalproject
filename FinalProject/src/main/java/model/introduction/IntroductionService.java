package model.introduction;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import model.CompanyAddressBean;
import model.CompanyEmailBean;
import model.CompanyIntroductionBean;
import model.CompanyProcessPicBean;
import model.CompanyProductProcessBean;
import model.CompanySiteMapBean;
import model.CompanyTelBean;
import model.IntroductionSetBean;

@Service
public class IntroductionService {
	@Autowired
	private IntroductionIDAO IDAO;
	
	public List<CompanyAddressBean> selectAddress() {
		return IDAO.selectAddress();
	}

	public List<CompanyEmailBean> selectEmail() {
		return IDAO.selectEmail();
	}
	
	public List<CompanyIntroductionBean> selectIntroduction() {
		return IDAO.selectIntroduction();
	} 
	
	public List<CompanyProcessPicBean> selectProcessPic() {
		return IDAO.selectProcessPic();
	}
	
	public List<CompanyProductProcessBean> selectProductProcess() {
		return IDAO.selectProductProcess();
	}
	
	public List<CompanySiteMapBean> selectSiteMap() {
		return IDAO.selectSiteMap();
	}
	
	public List<CompanyTelBean> selectTel() {
		return IDAO.selectTel();
	}
	//前台
	public IntroductionSetBean selectAll(){
		
		return IDAO.selectAll();
	}
	//後台
	public IntroductionSetBean selectAllback(){
		
		return IDAO.selectAllback();
	}
	
	//更新方法
	
	public void updatealldetail(IntroductionSetBean ib) {
		IDAO.updatealldetail(ib);
	}

}