package model.introduction;

import java.util.List;


import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import model.CompanyAddressBean;
import model.CompanyEmailBean;
import model.CompanyIntroductionBean;
import model.CompanyProcessPicBean;
import model.CompanyProductProcessBean;
import model.CompanySiteMapBean;
import model.CompanyTelBean;
import model.IntroductionSetBean;
import model.StatusBean;

@Repository
public class IntroductionDAO implements IntroductionIDAO {
	@Autowired
	private SessionFactory sessionFactory;
	
	public Session getSession() {
		//透過 sessionFactory取得getCurrentSession()與資料庫交易
		return this.sessionFactory.getCurrentSession();
	}

	@Override
	public void selectalldetails(IntroductionSetBean ib) {
		IntroductionSetBean InBean=sessionFactory.getCurrentSession().get(IntroductionSetBean.class,1);
		InBean.getDbISAddress().setDbIAddress(ib.getDbISAddress().getDbIAddress());
		InBean.getDbISEmail().setDbIEmail(ib.getDbISEmail().getDbIEmail());
		InBean.getDbISIntroduction().setDbIIntroduction(ib.getDbISIntroduction().getDbIIntroduction());
		InBean.getDbISitemap().setDbIISitemap(ib.getDbISitemap().getDbIISitemap());
		InBean.getDbISProcesspics().setDbIProcesspics(ib.getDbISProcesspics().getDbIProcesspics());
		InBean.getDbISProductprocess().setDbIProductprocess(ib.getDbISProductprocess().getDbIProductprocess());
		InBean.getDbISTel().setDbITel(ib.getDbISTel().getDbITel());

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CompanyAddressBean> selectAddress() {
		String HQL="From CompanyAddressBean";
		List<CompanyAddressBean> sa=sessionFactory.getCurrentSession().createQuery(HQL).getResultList();
		System.out.println("333");
		return sa;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CompanyEmailBean> selectEmail() {
		String HQL="From CompanyEmailBean";
		List<CompanyEmailBean> cb=sessionFactory.getCurrentSession().createQuery(HQL).getResultList();
		return cb;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CompanyIntroductionBean> selectIntroduction() {
		String HQL ="From CompanyIntroductionBean";
		List<CompanyIntroductionBean> cii=sessionFactory.getCurrentSession().createQuery(HQL).getResultList();
		return cii;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CompanyProcessPicBean> selectProcessPic() {
		String HQL="From CompanyProcessPicBean";
		List<CompanyProcessPicBean> cp=sessionFactory.getCurrentSession().createQuery(HQL).getResultList();
		return cp;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CompanyProductProcessBean> selectProductProcess() {
		String HQL="From CompanyProductProcessBean";
		List<CompanyProductProcessBean> cpb=sessionFactory.getCurrentSession().createQuery(HQL).getResultList();
		return cpb;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CompanySiteMapBean> selectSiteMap() {
		String HQL ="From CompanySiteMapBean";
		List<CompanySiteMapBean> csb=sessionFactory.getCurrentSession().createQuery(HQL).getResultList();
		return csb;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CompanyTelBean> selectTel() {
		String HQL = "From CompanyTelBean";
		List<CompanyTelBean> ct=sessionFactory.getCurrentSession().createQuery(HQL).getResultList();
		return ct;
	}

	//前台
	@SuppressWarnings("unchecked")
	public IntroductionSetBean selectAll(){
		String HQL ="From IntroductionSetBean";
		IntroductionSetBean Bi=(IntroductionSetBean) sessionFactory.getCurrentSession().createQuery(HQL).getSingleResult();	

		return Bi;
	}
	
	//後台
		@SuppressWarnings("unchecked")
		public IntroductionSetBean selectAllback(){
			String HQL ="From IntroductionSetBean";
			IntroductionSetBean Bi=(IntroductionSetBean) sessionFactory.getCurrentSession().createQuery(HQL).getSingleResult();
			return Bi;
		}
	
	//更新資料
	@Override
	public void updatealldetail(IntroductionSetBean ib) {//從controller 
		IntroductionSetBean IIbean=sessionFactory.getCurrentSession().get(IntroductionSetBean.class,1); //get(bean類別,主鍵)
		IIbean.getDbISIntroduction().setDbIIntroduction(ib.getDbISIntroduction().getDbIIntroduction()); //先get再set，大bean裡set
		IIbean.getDbISIntroduction().setDbIStatus(new StatusBean(ib.getDbISIntroduction().getDbIStatus().getDbSId(),null));
//		IIbean.getDbISIntroduction().setDbIStatus(ib.getDbISIntroduction().getDbIStatus());
		IIbean.getDbISTel().setDbITel(ib.getDbISTel().getDbITel());
		IIbean.getDbISTel().setDbIStatus(new StatusBean(ib.getDbISTel().getDbIStatus().getDbSId(),null));
		IIbean.getDbISEmail().setDbIEmail(ib.getDbISEmail().getDbIEmail());
		IIbean.getDbISEmail().setDbIStatus(new StatusBean(ib.getDbISEmail().getDbIStatus().getDbSId(),null));
		IIbean.getDbISAddress().setDbIAddress(ib.getDbISAddress().getDbIAddress());
		IIbean.getDbISAddress().setDbIStatus(new StatusBean(ib.getDbISAddress().getDbIStatus().getDbSId(),null));
		IIbean.getDbISProductprocess().setDbIProductprocess(ib.getDbISProductprocess().getDbIProductprocess());
		IIbean.getDbISProductprocess().setDbIStatus(new StatusBean(ib.getDbISProductprocess().getDbIStatus().getDbSId(),null));
		System.out.println("DAO..."+ib.getDbISProcesspics().getDbIProcesspics());
		IIbean.getDbISProcesspics().setDbIProcesspics(ib.getDbISProcesspics().getDbIProcesspics());
		IIbean.getDbISProcesspics().setDbIStatus(new StatusBean(ib.getDbISProcesspics().getDbIStatus().getDbSId(),null));
		IIbean.getDbISitemap().setDbIISitemap(ib.getDbISitemap().getDbIISitemap());
		IIbean.getDbISitemap().setDbIStatus(new StatusBean(ib.getDbISitemap().getDbIStatus().getDbSId(),null));
		
//		@Override
//		public void updatealldetail(IntroductionSetBean ib) {//從controller 
//			IntroductionSetBean IIbean=sessionFactory.getCurrentSession().get(IntroductionSetBean.class,1); //get(bean類別,主鍵)
//			IIbean.getDbISIntroduction().setDbIIntroduction(ib.getDbISIntroduction().getDbIIntroduction()); //先get再set，大bean裡set
//			//IIbean.getDbISIntroduction().setDbIStatus(ib.getDbISIntroduction().getDbIStatus());//按鈕
//			IIbean.getDbISIntroduction().setDbIStatus(new StatusBean(ib.getDbISIntroduction().getDbIStatus().getDbSId(),null));
//			IIbean.getDbISTel().setDbITel(ib.getDbISTel().getDbITel());
//			IIbean.getDbISTel().setDbIStatus(ib.getDbISTel().getDbIStatus());
//			IIbean.getDbISEmail().setDbIEmail(ib.getDbISEmail().getDbIEmail());
//			IIbean.getDbISEmail().setDbIStatus(ib.getDbISEmail().getDbIStatus());
//			IIbean.getDbISAddress().setDbIAddress(ib.getDbISAddress().getDbIAddress());
//			IIbean.getDbISAddress().setDbIStatus(ib.getDbISAddress().getDbIStatus());
//			IIbean.getDbISProductprocess().setDbIProductprocess(ib.getDbISProductprocess().getDbIProductprocess());
//			IIbean.getDbISProductprocess().setDbIStatus(ib.getDbISProductprocess().getDbIStatus());
//			IIbean.getDbISProcesspics().setDbIProcesspics(ib.getDbISProcesspics().getDbIProcesspics());
//			IIbean.getDbISProcesspics().setDbIStatus(ib.getDbISProcesspics().getDbIStatus());
//			IIbean.getDbISitemap().setDbIISitemap(ib.getDbISitemap().getDbIISitemap());
//			IIbean.getDbISitemap().setDbIStatus(ib.getDbISitemap().getDbIStatus());

	}

}
