package model.cityAndRegion;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CityService {
	@Autowired

	private CityIDao CDAO;

	public Map<Integer, String> selectAllCity() {

		return CDAO.selectAllCity();
	}

	public Map<Integer, String> selectAllRegion(int cid) {
		Map<Integer, String> list = CDAO.selectAllRegion(cid);
		return list;
	}
}
