package model.cityAndRegion;

import java.util.Map;

import model.CityBean;

public interface CityIDao {

	public Map<Integer, String> selectAllCity();
	
	public Map<Integer, String> selectAllRegion(int cid);

	CityBean CitynameTurnCityid(String CityName);
	
	
}
