package model.cityAndRegion;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import model.CityBean;
import model.RegionBean;
@Repository
public class CityDao implements CityIDao {

	@Autowired
	private SessionFactory sessionfactory;
	@Override
	public Map<Integer,String> selectAllCity() {
		String HQL = "From CityBean order by dbCId";
		@SuppressWarnings("unchecked")
		List<CityBean> list = sessionfactory.getCurrentSession().createQuery(HQL).getResultList();
		 Map<Integer, String> city = new TreeMap<Integer, String>() ;
			for(CityBean b:list) {
				b.getDbCid();
				b.getDbCName();
				city.put(b.getDbCid(),b.getDbCName());
				System.out.println(b.getDbCid()+","+b.getDbCName());
						}
			return city;
	}
	
	@Override
	public Map<Integer,String> selectAllRegion(int cid) {
		String HQL = "From RegionBean where dbCId =:CId order by dbRId";
		@SuppressWarnings("unchecked")
		List<RegionBean> list = sessionfactory.getCurrentSession().createQuery(HQL).setParameter("CId", cid).getResultList();
		Map<Integer, String> region = new TreeMap<Integer, String>() ;
		for(RegionBean b:list) {
			b.getDbRid();
			b.getDbRName();
			region.put(b.getDbRid(), b.getDbRName());
			System.out.println(b.getDbRid()+","+b.getDbRName());
		}
		return region;
	}
	
	@Override
	public CityBean CitynameTurnCityid(String CityName) {
		String HQL = "From CityBean where dbCNasme =:CityName ";
		 
		return sessionfactory.getCurrentSession().createQuery(HQL,CityBean.class).setParameter("CityName", CityName).getSingleResult();
	}

}
