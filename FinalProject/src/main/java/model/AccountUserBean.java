package model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "AccountUser")
public class AccountUserBean {
	@Id
	@Column(columnDefinition = "varchar(40)", nullable = false)
	private String dbAAccount;
	@Column(columnDefinition = "varchar(40)", nullable = false)
	private String dbAPassword = "";
	@Column(columnDefinition="nvarchar(20)")
	private String dbAName = "";	
	@Column(columnDefinition="nvarchar(100)")
	private String dbAPickupDetail ; //會員地址詳細
	@ManyToOne
	@JoinColumn(name="dbCid")
	private CityBean dbAPickpCity; //會員地址縣市
	@ManyToOne
	@JoinColumn(name="dbRid")
	private RegionBean dbAPickupRegion; //會員地址區域
	@Column(columnDefinition="varchar(10)")
	private String dbAMobile = "";
	@OneToMany(mappedBy="dbOMemberEmail")
	private List<OrderBean> dbAOrderList;
	@ManyToOne
	@JoinColumn(name="dbAStatus",nullable=false)
	private StatusBean dbAStatus;
	@OneToMany(mappedBy="dbWAccount")
	private List<WorkBean> dbWorkList;
	@OneToMany(mappedBy="dbAAccount")
	@JsonBackReference
	private List<TemporaryBean> dbTemporary;
	@Column(columnDefinition="int")
	private Integer verification;   //認證碼
	@Column(columnDefinition="bigint")
	private Long verificationTime;  //認證時間
	
	public AccountUserBean() {}
	

	public AccountUserBean(String dbAAccount, String dbAPassword) {
		this.dbAAccount = dbAAccount;
		this.dbAPassword = dbAPassword;
	}
	
	public AccountUserBean(String dbAAccount, StatusBean dbAStatus) {
		this.dbAAccount = dbAAccount;
		this.dbAStatus = dbAStatus;
	}

	public AccountUserBean(String dbAAccount, String dbAPassword, StatusBean dbAStatus) {
		this.dbAAccount = dbAAccount;
		this.dbAPassword = dbAPassword;
		this.dbAStatus = dbAStatus;
	}
	
	
	public AccountUserBean(String dbAAccount, String dbAPassword, String dbAName, String dbAPickupDetail,
			CityBean dbAPickpCity, RegionBean dbAPickupRegion, String dbAMobile,StatusBean dbAStatus) {
		this.dbAAccount = dbAAccount;
		this.dbAPassword = dbAPassword;
		this.dbAName = dbAName;
		this.dbAPickupDetail = dbAPickupDetail;
		this.dbAPickpCity = dbAPickpCity;
		this.dbAPickupRegion = dbAPickupRegion;
		this.dbAMobile = dbAMobile;
		this.dbAStatus = dbAStatus;
	}
	

	public AccountUserBean(String dbAAccount, String dbAPassword, String dbAName, String dbAPickupDetail,
			CityBean dbAPickpCity, RegionBean dbAPickupRegion, String dbAMobile,
			List<OrderBean> dbAOrderList, StatusBean dbAStatus, List<WorkBean> dbWorkList,
			List<TemporaryBean> dbTemporary, Integer verification, Long verificationTime) {
		super();
		this.dbAAccount = dbAAccount;
		this.dbAPassword = dbAPassword;
		this.dbAName = dbAName;
		this.dbAPickupDetail = dbAPickupDetail;
		this.dbAPickpCity = dbAPickpCity;
		this.dbAPickupRegion = dbAPickupRegion;
		this.dbAMobile = dbAMobile;
		this.dbAOrderList = dbAOrderList;
		this.dbAStatus = dbAStatus;
		this.dbWorkList = dbWorkList;
		this.dbTemporary = dbTemporary;
		this.verification = verification;
		this.verificationTime = verificationTime;
	}

	public String getDbAAccount() {return dbAAccount;}
	public void setDbAAccount(String dbAAccount) {this.dbAAccount = dbAAccount;}

	public String getDbAPassword() {return dbAPassword;}
	public void setDbAPassword(String dbAPassword) {this.dbAPassword = dbAPassword;}

	public String getDbAName() {return dbAName;}
	public void setDbAName(String dbAName) {this.dbAName = dbAName;}

	public String getDbAPickupDetail() {return dbAPickupDetail;}
	public void setDbAPickupDetail(String dbAPickupDetail) {this.dbAPickupDetail = dbAPickupDetail;}

	public CityBean getDbAPickpCity() {return dbAPickpCity;}
	public void setDbAPickpCity(CityBean dbAPickpCity) {this.dbAPickpCity = dbAPickpCity;}

	public RegionBean getDbAPickupRegion() {return dbAPickupRegion;}
	public void setDbAPickupRegion(RegionBean dbAPickupRegion) {this.dbAPickupRegion = dbAPickupRegion;}

	public String getDbAMobile() {return dbAMobile;}
	public void setDbAMobile(String dbAMobile) {this.dbAMobile = dbAMobile;}

	public List<OrderBean> getDbAOrderList() {return dbAOrderList;}
	public void setDbAOrderList(List<OrderBean> dbAOrderList) {this.dbAOrderList = dbAOrderList;}

	public StatusBean getDbAStatus() {return dbAStatus;}
	public void setDbAStatus(StatusBean dbAStatus) {this.dbAStatus = dbAStatus;}

	public List<WorkBean> getDbWorkList() {return dbWorkList;}
	public void setDbWorkList(List<WorkBean> dbWorkList) {this.dbWorkList = dbWorkList;}

	public List<TemporaryBean> getDbTemporary() {return dbTemporary;}
	public void setDbTemporary(List<TemporaryBean> dbTemporary) {this.dbTemporary = dbTemporary;}

	public Integer getVerification() {return verification;}
	public void setVerification(Integer verification) {this.verification = verification;}

	public Long getVerificationTime() {return verificationTime;}
	public void setVerificationTime(Long verificationTime) {this.verificationTime = verificationTime;}

}
  