package model.OrderDetail;

import java.util.List;

import model.OrderBean;
import model.OrderDetailBean;
import model.ProductBean;

public interface I_OrderDetailDAO {

	// 查詢
	List<OrderDetailBean> getOrderDetails(String dbOId);// 透過訂單編號dbOId找到訂單細項

	OrderDetailBean getDetailByPId(OrderBean ob, String dbPId);// 透過商品編號dbPId找到訂單細項

	// 新增
	void insertOrderDetail(OrderDetailBean oDetail);// 透過訂單編號dbOId新增訂單細項
//	OrderDetailBean insertOrderDetail(OrderDetailBean oDetail, String dbOId,ProductBean dbODProduct);
	
	// 修改 訂購數量
	void updateDetail(Integer dbODOrder,String paymentMethod,String transferMethod);
//	OrderDetailBean updateDetail(OrderDetailBean oDetail,Double newQty);
	// 刪除
	boolean delete(OrderDetailBean oDetail);// 刪除訂單細項
	
	//
	List<OrderDetailBean> selectOrderDetail(Integer oid);// 刪除訂單細項

	List<OrderDetailBean> selectAll();//搜尋全部資料
	
	public void deleteOD(OrderDetailBean odb);//後台刪除商品細項之商品
	
	public void updateODdbODAmount(OrderDetailBean odb);
}
