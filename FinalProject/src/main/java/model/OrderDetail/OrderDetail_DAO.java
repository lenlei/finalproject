
package model.OrderDetail;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import model.OrderBean;
import model.OrderDetailBean;
import model.ProductBean;

@Repository
public class OrderDetail_DAO implements I_OrderDetailDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public Session getSession() {
		return this.sessionFactory.getCurrentSession();
	}
////////////////////////////////////////查詢訂單細項//////////////////////////////////////////////////////////
	@SuppressWarnings("unchecked")
	@Override
	public List<OrderDetailBean> getOrderDetails(String dbOId) {
		// 透過訂單編號dbOId找到訂單細項
		List<OrderDetailBean> list = new ArrayList<OrderDetailBean>();
		String hql = "FROM OrderDetailBean odb WHERE odb.dbOId =:id";
		list = (List<OrderDetailBean>) getSession().createQuery(hql).setParameter("id", dbOId).getResultList();
		return list;
	}

	@Override
	public OrderDetailBean getDetailByPId(OrderBean ob, String dbPId) {
		// 先透過訂單編號DbOId找到訂單
		OrderBean order = getSession().get(OrderBean.class, ob.getDbOId());
		List<OrderDetailBean> orderDetails = order.getDbOOrderDetail();
		if (orderDetails != null) {
			for (OrderDetailBean orderDetail : orderDetails) {
				// 透過商品編號dbPId找到訂單細項
				if (orderDetail.getPk() == Integer.parseInt(dbPId)) {
					return orderDetail;
				}
			}
		}

		return null;
	}
	////////////////////////////////////////新增訂單細項//////////////////////////////////////////////////////////
	@Override
	public void insertOrderDetail(OrderDetailBean oDetail) {
		if(oDetail!=null) {
			getSession().save(oDetail);
			System.out.println("新增訂單細項");
		}
	}
	////////////////////////////////////////修改訂單細項 (訂購數量)//////////////////////////////////////////////////////////
	@SuppressWarnings("unchecked")
	@Override
	public void updateDetail(Integer dbODOrder,String paymentMethod,String transferMethod){
		String hql="FROM OrderDetailBean WHERE dbODOrder=:dbODOrder";
		List<OrderDetailBean> details = this.getSession().createQuery(hql).setParameter("dbODOrder", dbODOrder).getResultList();
		for(OrderDetailBean odb :details) {
			System.out.println("odb訂單細項update....");
			odb.setDbODpaymentMethod(paymentMethod);
			odb.setDbODtransferMethod(transferMethod);
		}
	}
	////////////////////////////////////////刪除訂單細項//////////////////////////////////////////////////////////
	@Override
	public boolean delete(OrderDetailBean oDetail) {
		if(oDetail!=null) {
			getSession().delete(oDetail);
			return true;
		}
		return false;
	}
	
	@Override //會員中心查詢使用
	public List<OrderDetailBean> selectOrderDetail(Integer oid) {
		String HQL="FROM OrderDetailBean WHERE dbODOrder=:oid";
		@SuppressWarnings("unchecked")
		TypedQuery<OrderDetailBean> query=getSession().createQuery(HQL);
		query.setParameter("oid", oid);
		List<OrderDetailBean> list = query.getResultList();
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<OrderDetailBean> selectAll() {
		
		String HQL = "From OrderDetailBean";
		return sessionFactory.getCurrentSession().createQuery(HQL).getResultList();
	}
	
	@Override
	public void deleteOD(OrderDetailBean odb) {
//		sessionFactory.getCurrentSession().delete(odb);
//		this.getSession().delete(this.getSession().get(OrderDetailBean.class,odb.getPk()));
		sessionFactory.getCurrentSession().delete(sessionFactory.getCurrentSession().get(OrderDetailBean.class,odb.getPk()));
	}
	
	@Override
	public void updateODdbODAmount(OrderDetailBean odb) {
		System.out.println("----------dao");
		OrderDetailBean temp =sessionFactory.getCurrentSession().get(OrderDetailBean.class, odb.getPk());
		System.out.println("----------dao1");
		temp.setDbODQty(odb.getDbODQty());
		temp.setDbODAmount(odb.getDbODAmount());
	}
}