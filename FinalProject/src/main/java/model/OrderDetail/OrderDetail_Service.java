package model.OrderDetail;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import model.OrderBean;
import model.OrderDetailBean;
import model.ProductBean;

@Service
public class OrderDetail_Service {
	@Autowired
	private OrderDetail_DAO orderDetail_DAO;

	// 查詢

	public List<OrderDetailBean> getOrderDetails(String dbOId) {// 透過 訂單 編號dbOId找到訂單細項
		return orderDetail_DAO.getOrderDetails(dbOId);
	}

	public OrderDetailBean getDetailByPId(OrderBean ob, String dbPId) {// 透過 商品 編號dbPId找到訂單細項
		return orderDetail_DAO.getDetailByPId(ob, dbPId);
	}

	// 新增
	public void insertOrderDetail(OrderDetailBean oDetail) {// 透過訂單編號dbOId新增訂單細項
		orderDetail_DAO.insertOrderDetail(oDetail);
	}
	// 刪除	
	public boolean delete(OrderDetailBean oDetail) {// 刪除訂單細項
		return orderDetail_DAO.delete(oDetail);
	}
	// 修改 訂購數量
	public void updateDetail(Integer dbODOrder,String paymentMethod,String transferMethod) {
		orderDetail_DAO.updateDetail(dbODOrder,paymentMethod,transferMethod);
	}
	
	public List<OrderDetailBean> selectOrderDetail(Integer oid){
		return orderDetail_DAO.selectOrderDetail(oid);
	}
	
	public List<OrderDetailBean> selectAll() {
		return orderDetail_DAO.selectAll();
	}
	
	public void deleteOD(OrderDetailBean odb) {
		orderDetail_DAO.deleteOD(odb);
	}
	
	public void updateODdbODAmount(OrderDetailBean odb) {
		orderDetail_DAO.updateODdbODAmount(odb);
	}
	
}
