package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="LicenseAccount")
public class LicenseAccountBean {
	@Id
	@Column(columnDefinition="varchar(40)",nullable=false)
	private String dbLAAccount = "";
	@Column(columnDefinition="varchar(20)",nullable=false)
	private String dbLAPassword = "";
	@Column(columnDefinition="nvarchar(20)")
	private String dbLAUserName= "";

	@ManyToOne
	@JoinColumn(name="dbLAStatus",nullable=false)
	private StatusBean dbLAStatus;
	@ManyToOne
	@JoinColumn(name="dbLGId",nullable=false)
	private LicenseGroupBean dbLAGroup;
	
	public LicenseAccountBean() {}
	
	public LicenseAccountBean(String dbLAAccount, String dbLAPassword, String dbLAUserName, StatusBean dbLAStatus,
			LicenseGroupBean dbLAGroupDefine) {
		this.dbLAAccount = dbLAAccount;
		this.dbLAPassword = dbLAPassword;
		this.dbLAUserName = dbLAUserName;
		this.dbLAStatus = dbLAStatus;
		this.dbLAGroup = dbLAGroupDefine;
	}


	public String getDbLAAccount() {return dbLAAccount;}
	public void setDbLAAccount(String dbLAAccount) {this.dbLAAccount = dbLAAccount;}
	
	public String getDbLAPassword() {return dbLAPassword;}
	public void setDbLAPassword(String dbLAPassword) {this.dbLAPassword = dbLAPassword;}
	
	public String getDbLAUserName() {return dbLAUserName;}
	public void setDbLAUserName(String dbLAUserName) {this.dbLAUserName = dbLAUserName;}
	
	public StatusBean getDbLAStatus() {return dbLAStatus;}
	public void setDbLAStatus(StatusBean dbLAStatus) {this.dbLAStatus = dbLAStatus;}
	
	public LicenseGroupBean getDbLAGroupDefine() {return dbLAGroup;}
	public void setDbLAGroupDefine(LicenseGroupBean dbLAGroup) {this.dbLAGroup = dbLAGroup;}
	
}


