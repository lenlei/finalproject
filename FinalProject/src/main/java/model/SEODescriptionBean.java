package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="SEODescription")
public class SEODescriptionBean {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer pk;
	@Column(columnDefinition = "nvarchar(100)", nullable = false)
	private String dbSDescription = "";
	@ManyToOne
	@JoinColumn(name="dbSStatus",nullable=false)
	private StatusBean dbSStatus;
	
	public SEODescriptionBean() {
		super();
	}
	public SEODescriptionBean(Integer pk) {
		super();
		this.pk = pk;
	}
	
	public SEODescriptionBean(Integer pk, String dbSDescription, StatusBean dbSStatus) {
		super();
		this.pk = pk;
		this.dbSDescription = dbSDescription;
		this.dbSStatus = dbSStatus;
	}

	public Integer getPk() {return pk;}
	public void setPk(Integer pk) {this.pk = pk;}
	
	public String getDbSDescription() {return dbSDescription;}
	public void setDbSDescription(String dbSDescription) {this.dbSDescription = dbSDescription;}

	public StatusBean getDbSStatus() {return dbSStatus;}
	public void setDbSStatus(StatusBean dbSStatus) {this.dbSStatus = dbSStatus;}

}
