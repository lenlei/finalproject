package model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "ShopOrder")
public class OrderBean{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer dbOId;
	@Column(columnDefinition = "datetime", nullable = false)
	private Date dbOOrderDate = new Date();
	@ManyToOne
	@JoinColumn(name="dbCid")
	private CityBean dbOPickpCity;//訂單地址縣市
	@ManyToOne
	@JoinColumn(name="dbRid")
	private RegionBean dbOPickupRegion;//取貨地址區域
	@Column(columnDefinition = "nvarchar(100)")
	private String dbOPickUpDetail;//取貨地址詳細
	@Column(columnDefinition = "datetime", nullable = false)
	private Date dbODeliverTime = new Date();
	@Column(columnDefinition = "datetime", nullable = false)
	private Date dbOArriveDate = new Date();
	@Column(columnDefinition = "int", nullable = false)
	private Integer dbOTotalPrice = 0;
	@ManyToOne
	@JoinColumn(name="dbOStatus",nullable=false)
	private StatusBean dbOStatus;

	@Column(columnDefinition = "varchar(40)")
	private String dbOMemberEmail ;
	@OneToMany(mappedBy="dbODOrder")
	private List<OrderDetailBean> dbOOrderDetail;
	
	public OrderBean() {
	}

	public OrderBean(Integer dbOId) {
		super();
		this.dbOId = dbOId;
	}
	
	public OrderBean(Integer dbOId, Date dbOOrderDate, CityBean dbOPickpCity, RegionBean dbOPickupRegion,
			String dbOPickUpDetail, Date dbODeliverTime, Date dbOArriveDate, Integer dbOTotalPrice,
			StatusBean dbOStatus, String dbOMemberEmail, List<OrderDetailBean> dbOOrderDetail) {
		super();
		this.dbOId = dbOId;
		this.dbOOrderDate = dbOOrderDate;
		this.dbOPickpCity = dbOPickpCity;
		this.dbOPickupRegion = dbOPickupRegion;
		this.dbOPickUpDetail = dbOPickUpDetail;
		this.dbODeliverTime = dbODeliverTime;
		this.dbOArriveDate = dbOArriveDate;
		this.dbOTotalPrice = dbOTotalPrice;
		this.dbOStatus = dbOStatus;
		this.dbOMemberEmail = dbOMemberEmail;
		this.dbOOrderDetail = dbOOrderDetail;
	}
	
	public CityBean getDbOPickpCity() {
		return dbOPickpCity;
	}

	public void setDbOPickpCity(CityBean dbOPickpCity) {
		this.dbOPickpCity = dbOPickpCity;
	}

	public String getDbOMemberEmail() {
		return dbOMemberEmail;
	}

	public void setDbOMemberEmail(String dbOMemberEmail) {
		this.dbOMemberEmail = dbOMemberEmail;
	}

	public Integer getDbOId() {return dbOId;}
	public void setDbOId(Integer dbOId) {this.dbOId = dbOId;}

	public Date getDbOOrderDate() {return dbOOrderDate;}
	public void setDbOOrderDate(Date dbOOrderDate) {this.dbOOrderDate = dbOOrderDate;}

	public RegionBean getDbOPickupRegion() {return dbOPickupRegion;}
	public void setDbOPickupRegion(RegionBean dbOPickupRegion) {this.dbOPickupRegion = dbOPickupRegion;}
	
	public String getDbOPickUpDetail() {return dbOPickUpDetail;}
	public void setDbOPickUpDetail(String dbOPickUpDetail) {this.dbOPickUpDetail = dbOPickUpDetail;}

	public Date getDbODeliverTime() {return dbODeliverTime;}
	public void setDbODeliverTime(Date dbODeliverTime) {this.dbODeliverTime = dbODeliverTime;}

	public Date getDbOArriveDate() {return dbOArriveDate;}
	public void setDbOArriveDate(Date dbOArriveDate) {this.dbOArriveDate = dbOArriveDate;}

	public Integer getDbOTotalPrice() {return dbOTotalPrice;}
	public void setDbOTotalPrice(Integer dbOTotalPrice) {this.dbOTotalPrice = dbOTotalPrice;}

	public StatusBean getDbOStatus() {return dbOStatus;}
	public void setDbOStatus(StatusBean dbOStatus) {this.dbOStatus = dbOStatus;}



	public List<OrderDetailBean> getDbOOrderDetail() {return dbOOrderDetail;}
	public void setDbOOrderDetail(List<OrderDetailBean> dbOOrderDetail) {this.dbOOrderDetail = dbOOrderDetail;}

}



