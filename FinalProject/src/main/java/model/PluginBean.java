package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="plugin")
public class PluginBean {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer dbPIId;
	@Column(columnDefinition="nvarchar(20)")
	private String dbPIName;
	@Column(columnDefinition="nvarchar(max)")	
	private String dbPIImage;
	@ManyToOne
	@JoinColumn(name="dbPIStatus",nullable=false)
	private StatusBean dbPIStatus;
	
	
	public PluginBean() {
		super();		
	}
	
	public PluginBean(Integer dbPIId, String dbPIName, StatusBean dbPIStatus) {
		super();
		this.dbPIId = dbPIId;
		this.dbPIName = dbPIName;
		this.dbPIStatus = dbPIStatus;
	}
	
	public PluginBean(String dbPIName, String dbPIImage, StatusBean dbPIStatus) {
		super();
		this.dbPIName = dbPIName;
		this.dbPIImage = dbPIImage;
		this.dbPIStatus = dbPIStatus;
	}

	public PluginBean(Integer dbPIId, String dbPIName, String dbPIImage, StatusBean dbPIStatus) {
		super();
		this.dbPIId = dbPIId;
		this.dbPIName = dbPIName;
		this.dbPIImage = dbPIImage;
		this.dbPIStatus = dbPIStatus;
	}

	public Integer getDbPIId() {return dbPIId;}
	public void setDbPIId(Integer dbPIId) {this.dbPIId = dbPIId;}
	
	public String getDbPIName() {return dbPIName;}
	public void setDbPIName(String dbPIName) {this.dbPIName = dbPIName;}
	
	public String getDbPIImage() {return dbPIImage;}
	public void setDbPIImage(String dbPIImage) {this.dbPIImage = dbPIImage;}
	
	public StatusBean getDbPIStatus() {return dbPIStatus;}
	public void setDbPIStatus(StatusBean dbPIStatus) {this.dbPIStatus = dbPIStatus;}
	
	
	
}
