package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
@Entity
@Table(name="CompanyAddress")
public class CompanyAddressBean {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer pk;
	@Column(columnDefinition="nvarchar(100)")
	private String dbIAddress;
//	@ManyToOne
	@OneToOne
	@JoinColumn(name="dbIStatus",nullable=false)
	private StatusBean dbIStatus=new StatusBean();
//	private StatusBean dbIStatus;
	
	public CompanyAddressBean() {
		super();
	}
	
	public CompanyAddressBean(Integer pk, String dbIAddress, StatusBean dbIStatus) {
		super();
		this.pk = pk;
		this.dbIAddress = dbIAddress;
		this.dbIStatus = dbIStatus;
	}
	
	public Integer getPk() {return pk;}
	public void setPk(Integer pk) {this.pk = pk;}
	
	public String getDbIAddress() {return dbIAddress;}
	public void setDbIAddress(String dbIAddress) {this.dbIAddress = dbIAddress;}
	
	public StatusBean getDbIStatus() {return dbIStatus;}
	public void setDbIStatus(StatusBean dbIStatus) {this.dbIStatus = dbIStatus;}
	
}
