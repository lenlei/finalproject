package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
@Entity
@Table(name="CompanyTel")
public class CompanyTelBean {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer pk;
	@Column(columnDefinition="varchar(12)")
	private String dbITel;
	@ManyToOne
	@JoinColumn(name="dbIStatus",nullable=false)
	private StatusBean dbIStatus;
	
	public CompanyTelBean() {
		super();
	}
	
	public CompanyTelBean(Integer pk, String dbITel, StatusBean dbIStatus) {
		super();
		this.pk = pk;
		this.dbITel = dbITel;
		this.dbIStatus = dbIStatus;
	}
	
	public Integer getPk() {return pk;}
	public void setPk(Integer pk) {this.pk = pk;}
	
	public String getDbITel() {return dbITel;}
	public void setDbITel(String dbITel) {this.dbITel = dbITel;}
	
	public StatusBean getDbIStatus() {return dbIStatus;}
	public void setDbIStatus(StatusBean dbIStatus) {this.dbIStatus = dbIStatus;}
	
}
