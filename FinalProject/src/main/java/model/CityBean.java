package model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "City")
public class CityBean {
	@Id
	@Column(columnDefinition = "int")
	private Integer dbCid;
	@Column(columnDefinition="nvarchar(20)")
	private String dbCName;
	@OneToMany(mappedBy="dbCid")
	private List<RegionBean> dbRegionBean;

	public CityBean() {
		super();
	}
	
	public CityBean(Integer dbCid) {
		super();
		this.dbCid = dbCid;
	}

	public CityBean(Integer dbCid, String dbCName) {
		super();
		this.dbCid = dbCid;
		this.dbCName = dbCName;
	}
	
	public CityBean(Integer dbCid, String dbCName, List<RegionBean> dbRegionBean) {
		super();
		this.dbCid = dbCid;
		this.dbCName = dbCName;
		this.dbRegionBean = dbRegionBean;
	}

	public List<RegionBean> getDbRegionBean() {
		return dbRegionBean;
	}
	
	public void setDbRegionBean(List<RegionBean> dbRegionBean) {
		this.dbRegionBean = dbRegionBean;
	}
	
	public Integer getDbCid() {
		return dbCid;
	}

	public void setDbCid(Integer dbCid) {
		this.dbCid = dbCid;
	}

	public String getDbCName() {
		return dbCName;
	}

	public void setDbCName(String dbCName) {
		this.dbCName = dbCName;
	}
}