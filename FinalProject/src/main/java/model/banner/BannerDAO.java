package model.banner;

import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import model.BannerBean;
import model.StatusBean;
@Repository
public class BannerDAO implements BannerIDAO {
	
	@Autowired
	private SessionFactory sessionfactory;

	@Override
	public List<BannerBean> selectAll() {
		String HQL = "From BannerBean order by dbBType , dbBSort";
		@SuppressWarnings("unchecked")
		TypedQuery<BannerBean> query = sessionfactory.getCurrentSession().createQuery(HQL);
		List<BannerBean> list = query.getResultList();
		return list;
//		return sessionfactory.getCurrentSession().createQuery(HQL).getResultList();
	}

	@Override
	public void delete(BannerBean bb) {
		bb.setDbBStatus(new StatusBean());
		sessionfactory.getCurrentSession().delete(sessionfactory.getCurrentSession().get(BannerBean.class,bb.getDbBId()));
	}

	@Override
	public void insert(BannerBean bb) {
		sessionfactory.getCurrentSession().save(bb);
	}
	

	@Override
	public void update(BannerBean bb) {
		BannerBean temp = sessionfactory.getCurrentSession().get(BannerBean.class, bb.getDbBId());
		if(!"".equals(bb.getDbBPicture())) {
			temp.setDbBPicture(bb.getDbBPicture());			
		}
		temp.setDbBHyperLink(bb.getDbBHyperLink());
		temp.setDbBIntroduction(bb.getDbBIntroduction());
		temp.setDbBStatus(new StatusBean(bb.getDbBStatus().getDbSId(),null));
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BannerBean> selectType(BannerBean bb) {
		String HQL = "From BannerBean where dbBType = :type order by dbBType , dbBSort";
		Session session = sessionfactory.getCurrentSession();
		TypedQuery<BannerBean> query = session.createQuery(HQL);
		query.setParameter("type", bb.getDbBType());
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<BannerBean> selectPicture(BannerBean bb) {
		String HQL = "From BannerBean where dbBType = :type and dbStatus=2";
		Session session = sessionfactory.getCurrentSession();
		TypedQuery<BannerBean> query = session.createQuery(HQL);
		query.setParameter("type", bb.getDbBType());
		System.out.println("111111111");
		System.out.println(bb.getDbBType());
		return query.getResultList();
	
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Integer selectSort(BannerBean bb) {
		String HQL = "select max(dbBSort) From BannerBean where dbBType = :type";
		TypedQuery<Integer> query = sessionfactory.getCurrentSession().createQuery(HQL);
		query.setParameter("type", bb.getDbBType());
		return query.getSingleResult();
	}




}
