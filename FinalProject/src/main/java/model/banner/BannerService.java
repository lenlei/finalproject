package model.banner;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import model.BannerBean;

@Service
public class BannerService {
	
	@Autowired
	private BannerIDAO BDAO;
	
	public void insert(BannerBean bb) {
		BDAO.insert(bb);
	}
	
	public void update(BannerBean bb) {
		BDAO.update(bb);
	}
	
	public void delete(BannerBean bb) {
		BDAO.delete(bb);
	}
	
	public List<BannerBean> selectAll(){
		return BDAO.selectAll();
	}
	
	public List<BannerBean> selectType(BannerBean bb){
		return BDAO.selectType(bb);
	}

	public Integer selectSort(BannerBean bb) {
		return BDAO.selectSort(bb);
	}
//	public List<BannerBean> selectPicture(){
//		System.out.println("2222222222");
//		return BDAO.selectPicture();
//	}
//	public List<BannerBean> selectPicture2(){
//		System.out.println("2222222222");
//		return BDAO.selectPicture2();
//	}
	public List<BannerBean> selectPicture(BannerBean bb){
		return BDAO.selectPicture(bb);
	}

}
