package model.banner;

import java.util.List;

import model.BannerBean;

public interface BannerIDAO {
	

	public List<BannerBean> selectAll();
	public List<BannerBean> selectType(BannerBean bb);
	public void delete(BannerBean bb);
	public void insert(BannerBean bb);
	public void update(BannerBean bb);
	public Integer selectSort(BannerBean bb);
	//public List<BannerBean> selectPicture();
	//public List<BannerBean> selectPicture2();
	List<BannerBean> selectPicture(BannerBean bb);
}