package model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
@Entity
@Table(name="SEO")
public class SEOSetBean {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer pk;
	@OneToOne
	@JoinColumn
	private SEODescriptionBean SEODescription;
	@OneToOne
	@JoinColumn
	private SEOKeyWordBean SEOKeyWord;
	
	public SEOSetBean() {}

	public SEOSetBean(SEODescriptionBean sEODescription, SEOKeyWordBean sEOKeyWord) {
		super();
		SEODescription = sEODescription;
		SEOKeyWord = sEOKeyWord;
	}

	public Integer getPk() {return pk;}
	public void setPk(Integer pk) {this.pk = pk;}

	public SEODescriptionBean getSEODescription() {return SEODescription;}
	public void setSEODescription(SEODescriptionBean sEODescription) {SEODescription = sEODescription;}

	public SEOKeyWordBean getSEOKeyWord() {return SEOKeyWord;}
	public void setSEOKeyWord(SEOKeyWordBean sEOKeyWord) {SEOKeyWord = sEOKeyWord;	}
	
	
}


