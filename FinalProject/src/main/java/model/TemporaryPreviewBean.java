package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TemporaryPreview")
public class TemporaryPreviewBean {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer pk;
	@Column(columnDefinition="int")
	private Integer dbTPId;
	@Column(columnDefinition="int")
	private Integer dbTPage;
	@Column(columnDefinition="nvarchar(max)")
	private String dbTPageImage;
	
	public TemporaryPreviewBean() {
		super();
	}
	
	public TemporaryPreviewBean(Integer dbTPId, Integer dbTPage, String dbTPageImage) {
		super();
		this.dbTPId = dbTPId;
		this.dbTPage = dbTPage;
		this.dbTPageImage = dbTPageImage;
	}
	
	public Integer getPk() {return pk;}
	public void setPk(Integer pk) {this.pk = pk;}
	
	public Integer getDbTPId() {return dbTPId;}
	public void setDbTPId(Integer dbTPId) {	this.dbTPId = dbTPId;}
	
	public Integer getDbTPage() {return dbTPage;}
	public void setDbTPage(Integer dbTPage) {this.dbTPage = dbTPage;}
	
	public String getDbTPageImage() {return dbTPageImage;}
	public void setDbTPageImage(String dbTPageImage) {this.dbTPageImage = dbTPageImage;}
	
	
}
