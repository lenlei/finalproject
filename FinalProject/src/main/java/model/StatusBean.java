package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="nowstatus")
public class StatusBean {
	@Id
	@Column(columnDefinition="tinyint")
	private Byte dbSId;
	@Column(columnDefinition="nvarchar(10)")
	private String dbSName;
	
	public StatusBean() {}

	
	public StatusBean(Byte dbSId, String dbSName) {
		super();
		this.dbSId = dbSId;
		this.dbSName = dbSName;
	}


	public Byte getDbSId() {return dbSId;}
	public void setDbSId(Byte dbSId) {this.dbSId = dbSId;}
	
	public String getDbSName() {return dbSName;}
	public void setDbSName(String dbSName) {this.dbSName = dbSName;}


	
}

