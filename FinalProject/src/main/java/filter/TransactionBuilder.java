package filter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

import org.hibernate.SessionFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.WebApplicationContext;


@WebFilter("/*")
public class TransactionBuilder implements Filter {
	private SessionFactory sessionFactory;
	
	public void init(FilterConfig fConfig) throws ServletException {
		ServletContext application = fConfig.getServletContext();
		ApplicationContext context =(ApplicationContext)application.getAttribute(WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE);
		this.sessionFactory = (SessionFactory)context.getBean("sessionFactory");	//取得工廠
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)  {
		try {
			request.setCharacterEncoding("UTF-8");
			response.setContentType("text/html;charset=UTF-8");
			sessionFactory.getCurrentSession().beginTransaction();				//開啟交易
			chain.doFilter(request, response);				//開始作業
			sessionFactory.getCurrentSession().getTransaction().commit();									//確認交易
		} catch (Exception e) {
			if (sessionFactory.getCurrentSession().getTransaction()!=null) {
				sessionFactory.getCurrentSession().getTransaction().rollback();}					//取消交易
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}

	public void destroy() {}
}
