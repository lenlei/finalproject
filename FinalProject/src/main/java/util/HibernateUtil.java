package util;

import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class HibernateUtil {
	private static SessionFactory sessionFactory = buildSessionFactory();
	  public static SessionFactory buildSessionFactory() {
		  try {
				StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder()
		                .configure("hibernate.cfg.xml").build();
		        Metadata metadata = new MetadataSources(standardRegistry).getMetadataBuilder().build();
		        SessionFactory sessionFactory  = metadata.getSessionFactoryBuilder().build();
		        System.out.println("SessionFactory be created ...");
		        return sessionFactory; 
				
			} catch (Throwable ex) {
				System.err.println("Initial SessionFactory creation failed." + ex);
				throw new ExceptionInInitializerError(ex);
			}
		  }

		  public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}

		public static void shutdown() {
			  getSessionFactory().close();
		  }
}
